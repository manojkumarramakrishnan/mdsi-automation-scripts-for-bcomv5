package BCOM_HANDBAGV5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.interactions.Actions;

import com.opera.core.systems.scope.protos.UmsProtos.Response;


public class BCOM_HANDBAGV5 extends XPath {
	static FileOutputStream fileLog = null;
	static FileOutputStream fileLog1 = null;
	static WebDriver driver;
	static void fileCreate() throws FileNotFoundException{
		String fileName = "D:\\TestReport\\SeleniumTestReport\\BCOMHANDBAGV5_"+new SimpleDateFormat("yyyyMMdd").format(new Date())+".csv";
		String fileNamest = "D:\\TestReport\\SeleniumTestReport\\BCOMHANDBAGV5SmokeTestCases_"+new SimpleDateFormat("yyyyMMdd").format(new Date())+".csv";
		System.out.println("fileName:" +fileName);
		fileLog = new FileOutputStream(new File(fileName));
		fileLog1 = new FileOutputStream(new File(fileNamest));
	}	
	public static void logInfo(String logMsg) throws IOException{
		logMsg +="\n";
		fileLog.write(logMsg.getBytes());
	}
	public static void logInfo1(String logMsg) throws IOException{
		logMsg +="\n";
		fileLog1.write(logMsg.getBytes());
	}
	public static void main(String[] args) throws InterruptedException, IOException, JSONException  
	{
		fileCreate();
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability("chrome.binary","C:\\Program Files (x86)\\Internet Explorer\\iexplore.exe");
		System.setProperty("webdriver.ie.driver", "D:\\Registry\\IEDriverServer.exe");
		driver = new InternetExplorerDriver(capabilities);
		driver.manage().deleteAllCookies();
		//V5 config & Smoke TestCases 
		launchSite();
		//browsePage();
		//singlePdpPageValidation();
		searchByUPC();
		//shoppingBagValidation();
		//settingPageValidation();
		searchByWebID();
		favoritePage();
		//logOut();
		//searchSuggestionsPage();
		//masterProductPage();
		//browsePagePriceValidation();
		//browseFacetsPage();
		browsePageSeeAllDesigners();
	}
	private static void launchSite() 
	{
		try
		{
			try
			{
				driver.get("http://localhost/SkavaCatalog/index_kiosk.html?t=1&debug=true");
				Thread.sleep(5000);
				Actions action = new Actions(driver);
				action.click().perform();
				Thread.sleep(2000);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}		
			//1.Verify that app gets installed and launched successfully without any errors 
			if(driver.findElement(By.xpath("//*[@id='skPageLayoutCell_1_id-center-north-north']/div/div[3]")).isDisplayed())
			{
				String logErrsmp1 ="1,Verify that app gets installed and launched successfully without any errors, Pass"; 
				logInfo1(logErrsmp1);
			}
			else
			{
				String logErrsmf1 ="1,Verify that app gets installed and launched successfully without any errors, Fail"; 
				logInfo1(logErrsmf1);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}	
	}
	private static void browsePage() 
	{	
		try 
		{
			try
			{
				//Selecting randomly the categories in  the HomePage 
				Random r2 = new java.util.Random();
				List<WebElement> links = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_1_id-center-north-center']/div/div"));
				WebElement randomElement = links.get(r2.nextInt(links.size()));
				randomElement.click();
				Thread.sleep(4000);  
				String logErr301 ="Pass:The Site gets launched Successfully"; 
				logInfo(logErr301);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
			Thread.sleep(4000);
			//2.Verify that while selecting all the categories in the home page, the browse page should be displayed 
			if(driver.findElement(By.xpath("//*[@id='bPimageDiv']")).isDisplayed())
			{
				String logErrsmp2 ="2,Verify that while selecting all the categories in the home page the browse page should be displayed, Pass"; 
				logInfo1(logErrsmp2);
			}
			else
			{
				String logErrsmf2 ="2,Verify that while selecting all the categories in the home page the browse page should be displayed, Fail"; 
				logInfo1 (logErrsmf2);
			}	
			Thread.sleep(1000);
			//Browse Page Functionalities
			String logErr900 ="Browse Page"; 
			logInfo(logErr900);
			String logErr901="-------------"; 
			logInfo(logErr901);
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='bp_tab_0']")).click();
			Thread.sleep(5000);
			//4.Verify that while selecting the ".COM/Lookbook" tabs  in the browse page, the respective tab products should be displayed in the browse page 
			if(driver.findElement(By.xpath("//*[@id='id_bPpdtImage_0']")).isDisplayed())
			{
				String logErrsmp4="4,Verify that while selecting the .COM/Lookbook tabs  in the browse page the respective tab products should be displayed in the browse page, Pass"; 
				logInfo1(logErrsmp4);
			}
			else
			{
				String logErrsmf4 ="4,Verify that while selecting the .COM/Lookbook tabs  in the browse page the respective tab products should be displayed in the browse page,  Fail"; 
				logInfo1(logErrsmf4);
			}
			Thread.sleep(3000);		
			try
			{
				//Selecting the products in the categories 
				Random r11 = new java.util.Random();
				List<WebElement> links11= driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div"));
				WebElement randomElement11 = links11.get(r11.nextInt(links11.size()));
				randomElement11.click();
				Thread.sleep(8000); 
				String logErr302 ="Pass:The BrowsePage gets displayed"; 
				logInfo(logErr302);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		} 
		catch (Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void singlePdpPageValidation() 
	{
		try
		{
			//Stub Data
			//driver.findElement(By.xpath("(//*[@class='cls_mamFSrch'])")).click();	
			//Thread.sleep(1000);
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).click();	
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("1426819");
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
			// To test the More Color Functionalities   859422 1283301
			//driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[9]/div/div[1]")).click();	
			//driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[2]/div[1]")).click();	
			//ProductID with the More Colors
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("2308437");
			//Product with the two color swatches 
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("2308456");
			//driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
			//Thread.sleep(6000);
			//  Need to check for master product condition  851147 
			//Working flow
			try
			{
				// Single Product Page
				String logErr303 ="Single Product Page"; 
				logInfo(logErr303);
				String logErr304="---------------------"; 
				logInfo(logErr304);
				String currentURL = driver.getCurrentUrl();
				System.out.println("currentURL:" +currentURL);  
				String pdtId=currentURL.substring(currentURL.indexOf("pdt_id=")+7,currentURL.indexOf("&type"));
				System.out.println("pdtId:" +pdtId); 
				String pdtUrl = "http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+pdtId+"?type=ID&storeid=28&campaignId=383";
				URL url  = new URL(pdtUrl);
				String pageSource  = new Scanner(url.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource:" +pageSource); 
				if(pageSource.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr6 = "Fail:The Product Page doesn't gets displayed:\n" +pageSource;
					logInfo(logErr6);
				}
				else
				{	 
					JSONObject StreamPDPJson = new JSONObject(pageSource);
					System.out.println("StreamPDPJson:" +StreamPDPJson);	
					String strmPDPName=StreamPDPJson.getString("name");
					System.out.println("strmPDPName:" +strmPDPName);
					String PdtName=driver.findElement(By.xpath(ProductName)).getText();
					System.out.println("PdtName:" +PdtName);
					if(PdtName.equals(strmPDPName))
					{
						String logErr4 = "Pass:The Product Name gets matched with the stream call \nProduct name: " +PdtName+ "\nStreamCall Response:" +strmPDPName; 
						logInfo(logErr4);
					}
					else
					{
						String logErr5 = "Fail:The Product Name doesn't gets matched with the stream call \n Product name: " +PdtName+ "\nStreamCall Response:" +strmPDPName; 
						logInfo(logErr5);
					} 
					String PdtDescriptiontitle=driver.findElement(By.xpath(ProductDescriptionHeader)).getText();
					System.out.println("PdtDescriptiontitle:" +PdtDescriptiontitle);
					JSONObject strmiteminfo=StreamPDPJson.getJSONObject("properties").getJSONObject("iteminfo");
					JSONObject strmDesc=strmiteminfo.getJSONArray("description").getJSONObject(0);
					String strmDescriptiontitle=strmDesc.getString("value");
					System.out.println("strmDescriptiontitle:" +strmDescriptiontitle);
					if(PdtDescriptiontitle.equals(strmDescriptiontitle))
					{
						String logErr8 = "Pass:The Product Description gets matched:\n" +PdtDescriptiontitle+ "\n" +strmDescriptiontitle; 
						logInfo(logErr8);
					}
					else
					{
						String logErr9 = "Fail:The Product Description doesn't gets matched:\n" +PdtDescriptiontitle+ "\n" +strmDescriptiontitle; 
						logInfo(logErr9);
					}
					JSONArray strmBultdes=strmiteminfo.getJSONArray("bulletdescription");
					int Productbulletlength = driver.findElements(By.xpath(Productdesclength)).size();
					System.out.println("Productbulletlength:" +Productbulletlength);	    	
					for(int k=0,l=1;k<strmBultdes.length()||l<=Productbulletlength;k++,l++)
					{
						JSONObject strmBultdesc=strmBultdes.getJSONObject(k);
						String	strmBulletvalue=strmBultdesc.getString("value");
						System.out.println("strmBulletvalue:\n" +strmBulletvalue);    
						String productbulletsdescription = driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[3]/div["+l+"]/div[2]")).getAttribute("innerHTML");
						System.out.println("productbulletsdescription:\n" +productbulletsdescription);
						if(productbulletsdescription.equals(strmBulletvalue))
						{
							String logErr6 = "Pass:The ProductDescription Bulletins gets matched:\n" +productbulletsdescription+ "\n" +strmBulletvalue; 
							logInfo(logErr6);
						}
						else
						{
							String logErr7 = "Fail:The ProductDescription Bulletins doesn't gets matched:\n" +productbulletsdescription+ "\n"+strmBulletvalue;
							logInfo(logErr7);
						} 
					}
					String Productdescriptiontitle=driver.findElement(By.xpath(productdescription)).getText();
					System.out.println("Productdescriptiontitle:" +Productdescriptiontitle);		
					//9.Verify that while selecting the products from the browse page,You Might Also Like Panel,search results page the Single PDP,Master and Member Products Pages should be displayed 
					if(driver.findElement(By.xpath("//*[@id='id_rightContainerDivChild']/div[2]/div[1]")).isDisplayed())
					{
						String logErrsmp9 ="9,Verify that while selecting the products from the browse page You Might Also Like Panel search results page the Single PDP Master and Member Products Pages should be displayed, Pass"; 
						logInfo1(logErrsmp9);
					}
					else
					{
						String logErrsmf9 ="9,Verify that while selecting the products from the browse page You Might Also Like Panel search results page the Single PDP Master and Member Products Pages should be displayed, Fail"; 
						logInfo1(logErrsmf9);
					}
					//5.Verify whether recommended products are shown in PDP page
					int productpanelcount=driver.findElements(By.xpath("//*[@id='id_rightContainerDivChild']/div")).size();  
					System.out.println("productpanelcount:" +productpanelcount);	
					if(productpanelcount==2&&Productdescriptiontitle.equals("DETAILS"))
					{
						String logErrsmf5 ="5,Verify whether recommended products are shown in PDP page, Fail"; 
						logInfo1(logErrsmf5);

					}
					else if(productpanelcount==3&&driver.findElement(By.xpath("//*[@id='reviewCont']")).isDisplayed())
					{ 

						String logErrsmf5y ="5,Verify whether recommended products are shown in PDP page, Fail"; 
						logInfo1(logErrsmf5y);
					}
					else if(productpanelcount==3&&driver.findElement(By.xpath("(//*[@class='pdtRecommendedTitle'])")).isDisplayed())       
					{
						String logErrsmp5y ="5,Verify whether recommended products are shown in PDP page, Pass"; 
						logInfo1(logErrsmp5y);
					}
					//if(driver.findElement(By.xpath("//*[@id='reviewCont']")).isDisplayed())
					//{
					//String Productreviewnametitle=driver.findElement(By.xpath("//*[@id='id_pdtReviewContainer']/div[1]")).getAttribute("innerHTML");
					//System.out.println("Productreviewnametitle:" +Productreviewnametitle);
					//if(Productdetailsnametitle.equals("Product Details")&&Productdescriptiontitle.equals("Product Description")&&Productreviewnametitle.equals("Customer Reviews"))
					//{
					//String logErrsmf5y ="5.Verify whether recommended products are shown in PDP page::: Fail"; 
					//logInfo(logErrsmf5y);
					//}
					//}			
					//else if(driver.findElement(By.xpath("//*[@id='id_recPdtContainer']/div[1]")).isDisplayed())
					//{
					//	String Youmightalsoliketitle=driver.findElement(By.xpath("//*[@id='id_recPdtContainer']/div[1]")).getAttribute("innerHTML");
					//System.out.println("Youmightalsoliketitle:" +Youmightalsoliketitle);
					//if(Productdetailsnametitle.equals("Product Details")&&Productdescriptiontitle.equals("Product Description")&&Youmightalsoliketitle.equals("You Might Also Like"))
					//{
					//String logErrsmp5y ="5.Verify whether recommended products are shown in PDP page::: Pass"; 
					//logInfo(logErrsmp5y);
					//}
					//}
					//}
					else if(productpanelcount==4)
					{
						String logErrsmp5 ="5,Verify whether recommended products are shown in PDP page, Pass"; 
						logInfo1(logErrsmp5);
					}					
					if(Productdescriptiontitle.equals("DETAILS"))
					{
						String logErr2 = "Pass:The Product Page Titles gets matched:\n"+Productdescriptiontitle;  
						logInfo(logErr2);
					}
					else
					{
						String logErr3 = "Fail:The Product Page Titles doesn't gets matched:\n"+Productdescriptiontitle;
						logInfo(logErr3);
					}
					//Product additional images 
					driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_0']")).click();  
					String Productprimaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
					System.out.println("Productprimaryimageurl:" +Productprimaryimageurl);
					String Productprimaryimagetrim=Productprimaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					System.out.println("Prroductprimaryimagetrim:" +Productprimaryimagetrim);
					String strmPDPImage=StreamPDPJson.getString("image");
					System.out.println("strmPDPImage:" +strmPDPImage);
					if(Productprimaryimagetrim.equals(strmPDPImage))
					{
						String logErr14= "Pass:The Product Page Primary Image gets matched:\n" +Productprimaryimagetrim+ "\n" +strmPDPImage; 
						logInfo(logErr14);
					}
					else
					{
						String logErr15= "Fail:The Product Page Primary Image doesn't gets matched:\n" +Productprimaryimagetrim+ "\n" +strmPDPImage; 
						logInfo(logErr15);
					}
					driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click(); 
					int productadditionalimagecount=driver.findElements(By.xpath("//*[@id='id_styleImageDiv']/div/div")).size();  
					System.out.println("productadditionalimagecount:" +productadditionalimagecount);
					//Condition for Additional Images 
					//JSONArray strmadditionalimages= strmiteminfo.getJSONArray("additionalimages");
					//for(int a=0,b=2;a<strmadditionalimages.length()||b<=productadditionalimagecount;a++,b++)
					//{
					//JSONObject strmadditionalobject=strmadditionalimages.getJSONObject(a);
					//String streamadditionalimageurl=strmadditionalobject.getString("image");
					//System.out.println("streamadditionalimageurl:" +streamadditionalimageurl);  
					//driver.findElement(By.xpath("//*[@id='id_styleImageDiv']/div/div["+b+"]")).click();
					//System.out.println("b:" +b);
					//int c=b-1;
					//driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_"+c+"']")).click();
					//String Productsecondaryimageurlzoom=driver.findElement(By.xpath("//*[@id='id_skImageScroller_"+c+"']/img")).getAttribute("src");
					//System.out.println("Productsecondaryimageurlzoom:" +Productsecondaryimageurlzoom);
					//String Productsecondaryimageurlzoomtrim=Productsecondaryimageurlzoom.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					//System.out.println("Productsecondaryimageurlzoomtrim:" +Productsecondaryimageurlzoomtrim); 
					//if(Productsecondaryimageurlzoomtrim.equals(streamadditionalimageurl))
					//{
					//String logErr16= "Pass:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr16);
					//}
					//else
					//{
					//String logErr17= "Fail:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr17);
					//}    
					//driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click();
					//}
					//Color Count and  Selection
					int productcolorcountno=driver.findElements(By.xpath(productcolorcount)).size();
					System.out.println("productcolorcountno:" +productcolorcountno);
					for(int mo=1;mo<=productcolorcountno;mo++)
					{   
						//Need to change the xpath for the more color condition
						if(productcolorcountno>5)
						{
							driver.findElement(By.xpath("//*[@id='id_moreColors']")).click();
							Thread.sleep(1500);             								                        
							driver.findElement(By.xpath("//*[@id='id_moreColorsPopup']/div[2]/div/div["+mo+"]")).click();
							Thread.sleep(1000);
							driver.findElement(By.xpath("//*[@id='id_moreColorsPopup']/div[1]/div[2]")).click();
						}
						else
						{				                 
							driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[2]/div["+mo+"]")).click();
							Thread.sleep(1000);
						}
						//Size is mostly NA for BCOM:Size Count and Selection
						//int productskucount=driver.findElements(By.xpath(productskusize)).size(); 
						//System.out.println("productskucount:" +productskucount);
						//int totalskucount= productcolorcountno*productskucount;
						//System.out.println("totalskucount:" +totalskucount);
						//String productskusizedetailss=driver.findElement(By.xpath(productskusizedetail)).getAttribute("sizename");
						//System.out.println("productskusizedetailss:" +productskusizedetailss);
						//for(int n=1;n<=productskucount;n++)
						//{
						//driver.findElement(By.xpath(productsizeselection)).click(); 
						//Thread.sleep(6000);
						//driver.findElement(By.xpath("html/body/div[2]/div/div/div/div/div[2]/div[2]/div[2]/div[1]/div["+n+"]")).click();
						Thread.sleep(2000);                         
						String productUPCvaluecheck= driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div#pdpUPCId.upcid")).getText();
						System.out.println("productUPCvaluecheck:" +productUPCvaluecheck);
						String Productupcvaluechecktrim=productUPCvaluecheck.substring(productUPCvaluecheck.indexOf("UPC:")+5);
						System.out.println("Productupcvaluechecktrim:" +Productupcvaluechecktrim);				
						//Stream Comparison 
						JSONObject strmChild=StreamPDPJson.getJSONObject("children");
						JSONArray strmSku=strmChild.getJSONArray("skus");
						for(int m=0;m<strmSku.length();m++)
						{
							JSONObject strmSkuobj=strmSku.getJSONObject(m);
							JSONObject strmProp=strmSkuobj.getJSONObject("properties");
							String  strmSkuId=strmSkuobj.getString("identifier");
							System.out.println("strmSkuId:" +strmSkuId);
							if(Productupcvaluechecktrim.equals(strmSkuId))
							{
								JSONObject strmPropr=strmProp.getJSONObject("orderinfo");
								String  strmProporder=strmPropr.getString("ordertype");
								System.out.println("strmProporder:" +strmProporder);
								JSONObject strmSkuinfo=strmProp.getJSONObject("skuinfo");
								JSONObject strmSkuColor=strmSkuinfo.getJSONObject("color");
								String  strmSkuColorvalue=strmSkuColor.getString("value");
								System.out.println("strmSkuColorvalue:" +strmSkuColorvalue);
								String strmSkuColorlabel=strmSkuColor.getString("label");
								System.out.println("strmSkuColorlabel:" +strmSkuColorlabel);
								//JSONObject strmSkuSize=strmSkuinfo.getJSONObject("size1");  
								//String  strmSkusizevalue=strmSkuSize.getString("value");
								//System.out.println("strmSkusizevalue:" +strmSkusizevalue);
								//String  strmSkusizelabel=strmSkuSize.getString("label");  
								//System.out.println("strmSkusizelabel:" +strmSkusizelabel);
								JSONObject strmBuyinfo=strmProp.getJSONObject("buyinfo");
								JSONObject strmPricing=strmBuyinfo.getJSONObject("pricing");
								String  strmQtylimit=strmBuyinfo.getString("qtylimit");
								System.out.println("strmQtylimit:" +strmQtylimit);
								JSONArray strmPrices=strmPricing.getJSONArray("prices");
								JSONObject strmOrstrPrice=strmPrices.getJSONObject(0);
								String  strmOrigstrprvalue=strmOrstrPrice.getString("value");
								System.out.println("strmOrigstrprvalue:" +strmOrigstrprvalue);
								JSONObject strmCurrstrPrice=strmPrices.getJSONObject(1);
								String strmCurrstrprvalue=strmCurrstrPrice.getString("value");
								System.out.println("strmCurrstrprvalue:" +strmCurrstrprvalue);
								JSONObject strmFedstrPrice=strmPrices.getJSONObject(2);
								String strmFedstrprvalue=strmFedstrPrice.getString("value");
								System.out.println("strmFedstrprvalue:" +strmFedstrprvalue);
								JSONArray strmAvailability=strmBuyinfo.getJSONArray("availability");
								JSONObject strmAvailability0=strmAvailability.getJSONObject(0);
								String strmOnlineinv=strmAvailability0.getString("onlineinventory");
								System.out.println("strmOnlineinv:" +strmOnlineinv);
								String strmOnline=strmAvailability0.getString("online");
								System.out.println("strmOnline:" +strmOnline); 
								String StrmFedfilinv=strmAvailability0.getString("fedfilinventory");
								System.out.println("StrmFedfilinv:" +StrmFedfilinv);
								String strmFedfil=strmAvailability0.getString("fedfil");
								System.out.println("strmFedfil:" +strmFedfil);
								Float strmOrigFloprvalue = Float.parseFloat(strmOrigstrprvalue);
								Float strmCurrFloprvalue = Float.parseFloat(strmCurrstrprvalue);
								Float strmFedFloprvalue = Float.parseFloat(strmFedstrprvalue); 
								JSONObject strmIteminfo=strmProp.getJSONObject("iteminfo");
								JSONArray strmShipmsgs=strmIteminfo.getJSONArray("shippingmessages");
								JSONObject strmShippingmsgs=strmShipmsgs.getJSONObject(0);
								String strmShippingmsgssuspain=strmShippingmsgs.getString("suspain");
								System.out.println("strmShippingmsgssuspain:" +strmShippingmsgssuspain);
								String strmShippingmsgsdeliverytype=strmShippingmsgs.getString("deliverytype");
								System.out.println("strmShippingmsgsdeliverytype:" +strmShippingmsgsdeliverytype);
								String strmShippingmsgsgiftwrap=strmShippingmsgs.getString("giftwrap");
								System.out.println("strmShippingmsgsgiftwrap:" +strmShippingmsgsgiftwrap);
								String strmShippingmsgsshipdate=strmShippingmsgs.getString("shipdate");
								System.out.println("strmShippingmsgsshipdate:" +strmShippingmsgsshipdate);
								String strmShippingmsgsshipdays=strmShippingmsgs.getString("shipdays");
								System.out.println("strmShippingmsgsshipdays:" +strmShippingmsgsshipdays); 
								String strmShippingmsgsmethod=strmShippingmsgs.getString("method");
								System.out.println("strmShippingmsgsmethod:" +strmShippingmsgsmethod);
								JSONArray strmSwtch=strmIteminfo.getJSONArray("swatches");
								JSONObject strmSwatches0=strmSwtch.getJSONObject(0);
								JSONArray strmPdtimage=strmSwatches0.getJSONArray("pdtimage");
								JSONObject strmPdtimage0=strmPdtimage.getJSONObject(0);
								String strmPdtaddseqnumber=strmPdtimage0.getString("sequencenumber");
								System.out.println("strmPdtaddseqnumber:" +strmPdtaddseqnumber);
								String strmPdtaddname=strmPdtimage0.getString("name");
								System.out.println("strmPdtaddname:" +strmPdtaddname);
								String strmPdtaddimage=strmPdtimage0.getString("image");     
								System.out.println("strmPdtaddimage:" +strmPdtaddimage);
								JSONArray strmStoreinfo=strmProp.getJSONArray("storeinfo");
								System.out.println("strmStoreinfo:" +strmStoreinfo);
								String WebID=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div.webid")).getText();
								System.out.println("WebID:" +WebID);
								String WebIDtrim=WebID.substring(WebID.indexOf("Web ID: ")+9);
								System.out.println("WebIDtrim:" +WebIDtrim);
								String strmPDPIdentifier= StreamPDPJson.getString("identifier");
								System.out.println("strmPDPIdentifier:" +strmPDPIdentifier);
								if(WebIDtrim.equalsIgnoreCase(strmPDPIdentifier))
								{
									String logErr18= "Pass:The Product Page Identifier gets matched:\n" +WebIDtrim+"\n" +strmPDPIdentifier; 
									logInfo(logErr18);
								}
								else
								{
									String logErr19= "Fail:The Product Page Identifier doesn't gets matched:\n" +WebIDtrim+ "\n" +strmPDPIdentifier; 
									logInfo(logErr19);
								}
								String productUPCvalue= driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div#pdpUPCId.upcid")).getText();
								System.out.println("productUPCvalue:" +productUPCvalue);
								String Productupcvaluetrim=productUPCvalue.substring(productUPCvalue.indexOf("UPC:")+5);
								System.out.println("Productupcvaluetrim:" +Productupcvaluetrim);  
								if(Productupcvaluetrim.equals(strmSkuId))
								{
									String logErr20= "Pass:The Product UPC value gets matched:\n" +Productupcvaluetrim+ "\n" +strmSkuId; 
									logInfo(logErr20);
								}
								else
								{
									String logErr21= "Fail:The Product UPC value doesn't gets matched:\n" +Productupcvaluetrim+ "\n" +strmSkuId; 
									logInfo(logErr21);
								}
								String productpagecolorvalue=driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[1]/div[2]")).getText();
								System.out.println("productpagecolorvalue:" +productpagecolorvalue);
								if(productpagecolorvalue.equalsIgnoreCase(strmSkuColorvalue)); 
								{
									String logErr22= "Pass:The Product Page UPC Color gets matched:\n" +productpagecolorvalue+"\n" +strmSkuColorvalue; 
									logInfo(logErr22);
								}
								//String productpagesizevalue=driver.findElement(By.xpath(productsizeselection)).getText(); 
								//System.out.println("productpagesizevalue:" +productpagesizevalue);
								//if(productpagesizevalue.equals(strmSkusizevalue))
								//{
								//String logErr24= "Pass:The Product Page UPC Size gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue; 
								//logInfo(logErr24);
								//}
								//else
								//{
								//String logErr25= "Fail:The Product Page UPC Size doesn't gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue;
								//logInfo(logErr25);
								//} 
								String pricevaluesaleinpdp=driver.findElement(By.xpath("//*[@id='id_pdpSalePrice']")).getText();
								System.out.println("pricevalueinpdp:" +pricevaluesaleinpdp);
								String pricevalueinpdp=driver.findElement(By.xpath("//*[@id='id_pdpRegPrice']")).getText();
								System.out.println("pricevalueinpdp:" +pricevalueinpdp);
								//Current and original are same
								if(strmCurrFloprvalue.compareTo(strmOrigFloprvalue)==0 && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
								{
									String logErr28= "Pass:The UPC Contains Current/Store price:\n" +pricevalueinpdp+"\n" +strmCurrFloprvalue; 
									logInfo(logErr28);
								}
								// Current/Store<Original
								else if(strmCurrFloprvalue.compareTo(strmOrigFloprvalue)<0 && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
								{
									String salepricevalueinpdp=driver.findElement(By.xpath("//*[@id='id_pdpSalePrice']")).getText();
									System.out.println("salepricevalueinpdp:" +salepricevalueinpdp); 
									String logErr29= "Pass:The UPC Contains Current/Store in Red Original in Black w/ label:\n" +salepricevalueinpdp+"\n" +strmCurrFloprvalue+ "\n" +pricevalueinpdp+ "\n" +strmOrigFloprvalue; 
									logInfo(logErr29);
								}
								// Current/Store>Original
								else if(strmCurrFloprvalue.compareTo(strmOrigFloprvalue)>0 && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
								{
									String logErr30= "Pass:The UPC Contains Current/Store in Black:\n" +pricevalueinpdp+"\n" +strmCurrFloprvalue; 
									logInfo(logErr30);
								}
								// Original Price is null
								else if((strmOrigFloprvalue==0) && (strmFedfil.equals("true")|| strmFedfil.equals("false")))
								{
									String logErr31= "Pass:The UPC Contains Current/Store in Black:\n" +pricevalueinpdp+"\n" +strmCurrFloprvalue; 
									logInfo(logErr31);
								}
								// Current price is null and Fedfilavailability price available 
								else if((strmCurrFloprvalue==0) && (strmFedfil.equals("true")))
								{
									String logErr31= "Pass:The UPC Contains Fedfil price:\n" +pricevalueinpdp+"\n" +strmFedFloprvalue; 
									logInfo(logErr31);
								}	
								// current price is null and Fedfilavailability price not available
								else if((strmCurrFloprvalue==0) && (strmFedfil.equals("false")))
								{
									String logErr32= "Pass:The UPC Contains Original price:\n" +pricevalueinpdp+"\n" +strmOrigFloprvalue; 
									logInfo(logErr32);
								}
								// Current and original price is null and Fedfilavailability price not available
								else if((strmCurrFloprvalue==0) && (strmOrigFloprvalue==0) && (strmFedfil.equals("false")))
								{
									String text="Price is unavailable, please validate price status in mPOS or POS.";
									String text1="Price is unavailable, please contact an  associate for pricing";
									if(pricevalueinpdp.equalsIgnoreCase(text))
									{
										String logErr33= "Pass:The Product Page contains penny price in Associate Mode:\n" +pricevalueinpdp+"\n" +text; 
										logInfo(logErr33);
									}
									else  
									{
										String logErr34= "Pass:The Product Page contains penny price in Customer Mode:\n" +pricevalueinpdp+ "\n" +text1; 
										logInfo(logErr34);
									}
								}
								else if((strmCurrFloprvalue==0) && (strmFedFloprvalue==0))
								{
									String logErr35= "Pass:The Product Page contains Original Price:\n" +pricevalueinpdp+ "\n" +strmOrigFloprvalue; 
									logInfo(logErr35);

								}
								//  Current/store price has penny price 
								else if	((strmCurrFloprvalue==0.01) && (strmFedFloprvalue<=0.10)) 			
								{
									String text="Price is unavailable, please validate price status in mPOS or POS.";
									String text1="Price is unavailable, please contact an  associate for pricing";
									if(pricevalueinpdp.equalsIgnoreCase(text))
									{
										String logErr33= "Pass:The Product Page contains penny price in Associate Mode:\n" +pricevalueinpdp+"\n" +text; 
										logInfo(logErr33);
									}
									else  
									{
										String logErr34= "Pass:The Product Page contains penny price in Customer Mode:\n" +pricevalueinpdp+ "\n" +text1; 
										logInfo(logErr34);
									}
								}
								if(productcolorcountno<=5)
								{
									String logErr60= "Pass:The Product doesn't contain more colors:\n" +productcolorcountno; 
									logInfo(logErr60);
								}
								else
								{
									String logErr61= "Pass:The Product contains more colors:\n" +productcolorcountno; 
									logInfo(logErr61);
								}
								if(strmOnline.equals("true")&&strmFedfil.equals("true"))
								{
									String logErr35= "Pass:The UPC is Available for shipping"; 
									logInfo(logErr35);
									driver.findElement(By.xpath("(//*[@class='addtoOrder'])")).click();  
									Thread.sleep(3000);
									String quickviewtitlestring=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
									System.out.println("quickviewtitlestring:" +quickviewtitlestring);
									String quickviewwebidstring=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
									System.out.println("quickviewwebidstring:" +quickviewwebidstring);
									String quickviewwebidstringtrim=quickviewwebidstring.substring(quickviewwebidstring.indexOf("(")+1,quickviewwebidstring.indexOf(")"));
									System.out.println("quickviewwebidstringtrim:" +quickviewwebidstringtrim);
									String quickviewpricestring=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
									System.out.println("quickviewpricestring:" +quickviewpricestring);
									String quickviewcolorstring=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
									System.out.println("quickviewcolorstring:" +quickviewcolorstring);
									String quickviewimageurl=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
									System.out.println("quickviewimageurl:" +quickviewimageurl);
									String quickviewimageurltrim=quickviewimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
									System.out.println("quickviewimageurltrim:" +quickviewimageurltrim);
									if(quickviewwebidstringtrim.equals(WebID)&&quickviewpricestring.equals(pricevalueinpdp)&&quickviewcolorstring.equalsIgnoreCase(productpagecolorvalue)&&quickviewimageurltrim.equals(Productprimaryimagetrim))
									{
										String logErr38= "Pass:The details in the AddtoBag overlay value gets matched:\n" +quickviewtitlestring+"\n" +PdtName+ "\n" +quickviewwebidstringtrim+ "\n" +WebID+ "\n" +quickviewpricestring+ "\n" +pricevalueinpdp+ "\n" +quickviewcolorstring+ "\n" +productpagecolorvalue+ "\n" +quickviewimageurltrim+ "\n" +Productprimaryimagetrim;
										logInfo(logErr38);  
									}
									else
									{
										String logErr39= "Fail:The details in the AddtoBag overlay value doesn't gets matched:\n" +quickviewtitlestring+"\n" +PdtName+ "\n" +quickviewwebidstringtrim+ "\n" +WebID+ "\n" +quickviewpricestring+ "\n" +pricevalueinpdp+ "\n" +quickviewcolorstring+ "\n" +productpagecolorvalue+ "\n" +quickviewimageurltrim+ "\n" +Productprimaryimagetrim;
										logInfo(logErr39);  
									}

									driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();		
								}
								else
								{
									String logErr36= "Pass:The UPC is not Available for shipping";
									logInfo(logErr36);
								}
								//Check others stores functionalities 
								Thread.sleep(2500);
								driver.findElement(By.xpath(productotherstores)).click();
								Thread.sleep(5000);
								//Check other Stores stream call data 
								String pdtupcvalue="http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+Productupcvaluetrim+"?type=UPCWithNearbyStores&storeid=28&campaignId=383";
								URL url1  = new URL(pdtupcvalue);
								String pageSource1  = new Scanner(url1.openConnection().getInputStream()).useDelimiter("\\Z").next();
								System.out.println("pageSource1:" +pageSource1);
								JSONObject StreamPDPUpccheckotherstores=new JSONObject(pageSource1);
								JSONObject streamPDPUpcchild=StreamPDPUpccheckotherstores.getJSONObject("children");
								JSONArray streamPDPUPCSku=streamPDPUpcchild.getJSONArray("skus");
								for(int q=0;q<streamPDPUPCSku.length();q++)
								{
									JSONObject streamPDPUPCSkuobj=streamPDPUPCSku.getJSONObject(q);
									String streamPDPUPCidentifier= streamPDPUPCSkuobj.getString("identifier");
									System.out.println("streamPDPUPCidentifier:" +streamPDPUPCidentifier);
									if(streamPDPUPCidentifier.equals(Productupcvaluetrim))
									{
										JSONObject streamPropUPC=streamPDPUPCSkuobj.getJSONObject("properties");
										JSONArray streamPDPStoreinfo=streamPropUPC.getJSONArray("storeinfo"); 
										int productstorescounts=driver.findElements(By.xpath(productstoresize)).size(); 
										System.out.println("productstorescounts:" +productstorescounts); 
										for(int o=0,p=1;o<streamPDPStoreinfo.length()||p<=productstorescounts;o++,p++)
										{

											JSONObject strmStoreinfo1=streamPDPStoreinfo.getJSONObject(o);
											String strmStoreinfophone=strmStoreinfo1.getString("phone");
											System.out.println("strmStoreinfophone:" +strmStoreinfophone);
											String strmStoreinfoSequenceno=strmStoreinfo1.getString("sequencenumber");
											System.out.println("strmStoreinfoSequenceno:" +strmStoreinfoSequenceno);
											String strmStoreinfoInventory=strmStoreinfo1.getString("inventory");
											System.out.println("strmStoreinfoInventory:" +strmStoreinfoInventory);
											String strmStoreinfoName=strmStoreinfo1.getString("name");
											System.out.println("strmStoreinfoName:" +strmStoreinfoName);
											String strmStoreinfoZipcode=strmStoreinfo1.getString("zipcode");
											System.out.println("strmStoreinfoZipcode:" +strmStoreinfoZipcode);
											String strmStoreinfoState=strmStoreinfo1.getString("state");
											System.out.println("strmStoreinfoState:" +strmStoreinfoState);
											String strmStoreinfoAddress1=strmStoreinfo1.getString("address1");
											System.out.println("strmStoreinfoAddress1:" +strmStoreinfoAddress1);
											String strmStoreinfoAddress2=strmStoreinfo1.getString("address2");
											System.out.println("strmStoreinfoAddress2:" +strmStoreinfoAddress2);
											String strmStoreinfoIdentifier=strmStoreinfo1.getString("identifier");
											System.out.println("strmStoreinfoIdentifier:" +strmStoreinfoIdentifier);
											String strmStoreinfoCity=strmStoreinfo1.getString("city");
											System.out.println("strmStoreinfoCity:" +strmStoreinfoCity);
											String strmStoreinfoNameconcat=strmStoreinfoName+strmStoreinfoIdentifier;
											System.out.println("strmStoreinfoNameconcat:" +strmStoreinfoNameconcat);
											String strmstoreinfocombined=strmStoreinfoNameconcat.replaceAll(""+strmStoreinfoIdentifier+".*","")+" "+"("+strmStoreinfoIdentifier+")";
											System.out.println("strmstoreinfocombined:" +strmstoreinfocombined);
											String checkavailabilityheadertitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[1]/div/div[2]/div")).getText();
											System.out.println("checkavailabilityheadertitle:" +checkavailabilityheadertitle);							                              
											driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[1]/div/div[2]/div")).click();
											Thread.sleep(2000);
											String checkavailabilitystreetNametitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[2]/div/div[1]")).getText();
											System.out.println("checkavailabilitystreetNametitle:" +checkavailabilitystreetNametitle);				                          
											String checkavailabilitystoreLocationtitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[2]/div/div[2]")).getText();
											System.out.println("checkavailabilitystoreLocationtitle:" +checkavailabilitystoreLocationtitle);
											String checkavailabilitystorePhonetitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[2]/div/div[3]")).getText();
											System.out.println("checkavailabilitystorePhonetitle:" +checkavailabilitystorePhonetitle);
											driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p+"]/div[1]/div/div[2]/div")).click();	
											if(checkavailabilityheadertitle.equals(strmstoreinfocombined)&&checkavailabilitystreetNametitle.equals(strmStoreinfoAddress1)&&checkavailabilitystoreLocationtitle.equals(strmStoreinfoAddress2)&&checkavailabilitystorePhonetitle.equals(strmStoreinfophone))
											{
												String logErr27= "Pass:The Product Page UPC Checkother Stores value gets matched:\n" +checkavailabilityheadertitle+"\n" +strmstoreinfocombined+ "\n" +checkavailabilitystreetNametitle+ "\n" +strmStoreinfoAddress1+ "\n" +checkavailabilitystoreLocationtitle+ "\n" +strmStoreinfoAddress2+ "\n" +checkavailabilitystorePhonetitle+ "\n" +strmStoreinfophone;  
												logInfo(logErr27);
											}	
											else
											{
												String logErr28= "Fail:The Product Page UPC Checkother Stores value doesn't gets matched:\n" +checkavailabilityheadertitle+"\n" +strmstoreinfocombined+ "\n" +checkavailabilitystreetNametitle+ "\n" +strmStoreinfoAddress1+ "\n" +checkavailabilitystoreLocationtitle+ "\n" +strmStoreinfoAddress2+ "\n" +checkavailabilitystorePhonetitle+ "\n" +strmStoreinfophone;
												logInfo(logErr28);
											}
											Thread.sleep(5000);
											String logErr37= "-----------------------------------------------------------------------------";
											logInfo(logErr37);
											System.out.println("-----------------------------------------");  
										}                                 
										driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent.ui-content div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_pdpSeeAllAvailable.SeeAllAvailable.cls_midleAlign.kioskOtherStore div.otherStlayoutContainer div.otherStoreCloseBtnDiv")).click();    
										driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent.ui-content div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#reviewCont div.pdpAddToFavt.pdpfavList.pdpfavList_icon")).click();
									}
								}
							}
						}
					} 
				}
				//11.Verify that PDP page price value should be displayed as per the PDP page price logic 
				String logErrsmp11="11,Verify that PDP page price value should be displayed as per the PDP page price logic, Pass"; 
				logInfo1(logErrsmp11);			
			} 
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());

		}
	}
	private static void searchByUPC()
	{
		try
		{
			try
			{
				//Search by UPC Values and comparison
				String logErr3051 ="Search By UPC"; 
				logInfo(logErr3051);
				String logErr3061="---------------"; 
				logInfo(logErr3061);	   
				driver.findElement(By.xpath("(//*[@class='cls_mamFSrch'])")).click();	
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).click();	
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("887042721632");
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
				Thread.sleep(8000);	
				String currentURL = driver.getCurrentUrl();
				System.out.println("currentURL:" +currentURL);  
				String pdtId133=currentURL.substring(currentURL.indexOf("pdt_id=")+7,currentURL.indexOf("&type"));
				System.out.println("pdtId:" +pdtId133); 
				String pdtUrl223 = "http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+pdtId133+"?type=UPCWithNearbyStores&storeid=28&campaignId=383";
				URL url223  = new URL(pdtUrl223);
				String pageSource223  = new Scanner(url223.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource22:" +pageSource223); 
				//12.Verify that PDP page should be displayed while searching the product via WebID and UPC
				String logErrsmp12="12.Verify that PDP page should be displayed while searching the product via WebID and UPC::: Pass"; 
				logInfo1(logErrsmp12);
				if(pageSource223.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr6991 = "Fail:The Product Page doesn't gets displayed:\n" +pageSource223;
					logInfo(logErr6991);
				}
				else
				{	 
					JSONObject Stream11PDPJson = new JSONObject(pageSource223);
					System.out.println("Stream11PDPJson:" +Stream11PDPJson);	
					String strm11PDPName=Stream11PDPJson.getString("name");
					System.out.println("strm11PDPName:" +strm11PDPName);
					String Pdt11Name=driver.findElement(By.xpath(ProductName)).getText();
					System.out.println("Pdt11Name:" +Pdt11Name);
					if(Pdt11Name.equals(strm11PDPName))
					{
						String logErr411 = "Pass:The Product Name gets matched with the stream call \nProduct name: " +Pdt11Name+ "\nStreamCall Response:" +strm11PDPName; 
						logInfo(logErr411);
					}
					else
					{
						String logErr511 = "Fail:The Product Name doesn't gets matched with the stream call \n Product name: " +Pdt11Name+ "\nStreamCall Response:" +strm11PDPName; 
						logInfo(logErr511);
					} 
					String Pdt11Descriptiontitle=driver.findElement(By.xpath(ProductDescriptionHeader)).getText();
					System.out.println("Pdt11Descriptiontitle:" +Pdt11Descriptiontitle);
					JSONObject strm11iteminfo=Stream11PDPJson.getJSONObject("properties").getJSONObject("iteminfo");
					JSONObject strm11Desc=strm11iteminfo.getJSONArray("description").getJSONObject(0);
					String strm11Descriptiontitle=strm11Desc.getString("value");
					System.out.println("strm11Descriptiontitle:" +strm11Descriptiontitle);
					if(Pdt11Descriptiontitle.equals(strm11Descriptiontitle))
					{
						String logErr815 = "Pass:The Product Description gets matched:\n" +Pdt11Descriptiontitle+ "\n" +strm11Descriptiontitle; 
						logInfo(logErr815);
					}
					else
					{
						String logErr912 = "Fail:The Product Description doesn't gets matched:\n" +Pdt11Descriptiontitle+ "\n" +strm11Descriptiontitle; 
						logInfo(logErr912);
					}
					JSONArray strm11Bultdes=strm11iteminfo.getJSONArray("bulletdescription");
					int Product11bulletlength = driver.findElements(By.xpath(Productdesclength)).size();
					System.out.println("Product11bulletlength:" +Product11bulletlength);	    	
					for(int k9=0,l9=1;k9<strm11Bultdes.length()||l9<=Product11bulletlength;k9++,l9++)
					{
						JSONObject strm11Bultdesc=strm11Bultdes.getJSONObject(k9);
						String	strm11Bulletvalue=strm11Bultdesc.getString("value");
						System.out.println("strm11Bulletvalue:\n" +strm11Bulletvalue);    
						String product11bulletsdescription = driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[3]/div["+l9+"]/div[2]")).getAttribute("innerHTML");
						System.out.println("product11bulletsdescription:\n" +product11bulletsdescription);
						if(product11bulletsdescription.equals(strm11Bulletvalue))
						{
							String logErr619 = "Pass:The ProductDescription Bulletins gets matched:\n" +product11bulletsdescription+ "\n" +strm11Bulletvalue; 
							logInfo(logErr619);
						}
						else
						{
							String logErr728 = "Fail:The ProductDescription Bulletins doesn't gets matched:\n" +product11bulletsdescription+ "\n"+strm11Bulletvalue;
							logInfo(logErr728);
						} 
					}
					String Product11descriptiontitle=driver.findElement(By.xpath(productdescription)).getText();
					System.out.println("Product11descriptiontitle:" +Product11descriptiontitle);
					if(Product11descriptiontitle.equals("DETAILS"))
					{
						String logErr288 = "Pass:The Product Page Titles gets matched:\n"+Product11descriptiontitle;  
						logInfo(logErr288);
					}
					else
					{
						String logErr388 = "Fail:The Product Page Titles doesn't gets matched:\n"+Product11descriptiontitle;
						logInfo(logErr388);
					}
					//Product additional images 
					driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_0']")).click();  
					String Product11primaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
					System.out.println("Product11primaryimageurl:" +Product11primaryimageurl);
					String Product11primaryimagetrim=Product11primaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					System.out.println("Prroduct11primaryimagetrim:" +Product11primaryimagetrim);
					String strm11PDPImage=Stream11PDPJson.getString("image");
					System.out.println("strm11PDPImage:" +strm11PDPImage);
					if(Product11primaryimagetrim.equals(strm11PDPImage))
					{
						String logErr947= "Pass:The Product Page Primary Image gets matched:\n" +Product11primaryimagetrim+ "\n" +strm11PDPImage; 
						logInfo(logErr947);
					}
					else
					{
						String logErr915= "Fail:The Product Page Primary Image doesn't gets matched:\n" +Product11primaryimagetrim+ "\n" +strm11PDPImage; 
						logInfo(logErr915);
					}
					driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click(); 
					int product11additionalimagecount=driver.findElements(By.xpath("//*[@id='id_styleImageDiv']/div/div")).size();  
					System.out.println("product11additionalimagecount:" +product11additionalimagecount);
					//Condition for Additional Images 
					//JSONArray strmadditionalimages= strmiteminfo.getJSONArray("additionalimages");
					//for(int a=0,b=2;a<strmadditionalimages.length()||b<=productadditionalimagecount;a++,b++)
					//{
					//JSONObject strmadditionalobject=strmadditionalimages.getJSONObject(a);
					//String streamadditionalimageurl=strmadditionalobject.getString("image");
					//System.out.println("streamadditionalimageurl:" +streamadditionalimageurl);  
					//driver.findElement(By.xpath("//*[@id='id_styleImageDiv']/div/div["+b+"]")).click();
					//System.out.println("b:" +b);
					//int c=b-1;
					//driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_"+c+"']")).click();
					//String Productsecondaryimageurlzoom=driver.findElement(By.xpath("//*[@id='id_skImageScroller_"+c+"']/img")).getAttribute("src");
					//System.out.println("Productsecondaryimageurlzoom:" +Productsecondaryimageurlzoom);
					//String Productsecondaryimageurlzoomtrim=Productsecondaryimageurlzoom.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					//System.out.println("Productsecondaryimageurlzoomtrim:" +Productsecondaryimageurlzoomtrim); 
					//if(Productsecondaryimageurlzoomtrim.equals(streamadditionalimageurl))
					//{
					//String logErr16= "Pass:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr16);
					//}
					//else
					//{
					//String logErr17= "Fail:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr17);
					//}    
					//driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click();
					//}
					//Color Count and  Selection
					int product11colorcountno=driver.findElements(By.xpath(productcolorcount)).size();
					System.out.println("product11colorcountno:" +product11colorcountno);
					for(int mor=1;mor<=product11colorcountno;mor++)
					{   
						//Need to change the xpath for the more color condition
						if(product11colorcountno>5)
						{
							driver.findElement(By.xpath("//*[@id='id_moreColors']")).click();
							Thread.sleep(1500);             								                        
							driver.findElement(By.xpath("//*[@id='id_moreColorsPopup']/div[2]/div/div["+mor+"]")).click();
							Thread.sleep(1000);
							driver.findElement(By.xpath("//*[@id='id_moreColorsPopup']/div[1]/div[2]")).click();
						}
						else
						{				                 
							driver.findElement(By.xpath("(//*[@class='colorMenuItems updateupc selectedImg'])["+mor+"]")).click();
							Thread.sleep(1000);
						}
						//Size is mostly NA for BCOM:Size Count and Selection
						//int productskucount=driver.findElements(By.xpath(productskusize)).size(); 
						//System.out.println("productskucount:" +productskucount);
						//int totalskucount= productcolorcountno*productskucount;
						//System.out.println("totalskucount:" +totalskucount);
						//String productskusizedetailss=driver.findElement(By.xpath(productskusizedetail)).getAttribute("sizename");
						//System.out.println("productskusizedetailss:" +productskusizedetailss);
						//for(int n=1;n<=productskucount;n++)
						//{
						//driver.findElement(By.xpath(productsizeselection)).click(); 
						//Thread.sleep(6000);
						//driver.findElement(By.xpath("html/body/div[2]/div/div/div/div/div[2]/div[2]/div[2]/div[1]/div["+n+"]")).click();
						Thread.sleep(2000);                         
						String product11UPCvaluecheck= driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div#pdpUPCId.upcid")).getText();
						System.out.println("product1UPCvaluecheck:" +product11UPCvaluecheck);
						String Product11upcvaluechecktrim=product11UPCvaluecheck.substring(product11UPCvaluecheck.indexOf("UPC:")+5);
						System.out.println("Product1upcvaluechecktrim:" +Product11upcvaluechecktrim);				
						//Stream Comparison 
						JSONObject strm11Child=Stream11PDPJson.getJSONObject("children");
						JSONArray strm11Sku=strm11Child.getJSONArray("skus");
						for(int miy=0;miy<strm11Sku.length();miy++)
						{
							JSONObject strm11Skuobj=strm11Sku.getJSONObject(miy);
							JSONObject strm11Prop=strm11Skuobj.getJSONObject("properties");
							String  strm11SkuId=strm11Skuobj.getString("identifier");
							System.out.println("strm11SkuId:" +strm11SkuId);
							if(Product11upcvaluechecktrim.equals(strm11SkuId))
							{
								JSONObject strm11Propr=strm11Prop.getJSONObject("orderinfo");
								String  strm11Proporder=strm11Propr.getString("ordertype");
								System.out.println("strm11Proporder:" +strm11Proporder);
								JSONObject strm11Skuinfo=strm11Prop.getJSONObject("skuinfo");
								JSONObject strm11SkuColor=strm11Skuinfo.getJSONObject("color");
								String  strm11SkuColorvalue=strm11SkuColor.getString("value");
								System.out.println("strm11SkuColorvalue:" +strm11SkuColorvalue);
								String strm11SkuColorlabel=strm11SkuColor.getString("label");
								System.out.println("strm11SkuColorlabel:" +strm11SkuColorlabel);
								//JSONObject strmSkuSize=strmSkuinfo.getJSONObject("size1");  
								//String  strmSkusizevalue=strmSkuSize.getString("value");
								//System.out.println("strmSkusizevalue:" +strmSkusizevalue);
								//String  strmSkusizelabel=strmSkuSize.getString("label");  
								//System.out.println("strmSkusizelabel:" +strmSkusizelabel);
								JSONObject strm11Buyinfo=strm11Prop.getJSONObject("buyinfo");
								JSONObject strm11Pricing=strm11Buyinfo.getJSONObject("pricing");
								String  strm11Qtylimit=strm11Buyinfo.getString("qtylimit");
								System.out.println("strm11Qtylimit:" +strm11Qtylimit);
								JSONArray strm11Prices=strm11Pricing.getJSONArray("prices");
								JSONObject strm11OrstrPrice=strm11Prices.getJSONObject(0);
								String  strm11Origstrprvalue=strm11OrstrPrice.getString("value");
								System.out.println("strm11Origstrprvalue:" +strm11Origstrprvalue);
								JSONObject strm11CurrstrPrice=strm11Prices.getJSONObject(1);
								String strm11Currstrprvalue=strm11CurrstrPrice.getString("value");
								System.out.println("strmCurrstrprvalue:" +strm11Currstrprvalue);
								JSONObject strm11FedstrPrice=strm11Prices.getJSONObject(2);
								String strm11Fedstrprvalue=strm11FedstrPrice.getString("value");
								System.out.println("strm11Fedstrprvalue:" +strm11Fedstrprvalue);
								JSONArray strm11Availability=strm11Buyinfo.getJSONArray("availability");
								JSONObject strm11Availability0=strm11Availability.getJSONObject(0);
								String strm11Onlineinv=strm11Availability0.getString("onlineinventory");
								System.out.println("strm11Onlineinv:" +strm11Onlineinv);
								String strm11Online=strm11Availability0.getString("online");
								System.out.println("strm11Online:" +strm11Online); 
								String Strm11Fedfilinv=strm11Availability0.getString("fedfilinventory");
								System.out.println("Strm11Fedfilinv:" +Strm11Fedfilinv);
								String strm11Fedfil=strm11Availability0.getString("fedfil");
								System.out.println("strm11Fedfil:" +strm11Fedfil);
								Float strm11OrigFloprvalue = Float.parseFloat(strm11Origstrprvalue);
								Float strm11CurrFloprvalue = Float.parseFloat(strm11Currstrprvalue);
								Float strm11FedFloprvalue = Float.parseFloat(strm11Fedstrprvalue); 
								JSONObject strm11Iteminfo=strm11Prop.getJSONObject("iteminfo");
								JSONArray strm11Shipmsgs=strm11Iteminfo.getJSONArray("shippingmessages");
								JSONObject strm11Shippingmsgs=strm11Shipmsgs.getJSONObject(0);
								String strm11Shippingmsgssuspain=strm11Shippingmsgs.getString("suspain");
								System.out.println("strm11Shippingmsgssuspain:" +strm11Shippingmsgssuspain);
								String strm11Shippingmsgsdeliverytype=strm11Shippingmsgs.getString("deliverytype");
								System.out.println("strm11Shippingmsgsdeliverytype:" +strm11Shippingmsgsdeliverytype);
								String strm11Shippingmsgsgiftwrap=strm11Shippingmsgs.getString("giftwrap");
								System.out.println("strm11Shippingmsgsgiftwrap:" +strm11Shippingmsgsgiftwrap);
								String strm11Shippingmsgsshipdate=strm11Shippingmsgs.getString("shipdate");
								System.out.println("strm11Shippingmsgsshipdate:" +strm11Shippingmsgsshipdate);
								String strm11Shippingmsgsshipdays=strm11Shippingmsgs.getString("shipdays");
								System.out.println("strm11Shippingmsgsshipdays:" +strm11Shippingmsgsshipdays); 
								String strm11Shippingmsgsmethod=strm11Shippingmsgs.getString("method");
								System.out.println("strm11Shippingmsgsmethod:" +strm11Shippingmsgsmethod);
								JSONArray strm11Swtch=strm11Iteminfo.getJSONArray("swatches");
								JSONObject strm11Swatches0=strm11Swtch.getJSONObject(0);
								JSONArray strm11Pdtimage=strm11Swatches0.getJSONArray("pdtimage");
								JSONObject strm11Pdtimage0=strm11Pdtimage.getJSONObject(0);
								String strm11Pdtaddseqnumber=strm11Pdtimage0.getString("sequencenumber");
								System.out.println("strm11Pdtaddseqnumber:" +strm11Pdtaddseqnumber);
								String strm11Pdtaddname=strm11Pdtimage0.getString("name");
								System.out.println("strm11Pdtaddname:" +strm11Pdtaddname);
								String strm11Pdtaddimage=strm11Pdtimage0.getString("image");     
								System.out.println("strm11Pdtaddimage:" +strm11Pdtaddimage);
								JSONArray strm11Storeinfo=strm11Prop.getJSONArray("storeinfo");
								System.out.println("strm11Storeinfo:" +strm11Storeinfo);
								String Web11ID=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div.webid")).getText();
								System.out.println("Web11ID:" +Web11ID);
								String Web11IDtrim=Web11ID.substring(Web11ID.indexOf("Web ID: ")+9);
								System.out.println("Web11IDtrim:" +Web11IDtrim);
								String strm11PDPIdentifier= Stream11PDPJson.getString("identifier");
								System.out.println("strm1PDPIdentifier:" +strm11PDPIdentifier);
								if(Web11IDtrim.equalsIgnoreCase(strm11PDPIdentifier))
								{
									String logErr918= "Pass:The Product Page Identifier gets matched:\n" +Web11IDtrim+"\n" +strm11PDPIdentifier; 
									logInfo(logErr918);
								}
								else
								{
									String logErr919= "Fail:The Product Page Identifier doesn't gets matched:\n" +Web11IDtrim+ "\n" +strm11PDPIdentifier; 
									logInfo(logErr919);
								}
								String product11UPCvalue= driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div#pdpUPCId.upcid")).getText();
								System.out.println("product11UPCvalue:" +product11UPCvalue);
								String Product11upcvaluetrim=product11UPCvalue.substring(product11UPCvalue.indexOf("UPC:")+5);
								System.out.println("Product11upcvaluetrim:" +Product11upcvaluetrim);  
								if(Product11upcvaluetrim.equals(strm11SkuId))
								{
									String logErr920= "Pass:The Product UPC value gets matched:\n" +Product11upcvaluetrim+ "\n" +strm11SkuId; 
									logInfo(logErr920);
								}
								else
								{
									String logErr921= "Fail:The Product UPC value doesn't gets matched:\n" +Product11upcvaluetrim+ "\n" +strm11SkuId; 
									logInfo(logErr921);
								}
								String product11pagecolorvalue=driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[1]")).getText();
								System.out.println("product1pagecolorvalue:" +product11pagecolorvalue);
								if(product11pagecolorvalue.equalsIgnoreCase(strm11SkuColorvalue)); 
								{
									String logErr922= "Pass:The Product Page UPC Color gets matched:\n" +product11pagecolorvalue+"\n" +strm11SkuColorvalue; 
									logInfo(logErr922);
								}
								//String productpagesizevalue=driver.findElement(By.xpath(productsizeselection)).getText(); 
								//System.out.println("productpagesizevalue:" +productpagesizevalue);
								//if(productpagesizevalue.equals(strmSkusizevalue))
								//{
								//String logErr24= "Pass:The Product Page UPC Size gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue; 
								//logInfo(logErr24);
								//}
								//else
								//{
								//String logErr25= "Fail:The Product Page UPC Size doesn't gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue;
								//logInfo(logErr25);
								//} 
								String price11valueinpdp=driver.findElement(By.xpath(pricedisplayedinpdp)).getText();
								System.out.println("price11valueinpdp:" +price11valueinpdp);
								//Current and original are same
								if(strm11CurrFloprvalue.compareTo(strm11OrigFloprvalue)==0 && (strm11Fedfil.equals("true")|| strm11Fedfil.equals("false")))
								{
									String logErr928= "Pass:The UPC Contains Current/Store price:\n" +price11valueinpdp+"\n" +strm11CurrFloprvalue; 
									logInfo(logErr928);
								}
								// Current/Store<Original
								else if(strm11CurrFloprvalue.compareTo(strm11OrigFloprvalue)<0 && (strm11Fedfil.equals("true")|| strm11Fedfil.equals("false")))
								{
									String sale11pricevalueinpdp=driver.findElement(By.xpath(pricesale)).getText();
									System.out.println("sale11pricevalueinpdp:" +sale11pricevalueinpdp); 
									String logErr929= "Pass:The UPC Contains Current/Store in Red Original in Black w/ label:\n" +sale11pricevalueinpdp+"\n" +strm11CurrFloprvalue+ "\n" +price11valueinpdp+ "\n" +strm11OrigFloprvalue; 
									logInfo(logErr929);
								}
								// Current/Store>Original
								else if(strm11CurrFloprvalue.compareTo(strm11OrigFloprvalue)>0 && (strm11Fedfil.equals("true")|| strm11Fedfil.equals("false")))
								{
									String logErr930= "Pass:The UPC Contains Current/Store in Black:\n" +price11valueinpdp+"\n" +strm11CurrFloprvalue; 
									logInfo(logErr930);
								}
								// Original Price is null
								else if((strm11OrigFloprvalue==0) && (strm11Fedfil.equals("true")|| strm11Fedfil.equals("false")))
								{
									String logErr931= "Pass:The UPC Contains Current/Store in Black:\n" +price11valueinpdp+"\n" +strm11CurrFloprvalue; 
									logInfo(logErr931);
								}
								// Current price is null and Fedfilavailability price available 
								else if((strm11CurrFloprvalue==0) && (strm11Fedfil.equals("true")))
								{
									String logErr9313= "Pass:The UPC Contains Fedfil price:\n" +price11valueinpdp+"\n" +strm11FedFloprvalue; 
									logInfo(logErr9313);
								}	
								// current price is null and Fedfilavailability price not available
								else if((strm11CurrFloprvalue==0) && (strm11Fedfil.equals("false")))
								{
									String logErr932= "Pass:The UPC Contains Original price:\n" +price11valueinpdp+"\n" +strm11OrigFloprvalue; 
									logInfo(logErr932);
								}
								// Current and original price is null and Fedfilavailability price not available
								else if((strm11CurrFloprvalue==0) && (strm11OrigFloprvalue==0) && (strm11Fedfil.equals("false")))
								{
									String text112="Price is unavailable, please validate price status in mPOS or POS.";
									String text1112="Price is unavailable, please contact an  associate for pricing";
									if(price11valueinpdp.equalsIgnoreCase(text112))
									{
										String logErr933= "Pass:The Product Page contains penny price in Associate Mode:\n" +price11valueinpdp+"\n" +text112; 
										logInfo(logErr933);
									}
									else  
									{
										String logErr934= "Pass:The Product Page contains penny price in Customer Mode:\n" +price11valueinpdp+ "\n" +text1112; 
										logInfo(logErr934);
									}
								}
								else if((strm11CurrFloprvalue==0) && (strm11FedFloprvalue==0))
								{
									String logErr935= "Pass:The Product Page contains Original Price:\n" +price11valueinpdp+ "\n" +strm11OrigFloprvalue; 
									logInfo(logErr935);

								}
								//  Current/store price has penny price 
								else if	((strm11CurrFloprvalue==0.01) && (strm11FedFloprvalue<=0.10)) 			
								{
									String text122="Price is unavailable, please validate price status in mPOS or POS.";
									String text1122="Price is unavailable, please contact an  associate for pricing";
									if(price11valueinpdp.equalsIgnoreCase(text122))
									{
										String logErr936= "Pass:The Product Page contains penny price in Associate Mode:\n" +price11valueinpdp+"\n" +text122; 
										logInfo(logErr936);
									}
									else  
									{
										String logErr834= "Pass:The Product Page contains penny price in Customer Mode:\n" +price11valueinpdp+ "\n" +text1122; 
										logInfo(logErr834);
									}
								}
								if(product11colorcountno<=5)
								{
									String logErr960= "Pass:The Product doesn't contain more colors:\n" +product11colorcountno; 
									logInfo(logErr960);
								}
								else
								{
									String logErr961= "Pass:The Product contains more colors:\n" +product11colorcountno; 
									logInfo(logErr961);
								}
								if(driver.findElement(By.xpath("(//*[@class='addtoOrder'])")).isDisplayed())
								{
									String logErr735= "Pass:The UPC is Available for shipping"; 
									logInfo(logErr735);
									driver.findElement(By.xpath("(//*[@class='addtoOrder'])")).click();								
									Thread.sleep(3000);
									String quick11viewtitlestring=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
									System.out.println("quick11qviewtitlestring:" +quick11viewtitlestring);	
									String quick11viewwebidstring=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
									System.out.println("quick11viewwebidstring:" +quick11viewwebidstring);
									String quick11viewwebidstringtrim=quick11viewwebidstring.substring(quick11viewwebidstring.indexOf("(")+1,quick11viewwebidstring.indexOf(")"));
									System.out.println("quick11viewwebidstringtrim:" +quick11viewwebidstringtrim);
									String quick11viewpricestring=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
									System.out.println("quick11viewpricestring:" +quick11viewpricestring);			
									String quick11viewcolorstring=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
									System.out.println("quick11viewcolorstring:" +quick11viewcolorstring);		
									String quick11viewimageurl=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
									System.out.println("quick1viewimageurl:" +quick11viewimageurl);
									String quick11viewimageurltrim=quick11viewimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
									System.out.println("quick1viewimageurltrim:" +quick11viewimageurltrim);			
									if(quick11viewwebidstringtrim.equals(Web11ID)&&quick11viewpricestring.equals(price11valueinpdp)&&quick11viewcolorstring.equalsIgnoreCase(product11pagecolorvalue)&&quick11viewimageurltrim.equals(Product11primaryimagetrim))
									{
										String logErr1381= "Pass:The details in the AddtoBag overlay value gets matched:\n" +quick11viewtitlestring+"\n" +Pdt11Name+ "\n" +quick11viewwebidstringtrim+ "\n" +Web11ID+ "\n" +quick11viewpricestring+ "\n" +price11valueinpdp+ "\n" +quick11viewcolorstring+ "\n" +product11pagecolorvalue+ "\n" +quick11viewimageurltrim+ "\n" +Product11primaryimagetrim;
										logInfo(logErr1381);  
									}
									else
									{
										String logErr1391= "Fail:The details in the AddtoBag overlay value doesn't gets matched:\n" +quick11viewtitlestring+"\n" +Pdt11Name+ "\n" +quick11viewwebidstringtrim+ "\n" +Web11ID+ "\n" +quick11viewpricestring+ "\n" +price11valueinpdp+ "\n" +quick11viewcolorstring+ "\n" +product11pagecolorvalue+ "\n" +quick11viewimageurltrim+ "\n" +Product11primaryimagetrim;
										logInfo(logErr1391);  
									}				
									driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();
								}
								else
								{
									String logErr1361= "Pass:The UPC is not Available for shipping";
									logInfo(logErr1361);
								}						
								//Check others stores functionalities 
								Thread.sleep(2500);
								driver.findElement(By.xpath(productotherstores)).click();
								Thread.sleep(5000);
								//Check other Stores stream call data 
								String pdt11upcvalue="http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+Product11upcvaluetrim+"?type=UPCWithNearbyStores&storeid=28&campaignId=383";
								URL url118 = new URL(pdt11upcvalue);
								String pageSource118  = new Scanner(url118.openConnection().getInputStream()).useDelimiter("\\Z").next();
								System.out.println("pageSource118:" +pageSource118);
								JSONObject Stream11PDPUpccheckotherstores=new JSONObject(pageSource118);
								JSONObject stream11PDPUpcchild=Stream11PDPUpccheckotherstores.getJSONObject("children");
								JSONArray stream11PDPUPCSku=stream11PDPUpcchild.getJSONArray("skus");
								for(int q16=0;q16<stream11PDPUPCSku.length();q16++)
								{
									JSONObject stream11PDPUPCSkuobj=stream11PDPUPCSku.getJSONObject(q16);
									String stream11PDPUPCidentifier= stream11PDPUPCSkuobj.getString("identifier");
									System.out.println("stream11PDPUPCidentifier:" +stream11PDPUPCidentifier);
									if(stream11PDPUPCidentifier.equals(Product11upcvaluetrim))
									{
										JSONObject stream11PropUPC=stream11PDPUPCSkuobj.getJSONObject("properties");
										JSONArray stream11PDPStoreinfo=stream11PropUPC.getJSONArray("storeinfo"); 
										int product11storescounts=driver.findElements(By.xpath(productstoresize)).size(); 
										System.out.println("product11storescounts:" +product11storescounts); 
										for(int o11=0,p11=1;o11<stream11PDPStoreinfo.length()||p11<=product11storescounts;o11++,p11++)
										{
											JSONObject strm11Storeinfo1=stream11PDPStoreinfo.getJSONObject(o11);
											String strm11Storeinfophone=strm11Storeinfo1.getString("phone");
											System.out.println("strm11Storeinfophone:" +strm11Storeinfophone);
											String strm11StoreinfoSequenceno=strm11Storeinfo1.getString("sequencenumber");
											System.out.println("strm11StoreinfoSequenceno:" +strm11StoreinfoSequenceno);
											String strm11StoreinfoInventory=strm11Storeinfo1.getString("inventory");
											System.out.println("strm11StoreinfoInventory:" +strm11StoreinfoInventory);
											String strm11StoreinfoName=strm11Storeinfo1.getString("name");
											System.out.println("strm1StoreinfoName:" +strm11StoreinfoName);
											String strm11StoreinfoZipcode=strm11Storeinfo1.getString("zipcode");
											System.out.println("strm11StoreinfoZipcode:" +strm11StoreinfoZipcode);
											String strm11StoreinfoState=strm11Storeinfo1.getString("state");
											System.out.println("strm11StoreinfoState:" +strm11StoreinfoState);
											String strm11StoreinfoAddress1=strm11Storeinfo1.getString("address1");
											System.out.println("str1mStoreinfoAddress1:" +strm11StoreinfoAddress1);
											String strm11StoreinfoAddress2=strm11Storeinfo1.getString("address2");
											System.out.println("strm1StoreinfoAddress2:" +strm11StoreinfoAddress2);
											String strm11StoreinfoIdentifier=strm11Storeinfo1.getString("identifier");
											System.out.println("strm1StoreinfoIdentifier:" +strm11StoreinfoIdentifier);
											String strm11StoreinfoCity=strm11Storeinfo1.getString("city");
											System.out.println("strm11StoreinfoCity:" +strm11StoreinfoCity);
											String strm11StoreinfoNameconcat=strm11StoreinfoName+strm11StoreinfoIdentifier;
											System.out.println("strm11StoreinfoNameconcat:" +strm11StoreinfoNameconcat);
											String strm11storeinfocombined=strm11StoreinfoNameconcat.replaceAll(""+strm11StoreinfoIdentifier+".*","")+" "+"("+strm11StoreinfoIdentifier+")";
											System.out.println("strm11storeinfocombined:" +strm11storeinfocombined);
											String check11availabilityheadertitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p11+"]/div[1]/div/div[2]/div")).getText();
											System.out.println("check1availabilityheadertitle:" +check11availabilityheadertitle);							                              
											driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p11+"]/div[1]/div/div[2]/div")).click();
											Thread.sleep(2000);
											String check11availabilitystreetNametitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p11+"]/div[2]/div/div[1]")).getText();
											System.out.println("check1availabilitystreetNametitle:" +check11availabilitystreetNametitle);				                          
											String check11availabilitystoreLocationtitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p11+"]/div[2]/div/div[2]")).getText();
											System.out.println("check1availabilitystoreLocationtitle:" +check11availabilitystoreLocationtitle);
											String check11availabilitystorePhonetitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p11+"]/div[2]/div/div[3]")).getText();
											System.out.println("check1availabilitystorePhonetitle:" +check11availabilitystorePhonetitle);
											driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p11+"]/div[1]/div/div[2]/div")).click();	
											if(check11availabilityheadertitle.equals(strm11storeinfocombined)&&check11availabilitystreetNametitle.equals(strm11StoreinfoAddress1)&&check11availabilitystoreLocationtitle.equals(strm11StoreinfoAddress2)&&check11availabilitystorePhonetitle.equals(strm11Storeinfophone))
											{
												String logErr1127= "Pass:The Product Page UPC Checkother Stores value gets matched:\n" +check11availabilityheadertitle+"\n" +strm11storeinfocombined+ "\n" +check11availabilitystreetNametitle+ "\n" +strm11StoreinfoAddress1+ "\n" +check11availabilitystoreLocationtitle+ "\n" +strm11StoreinfoAddress2+ "\n" +check11availabilitystorePhonetitle+ "\n" +strm11Storeinfophone;  
												logInfo(logErr1127);
											}	
											else
											{
												String logErr1128= "Fail:The Product Page UPC Checkother Stores value doesn't gets matched:\n" +check11availabilityheadertitle+"\n" +strm11storeinfocombined+ "\n" +check11availabilitystreetNametitle+ "\n" +strm11StoreinfoAddress1+ "\n" +check11availabilitystoreLocationtitle+ "\n" +strm11StoreinfoAddress2+ "\n" +check11availabilitystorePhonetitle+ "\n" +strm11Storeinfophone;
												logInfo(logErr1128);
											}
											Thread.sleep(5000);
											String logErr1137= "-----------------------------------------------------------------------------";
											logInfo(logErr1137);
											System.out.println("-----------------------------------------");  
										}                                 
										driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent.ui-content div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_pdpSeeAllAvailable.SeeAllAvailable.cls_midleAlign.kioskOtherStore div.otherStlayoutContainer div.otherStoreCloseBtnDiv")).click();    
										driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent.ui-content div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#reviewCont div.pdpAddToFavt.pdpfavList.pdpfavList_icon")).click();
									}
								}
							}
						}
					} 
				} 
			} 
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 			
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void shoppingBagValidation() 
	{
		try
		{
			try
			{
				//Shopping Bag Functionalities 		
				String logErr307 ="Shopping Bag Page Functionalities"; 
				logInfo(logErr307);
				String logErr308="----------------------------------"; 
				logInfo(logErr308);
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[5]/div[1]")).click();
				Thread.sleep(5000);
				//Checking the Shopping Bag Status  
				String shoppingbagfootericoncount=driver.findElement(By.xpath("//*[@id='id_skrlMyBagCount']")).getText();
				System.out.println("shoppingbagfootericoncount:" +shoppingbagfootericoncount);
				int shoppingbagtotalproductcount=driver.findElements(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div")).size();
				System.out.println("shoppingbagtotalproductcount:" +shoppingbagtotalproductcount);
				//19.Verify that  added items to the bag should be displayed in the "Order bag" page
				String logErrsmp19 ="19,Verify that  added items to the bag should be displayed in the Order bag page, Pass"; 
				logInfo1(logErrsmp19);
				for(int c4=1;c4<=shoppingbagtotalproductcount;c4++)
				{
					String producupccodevalue=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]")).getAttribute("upccode");
					System.out.println("producupccodevalue:" +producupccodevalue);
					String shoppingbagproductname=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[1]")).getText();
					System.out.println("shoppingbagproductname:" +shoppingbagproductname); 
					String shoppingbagproductcolors=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[2]")).getText();
					System.out.println("shoppingbagproductcolors:" +shoppingbagproductcolors);
					//Color name label
					//String shoppingbagproductcolorsname=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[2]/text()")).getText();
					//System.out.println("shoppingbagproductcolorsname:" +shoppingbagproductcolorsname);
					//String shoppingbagproductsize=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[3]")).getText();
					//System.out.println("shoppingbagproductsize:" +shoppingbagproductsize);
					//Size name label
					//String shoppingbagproductsizetext=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[1]/div[2]/div[3]/text()")).getText();
					//System.out.println("shoppingbagproductsizetext:" +shoppingbagproductsize);
					String shoppingbagproductprice=(driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[2]/div"))).getText();
					System.out.println("shoppingbagproductprice:" +shoppingbagproductprice);     //*[@class='skrlCustomComboTxt'])[1]
					String shoppingbagproductpricetrimmed=shoppingbagproductprice.replaceAll(".*\\$","");
					System.out.println("shoppingbagproductpricetrimmed: " +shoppingbagproductpricetrimmed);
					float shoppingbagproductpricefloat=Float.parseFloat(shoppingbagproductpricetrimmed);    
					System.out.println("shoppingbagproductpricefloat:" +shoppingbagproductpricefloat);        
					String shoppingbagupcqtyvalues=(driver.findElement(By.xpath("//*[@id='id_skrlCustomComboCont_"+producupccodevalue+"']/div["+c4+"]"))).getText();
					System.out.println("shoppingbagupcqtyvalues:" +shoppingbagupcqtyvalues);
					int shoppingbagupcqtyvaluesint=Integer.parseInt(shoppingbagupcqtyvalues);
					System.out.println("shoppingbagupcqtyvaluesint:" +shoppingbagupcqtyvaluesint);
					float totalupcproductvalues= shoppingbagproductpricefloat * shoppingbagupcqtyvaluesint; 
					String shoppingbagpageproducttotalprice=driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div["+c4+"]/div[4]/div[1]")).getText();
					System.out.println("shoppingbagpageproducttotalprice:" +shoppingbagpageproducttotalprice);
					String shoppingbagpageproducttotalpricetrimmed=shoppingbagpageproducttotalprice.replaceAll(".*\\$","");
					System.out.println("shoppingbagpageproducttotalpricetrimmed: " +shoppingbagpageproducttotalpricetrimmed);
					float shoppingbagpageproducttotalpricefloat=Float.parseFloat(shoppingbagpageproducttotalpricetrimmed);    
					System.out.println("shoppingbagpageproducttotalpricefloat:" +shoppingbagpageproducttotalpricefloat);
					if(totalupcproductvalues==shoppingbagpageproducttotalpricefloat)
					{
						String logErr160= "Pass:The Productdetails,Price,Qty and Totalprice values gets matched in the shopping bag:\n" +producupccodevalue+ "\n" +shoppingbagproductname+ "\n"  +shoppingbagproductcolors+ "\n"  +shoppingbagproductprice+ "\n" +shoppingbagupcqtyvalues+ "\n" +shoppingbagpageproducttotalprice;
						logInfo(logErr160);	 
						String logErr162= "------------------------------------------------------------------------";
						logInfo(logErr162);
					}
					else
					{
						String logErr161= "Fail:The Productdetails,Price,Qty and Totalprice values doesn't gets matched in the shopping bag:\n" +producupccodevalue+ "\n" +shoppingbagproductname+ "\n"  +shoppingbagproductcolors+ "\n"  +shoppingbagproductprice+ "\n" +shoppingbagupcqtyvalues+ "\n" +shoppingbagpageproducttotalprice;   
						logInfo(logErr161);
						String logErr163= "------------------------------------------------------------------------";
						logInfo(logErr163);
					}
					String shoppingbagproductstotalpricevalues=driver.findElement(By.xpath("//*[@id='id_skrlSubTotalAmount']")).getText(); 
					System.out.println("shoppingbagproductstotalpricevalues: " +shoppingbagproductstotalpricevalues); 
					//Shopping bag suspend
					driver.findElement(By.xpath("//*[@id='id_sklrPrdtListScroll']/div/div/div[4]/div[2]")).click();
					Thread.sleep(5000);		
					//20.Verify that while selecting the ""Clear All Items & Remove Items" in the shopping bag page, the item should be removed from the shopping bag page 
					String logErrsmp20 ="20,Verify that while selecting the Clear All Items & Remove Items in the shopping bag page the item should be removed from the shopping bag page, Pass"; 
					logInfo1(logErrsmp20);
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer div.startNewOrderDiv.suspendwaringCancelBtn div.suspendOverlayConfrimBtns.suspendwaringCancelBtnImg div.suspendBtnText")).click();   
					Thread.sleep(500);
					driver.findElement(By.xpath("//*[@id='id_skrlSusspendBtn']")).click();
					Thread.sleep(3000);
					//21.Verify that  while selecting the Suspend /Check out option, the associate login page should be displayed if the user doesn't gets logged-in before
					String logErrsmp21 ="21,Verify that  while selecting the Suspend/Check out option the associate login page should be displayed if the user doesn't gets logged-in before, Pass"; 
					logInfo1(logErrsmp21);
					driver.findElement(By.xpath("//*[@id='id_loginForm_AID']")).sendKeys("01002158");			
					driver.findElement(By.xpath("//*[@id='id_loginForm_PIN']")).sendKeys("6599");
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div.cls_mamLoginOverlayMask.currentlyActive div#id_mamLoginForm.cls_mamLoginForm div.cls_mamLoginFormContainer form#id_mamLoginForm.cls_mamLoginForm div.cls_mamLoginPage div.cls_mamLoginInputData div.cls_mamLoginDataContent div.cls_mamLoginFormSigninCancelDiv div.cls_mamSignin")).click();
					Thread.sleep(5000);	
					//22.verify that while selecting the Suspend /Check out option, the Confimation popup should be enabled when the user is alreeady logged-in
					String logErrsmp22 ="22,verify that while selecting the Suspend /Check out option the Confimation popup should be enabled when the user is alreeady logged-in, Pass"; 
					logInfo1(logErrsmp22);
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer.skrlSuspendSwitchUserBtn div.startNewOrderDiv.suspendwaringCancelBtn div.suspendOverlayConfrimBtns.suspendwaringCancelBtnImg div.suspendBtnText")).click();
					Thread.sleep(2000);
					//Apply Coupon Functionalities 
					driver.findElement(By.xpath("//*[@id='id_skrlApplyCopn']")).click();  
					Thread.sleep(2000);
					driver.findElement(By.xpath("//*[@id='id_favPromoScanTxtInput']")).click(); 
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='id_favPromoScanTxtInput']")).sendKeys("00000000000315040005");
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[@id='id_favPromoSubmit']")).click();  
					Thread.sleep(60000);
					driver.findElement(By.xpath("//*[@id='id_errorPopUpContainer']")).isDisplayed();
					String applycouponerror=driver.findElement(By.xpath("//*[@id='id_errorPopUpContainer']/div[2]")).getText();
					System.out.println("applycouponerror:" +applycouponerror);
					//25.Verify that "Apply Coupon"  functionalities should be worked as expected 
					String logErrsmp25 ="25,Verify that Apply Coupon functionalities should be worked as expected, Fail\n" +applycouponerror;
					logInfo1(logErrsmp25);
					Thread.sleep(1000);
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_9.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent.ui-content div#Shopping_Bag_Page.page.lyt_cont_div div#id_myBagContent.cls_mamMyBagPage div.skrlorderPageKisok div#id_errorPopUpContainer.errorPopUpContainer div.errorPopupOkBtn")).click();
					Thread.sleep(3000);	
					driver.findElement(By.xpath("//*[@id='id_skrlSusspendBtn']")).click();
					Thread.sleep(2000);
					//Switch User Account
					//driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer.skrlSuspendSwitchUserBtn div.startNewOrderDiv.suspendSwitchUserBtn div.suspendOverlayConfrimBtns.suspendSwitchUserBtnImg")).click();
					//Thread.sleep(1000);	
					//driver.findElement(By.xpath("//*[@id='id_loginForm_AID']")).sendKeys("71253659");
					//driver.findElement(By.xpath("//*[@id='id_loginForm_PIN']")).sendKeys("0947");
					//driver.findElement(By.xpath("//*[@id='id_mamLoginForm']/div/div[2]/div/div[3]/div[2]")).click();
					//Thread.sleep(1000); 
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer.skrlSuspendSwitchUserBtn div.startNewOrderDiv.suspendWaringOkBtn div.startNewOrderImg.suspendWaringOkBtnImg div.suspendBtnText")).click();				
					Thread.sleep(10000);  
					if(driver.findElement(By.className("sksuspendOverlayContainer")).isDisplayed())
					{
						System.out.println("Yes");	

					}
					else
					{
						System.out.println("Nooo");

					}
					//23.verify that while selecting the "Continue" button in the Suspend confirmation popup, the "Suspend Transaction/Suspend Failed" overlay  should  be enabled
					String logErrsmp23 ="23,verify that while selecting the Suspend /Check out option the Confimation popup should be enabled when the user is alreeady logged-in, Pass"; 
					logInfo1(logErrsmp23);
					String suspendresponsedetails=driver.findElement(By.className("suspendOverlayHeadingDiv")).getText();				
					System.out.println("suspendresponsedetails:" +suspendresponsedetails);	
					Thread.sleep(5000);
					if(suspendresponsedetails.equals("TRANSACTION SUSPENDED"))
					{
						String suspendresponsedetailss=driver.findElement(By.className("suspendOverlayHeadingDiv")).getText();				
						System.out.println("suspendresponsedetailss:" +suspendresponsedetailss);	
						String suspendTransactionCode=driver.findElement(By.className("suspendTransactionCodeDiv")).getText();
						System.out.println("suspendTransactionCode:" +suspendTransactionCode);
						String suspendTransactionSubtotalvalue=driver.findElement(By.className("suspendTransactionSubtotalDiv")).getText();
						System.out.println("suspendTransactionSubtotalvalue:" +suspendTransactionSubtotalvalue);
						String suspendTransactionMSGdetails=driver.findElement(By.className("suspendTransactionMsgDiv")).getText();
						System.out.println("suspendTransactionMSGdetails:" +suspendTransactionMSGdetails);
						Thread.sleep(1000);
						driver.findElement(By.cssSelector(".suspendBtnText.startNewOrderText")).click();			
						String logErr165= "Pass:The Products gets Suspended Successfully:\n" +suspendresponsedetailss+ "\n" +suspendTransactionCode+ "\n" +suspendTransactionSubtotalvalue+ "\n" +suspendTransactionMSGdetails;    
						logInfo(logErr165);
					}
					else
					{ 
						String suspendfailuredataerror=driver.findElement(By.className("suspendOverlayHeadingDiv")).getText();
						System.out.println("suspendfailuredataerror:" +suspendfailuredataerror);
						String suspendfailuretranserror=driver.findElement(By.className("suspendTransactionMsgDiv")).getText();
						System.out.println("suspendfailuretranserror:" +suspendfailuretranserror);
						Thread.sleep(1000);
						driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div.overLayContainer.skrlOverlay div.sksuspendOverlayContainer div.suspendOverLayBtnContainer div.startNewOrderDiv.suspendErrorOkBtn div.startNewOrderImg.suspendErrorOkBtnImg div.suspendBtnText")).click();
						String logErr164= "Fail:The Product Suspend Transaction Gets Failed:\n" +suspendfailuredataerror+ "\n" +suspendfailuretranserror;    
						logInfo(logErr164);			
					}   
				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void settingPageValidation() 
	{
		try
		{
			//Settings Page Functionalities
			String logErr903 ="Settings Page Functionalities"; 
			logInfo(logErr903);
			String logErr904="----------------------------------"; 
			logInfo(logErr904);
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]/div")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='id_mamFooterSubMenuScroll']/div[4]/div[1]")).click();
			Thread.sleep(1000);
			//35.Verify that the footer should be shown in all pages.
			String logErrsmp35="35.Verify that the footer should be shown in all pages::: Pass"; 
			logInfo1(logErrsmp35);
			//32.Verify that  selecting the �Settings� option from the Menu section, the settings page should be displayed
			String logErrsmp32="32,Verify that  selecting the Settings option from the Menu section the settings page should be displayed, Pass"; 
			logInfo1(logErrsmp32);
			String settingsheader=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[1]")).getText();
			System.out.println("settingsheader:" +settingsheader);
			String settingstoreid=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[2]/div[1]")).getText();
			System.out.println("settingsheader:" +settingstoreid);
			String settingsfob=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[1]")).getText();
			System.out.println("settingsfob:" +settingsfob);
			String settingssellingarea=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[2]")).getText();
			System.out.println("settingssellingarea:" +settingssellingarea);
			String settingscheckout=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[3]")).getText();
			System.out.println("settingscheckout:" +settingscheckout);
			String settingsassistedcheckout=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[4]")).getText();
			System.out.println("settingsassistedcheckout:" +settingsassistedcheckout);
			String settingssearchsuggestion=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[3]/div[5]")).getText();
			System.out.println("settingssearchsuggestion:" +settingssearchsuggestion);
			String settingsdeviceid=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[2]/div[2]")).getText();
			System.out.println("settingsdeviceid:" +settingsdeviceid);		
			String settingscatalogname=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[1]")).getText();
			System.out.println("settingscatalogname:" +settingscatalogname);
			String settingsserviceversion=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[2]")).getText();
			System.out.println("settingsserviceversion:" +settingsserviceversion);		
			String settingsappversion=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[3]")).getText();
			System.out.println("settingsappversion:" +settingsappversion);
			String settingsenvironment=driver.findElement(By.xpath("//*[@id='id_settingsPageContainer']/div[4]/div[4]")).getText();
			System.out.println("settingsenvironment:" +settingsenvironment);
			String logErr450 ="Settings Page:" +settingsheader+ "\n" +settingstoreid+ "\n"  +settingsfob+ "\n"  +settingssellingarea+ "\n"  +settingscheckout+ "\n" +settingsassistedcheckout+ "\n"  +settingssearchsuggestion+ "\n" +settingssearchsuggestion+ "\n" +settingsdeviceid+ "\n" +settingscatalogname+ "\n" +settingsserviceversion+ "\n"  +settingsappversion+ "\n" +settingsenvironment;
			logInfo(logErr450);				
			//33.Verify that  �Settings� page should contain �Store Id,Device ID,FOB, Catalog Name,Selling Area,Checkout,ServiceVersion, Assisted Checkout, AppVersion, Search Suggestion and Environment
			String logErrsmp33 ="33,Verify that  �Settings� page should contain Store Id Device ID FOB  Catalog Name Selling Area Checkout ServiceVersion  Assisted Checkout  AppVersion Search Suggestion and Environment, Pass"; 
			logInfo1(logErrsmp33);
			String settingsassistedcheckouttri=settingsassistedcheckout.replaceAll(".*\\:","");
			System.out.println("settingsassistedcheckouttri: " +settingsassistedcheckouttri);
			String settingsassistedcheckouttrim=settingsassistedcheckouttri.trim();
			if(settingsassistedcheckouttrim.equals("OFF") && driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[5]/div")).isDisplayed())
			{
				//26.Verify that "Checkout" & "Guest Checkout" functionalities should be as expected as per the truth table 
				String logErrsmp26 ="26,Verify that Checkout & Guest Checkout functionalities should be as expected as per the truth table, Pass"; 
				logInfo1(logErrsmp26);
			}
			else
			{
				//26.Verify that "Checkout" & "Guest Checkout" functionalities should be as expected as per the truth table
				String logErrsmf26 ="26,Verify that Checkout & Guest Checkout functionalities should be as expected as per the truth table, Fail"; 
				logInfo1(logErrsmf26);
			}
			Thread.sleep(1000);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void searchByWebID() 
	{
		try
		{
			try
			{
				//Search By WebId
				String logErr315 ="Search By WebID"; 
				logInfo(logErr315);
				String logErr314="------------------"; 
				logInfo(logErr314);
				driver.findElement(By.xpath("(//*[@class='cls_mamFSrch'])")).click();	
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).click();	
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("1426336");  
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys(Keys.ENTER);
				Thread.sleep(8000);
				String currentURL = driver.getCurrentUrl();
				System.out.println("currentURL:" +currentURL);  
				String pdtId1=currentURL.substring(currentURL.indexOf("pdt_id=")+7,currentURL.indexOf("&type"));
				System.out.println("pdtId:" +pdtId1); 
				String pdtUrl22 = "http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+pdtId1+"?type=ID&storeid=28&campaignId=383";
				URL url22  = new URL(pdtUrl22);
				String pageSource22  = new Scanner(url22.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource22:" +pageSource22); 
				if(pageSource22.contains("HTTP/1.1 500 Internal Server Error"))
				{
					String logErr612 = "Fail:The Product Page doesn't gets displayed:\n" +pageSource22;
					logInfo(logErr612);
				}
				else
				{	 
					JSONObject Stream1PDPJson = new JSONObject(pageSource22);
					System.out.println("Stream1PDPJson:" +Stream1PDPJson);	
					String strm1PDPName=Stream1PDPJson.getString("name");
					System.out.println("strm1PDPName:" +strm1PDPName);
					String Pdt1Name=driver.findElement(By.xpath(ProductName)).getText();
					System.out.println("Pdt1Name:" +Pdt1Name);
					if(Pdt1Name.equals(strm1PDPName))
					{
						String logErr412 = "Pass:The Product Name gets matched with the stream call \nProduct name: " +Pdt1Name+ "\nStreamCall Response:" +strm1PDPName; 
						logInfo(logErr412);
					}
					else
					{
						String logErr512 = "Fail:The Product Name doesn't gets matched with the stream call \n Product name: " +Pdt1Name+ "\nStreamCall Response:" +strm1PDPName; 
						logInfo(logErr512);
					} 
					String Pdt1Descriptiontitle=driver.findElement(By.xpath(ProductDescriptionHeader)).getText();
					System.out.println("Pdt1Descriptiontitle:" +Pdt1Descriptiontitle);
					JSONObject strm1iteminfo=Stream1PDPJson.getJSONObject("properties").getJSONObject("iteminfo");
					JSONObject strm1Desc=strm1iteminfo.getJSONArray("description").getJSONObject(0);
					String strm1Descriptiontitle=strm1Desc.getString("value");
					System.out.println("strm1Descriptiontitle:" +strm1Descriptiontitle);
					if(Pdt1Descriptiontitle.equals(strm1Descriptiontitle))
					{
						String logErr812 = "Pass:The Product Description gets matched:\n" +Pdt1Descriptiontitle+ "\n" +strm1Descriptiontitle; 
						logInfo(logErr812);
					}
					else
					{
						String logErr912 = "Fail:The Product Description doesn't gets matched:\n" +Pdt1Descriptiontitle+ "\n" +strm1Descriptiontitle; 
						logInfo(logErr912);
					}
					JSONArray strm1Bultdes=strm1iteminfo.getJSONArray("bulletdescription");
					int Product1bulletlength = driver.findElements(By.xpath(Productdesclength)).size();
					System.out.println("Product1bulletlength:" +Product1bulletlength);	    	
					for(int k6=0,l6=1;k6<strm1Bultdes.length()||l6<=Product1bulletlength;k6++,l6++)
					{
						JSONObject strm1Bultdesc=strm1Bultdes.getJSONObject(k6);
						String	strm1Bulletvalue=strm1Bultdesc.getString("value");
						System.out.println("strm1Bulletvalue:\n" +strm1Bulletvalue);    
						String product1bulletsdescription = driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[3]/div["+l6+"]/div[2]")).getAttribute("innerHTML");
						System.out.println("product1bulletsdescription:\n" +product1bulletsdescription);
						if(product1bulletsdescription.equals(strm1Bulletvalue))
						{
							String logErr612 = "Pass:The ProductDescription Bulletins gets matched:\n" +product1bulletsdescription+ "\n" +strm1Bulletvalue; 
							logInfo(logErr612);
						}
						else
						{
							String logErr712 = "Fail:The ProductDescription Bulletins doesn't gets matched:\n" +product1bulletsdescription+ "\n"+strm1Bulletvalue;
							logInfo(logErr712);
						} 
					}
					String Product1descriptiontitle=driver.findElement(By.xpath(productdescription)).getText();
					System.out.println("Product2descriptiontitle:" +Product1descriptiontitle);
					if(Product1descriptiontitle.equals("DETAILS"))
					{
						String logErr212 = "Pass:The Product Page Titles gets matched:\n"+Product1descriptiontitle;  
						logInfo(logErr212);
					}
					else
					{
						String logErr319= "Fail:The Product Page Titles doesn't gets matched:\n"+Product1descriptiontitle;
						logInfo(logErr319);
					}
					//Product additional images 
					driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_0']")).click();  
					String Product1primaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
					System.out.println("Product1primaryimageurl:" +Product1primaryimageurl);
					String Product1primaryimagetrim=Product1primaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					System.out.println("Prroduct1primaryimagetrim:" +Product1primaryimagetrim);
					String strm1PDPImage=Stream1PDPJson.getString("image");
					System.out.println("strm1PDPImage:" +strm1PDPImage);
					if(Product1primaryimagetrim.equals(strm1PDPImage))
					{
						String logErr147= "Pass:The Product Page Primary Image gets matched:\n" +Product1primaryimagetrim+ "\n" +strm1PDPImage; 
						logInfo(logErr147);
					}
					else
					{
						String logErr157= "Fail:The Product Page Primary Image doesn't gets matched:\n" +Product1primaryimagetrim+ "\n" +strm1PDPImage; 
						logInfo(logErr157);
					}
					driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click(); 
					int product1additionalimagecount=driver.findElements(By.xpath("//*[@id='id_styleImageDiv']/div/div")).size();  
					System.out.println("product1additionalimagecount:" +product1additionalimagecount);
					//Condition for Additional Images 
					//JSONArray strmadditionalimages= strmiteminfo.getJSONArray("additionalimages");
					//for(int a=0,b=2;a<strmadditionalimages.length()||b<=productadditionalimagecount;a++,b++)
					//{
					//JSONObject strmadditionalobject=strmadditionalimages.getJSONObject(a);
					//String streamadditionalimageurl=strmadditionalobject.getString("image");
					//System.out.println("streamadditionalimageurl:" +streamadditionalimageurl);  
					//driver.findElement(By.xpath("//*[@id='id_styleImageDiv']/div/div["+b+"]")).click();
					//System.out.println("b:" +b);
					//int c=b-1;
					//driver.findElement(By.xpath("//*[@id='id_pdtLargeImg_"+c+"']")).click();
					//String Productsecondaryimageurlzoom=driver.findElement(By.xpath("//*[@id='id_skImageScroller_"+c+"']/img")).getAttribute("src");
					//System.out.println("Productsecondaryimageurlzoom:" +Productsecondaryimageurlzoom);
					//String Productsecondaryimageurlzoomtrim=Productsecondaryimageurlzoom.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
					//System.out.println("Productsecondaryimageurlzoomtrim:" +Productsecondaryimageurlzoomtrim); 
					//if(Productsecondaryimageurlzoomtrim.equals(streamadditionalimageurl))
					//{
					//String logErr16= "Pass:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr16);
					//}
					//else
					//{
					//String logErr17= "Fail:The Product Page Additional Image gets matched:\n" +Productsecondaryimageurlzoomtrim+ "\n" +streamadditionalimageurl; 
					//logInfo(logErr17);
					//}    
					//driver.findElement(By.xpath("//*[@id='id_pinchZoomOverlay']/div[2]")).click();
					//}
					//Color Count and  Selection
					int product1colorcountno=driver.findElements(By.xpath(productcolorcount)).size();
					System.out.println("product1colorcountno:" +product1colorcountno);
					for(int moi=1;moi<=product1colorcountno;moi++)
					{   
						//Need to change the xpath for the more color condition
						if(product1colorcountno>5)
						{
							driver.findElement(By.xpath("//*[@id='id_moreColors']")).click();
							Thread.sleep(1500);             								                        
							driver.findElement(By.xpath("//*[@id='id_moreColorsPopup']/div[2]/div/div["+moi+"]")).click();
							Thread.sleep(1000);
							driver.findElement(By.xpath("//*[@id='id_moreColorsPopup']/div[1]/div[2]")).click();
						}
						else              
						{				                 
							driver.findElement(By.xpath("//*[@id='id_colorContainer']/div[2]/div["+moi+"]")).click();
							Thread.sleep(1000);
						}
						//Size is mostly NA for BCOM:Size Count and Selection
						//int productskucount=driver.findElements(By.xpath(productskusize)).size(); 
						//System.out.println("productskucount:" +productskucount);
						//int totalskucount= productcolorcountno*productskucount;
						//System.out.println("totalskucount:" +totalskucount);
						//String productskusizedetailss=driver.findElement(By.xpath(productskusizedetail)).getAttribute("sizename");
						//System.out.println("productskusizedetailss:" +productskusizedetailss);
						//for(int n=1;n<=productskucount;n++)
						//{
						//driver.findElement(By.xpath(productsizeselection)).click(); 
						//Thread.sleep(6000);
						//driver.findElement(By.xpath("html/body/div[2]/div/div/div/div/div[2]/div[2]/div[2]/div[1]/div["+n+"]")).click();
						Thread.sleep(2000);                         
						String product1UPCvaluecheck= driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div#pdpUPCId.upcid")).getText();
						System.out.println("product1UPCvaluecheck:" +product1UPCvaluecheck);
						String Product1upcvaluechecktrim=product1UPCvaluecheck.substring(product1UPCvaluecheck.indexOf("UPC:")+5);
						System.out.println("Product1upcvaluechecktrim:" +Product1upcvaluechecktrim);				
						//Stream Comparison 
						JSONObject strm1Child=Stream1PDPJson.getJSONObject("children");
						JSONArray strm1Sku=strm1Child.getJSONArray("skus");
						for(int mi=0;mi<strm1Sku.length();mi++)
						{
							JSONObject strm1Skuobj=strm1Sku.getJSONObject(mi);
							JSONObject strm1Prop=strm1Skuobj.getJSONObject("properties");
							String  strm1SkuId=strm1Skuobj.getString("identifier");
							System.out.println("strm1SkuId:" +strm1SkuId);
							if(Product1upcvaluechecktrim.equals(strm1SkuId))
							{
								JSONObject strm1Propr=strm1Prop.getJSONObject("orderinfo");
								String  strm1Proporder=strm1Propr.getString("ordertype");
								System.out.println("strm1Proporder:" +strm1Proporder);
								JSONObject strm1Skuinfo=strm1Prop.getJSONObject("skuinfo");
								JSONObject strm1SkuColor=strm1Skuinfo.getJSONObject("color");
								String  strm1SkuColorvalue=strm1SkuColor.getString("value");
								System.out.println("strm1SkuColorvalue:" +strm1SkuColorvalue);
								String strm1SkuColorlabel=strm1SkuColor.getString("label");
								System.out.println("strm1SkuColorlabel:" +strm1SkuColorlabel);
								//JSONObject strmSkuSize=strmSkuinfo.getJSONObject("size1");  
								//String  strmSkusizevalue=strmSkuSize.getString("value");
								//System.out.println("strmSkusizevalue:" +strmSkusizevalue);
								//String  strmSkusizelabel=strmSkuSize.getString("label");  
								//System.out.println("strmSkusizelabel:" +strmSkusizelabel);
								JSONObject strm1Buyinfo=strm1Prop.getJSONObject("buyinfo");
								JSONObject strm1Pricing=strm1Buyinfo.getJSONObject("pricing");
								String  strm1Qtylimit=strm1Buyinfo.getString("qtylimit");
								System.out.println("strm1Qtylimit:" +strm1Qtylimit);
								JSONArray strm1Prices=strm1Pricing.getJSONArray("prices");
								JSONObject strm1OrstrPrice=strm1Prices.getJSONObject(0);
								String  strm1Origstrprvalue=strm1OrstrPrice.getString("value");
								System.out.println("strm1Origstrprvalue:" +strm1Origstrprvalue);
								JSONObject strm1CurrstrPrice=strm1Prices.getJSONObject(1);
								String strm1Currstrprvalue=strm1CurrstrPrice.getString("value");
								System.out.println("strmCurrstrprvalue:" +strm1Currstrprvalue);
								JSONObject strm1FedstrPrice=strm1Prices.getJSONObject(2);
								String strm1Fedstrprvalue=strm1FedstrPrice.getString("value");
								System.out.println("strmFedstrprvalue:" +strm1Fedstrprvalue);
								JSONArray strm1Availability=strm1Buyinfo.getJSONArray("availability");
								JSONObject strm1Availability0=strm1Availability.getJSONObject(0);
								String strm1Onlineinv=strm1Availability0.getString("onlineinventory");
								System.out.println("strm1Onlineinv:" +strm1Onlineinv);
								String strm1Online=strm1Availability0.getString("online");
								System.out.println("strm1Online:" +strm1Online); 
								String Strm1Fedfilinv=strm1Availability0.getString("fedfilinventory");
								System.out.println("Strm1Fedfilinv:" +Strm1Fedfilinv);
								String strm1Fedfil=strm1Availability0.getString("fedfil");
								System.out.println("strm1Fedfil:" +strm1Fedfil);
								Float strm1OrigFloprvalue = Float.parseFloat(strm1Origstrprvalue);
								Float strm1CurrFloprvalue = Float.parseFloat(strm1Currstrprvalue);
								Float strm1FedFloprvalue = Float.parseFloat(strm1Fedstrprvalue); 
								JSONObject strm1Iteminfo=strm1Prop.getJSONObject("iteminfo");
								JSONArray strm1Shipmsgs=strm1Iteminfo.getJSONArray("shippingmessages");
								JSONObject strm1Shippingmsgs=strm1Shipmsgs.getJSONObject(0);
								String strm1Shippingmsgssuspain=strm1Shippingmsgs.getString("suspain");
								System.out.println("strm1Shippingmsgssuspain:" +strm1Shippingmsgssuspain);
								String strm1Shippingmsgsdeliverytype=strm1Shippingmsgs.getString("deliverytype");
								System.out.println("strm1Shippingmsgsdeliverytype:" +strm1Shippingmsgsdeliverytype);
								String strm1Shippingmsgsgiftwrap=strm1Shippingmsgs.getString("giftwrap");
								System.out.println("strm1Shippingmsgsgiftwrap:" +strm1Shippingmsgsgiftwrap);
								String strm1Shippingmsgsshipdate=strm1Shippingmsgs.getString("shipdate");
								System.out.println("strm1Shippingmsgsshipdate:" +strm1Shippingmsgsshipdate);
								String strm1Shippingmsgsshipdays=strm1Shippingmsgs.getString("shipdays");
								System.out.println("strm1Shippingmsgsshipdays:" +strm1Shippingmsgsshipdays); 
								String strm1Shippingmsgsmethod=strm1Shippingmsgs.getString("method");
								System.out.println("strm1Shippingmsgsmethod:" +strm1Shippingmsgsmethod);
								JSONArray strm1Swtch=strm1Iteminfo.getJSONArray("swatches");
								JSONObject strm1Swatches0=strm1Swtch.getJSONObject(0);
								JSONArray strm1Pdtimage=strm1Swatches0.getJSONArray("pdtimage");
								JSONObject strm1Pdtimage0=strm1Pdtimage.getJSONObject(0);
								String strm1Pdtaddseqnumber=strm1Pdtimage0.getString("sequencenumber");
								System.out.println("strm1Pdtaddseqnumber:" +strm1Pdtaddseqnumber);
								String strm1Pdtaddname=strm1Pdtimage0.getString("name");
								System.out.println("strm1Pdtaddname:" +strm1Pdtaddname);
								String strm1Pdtaddimage=strm1Pdtimage0.getString("image");     
								System.out.println("strm1Pdtaddimage:" +strm1Pdtaddimage);
								JSONArray strm1Storeinfo=strm1Prop.getJSONArray("storeinfo");
								System.out.println("strm1Storeinfo:" +strm1Storeinfo);
								String Web1ID=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div.webid")).getText();
								System.out.println("Web1ID:" +Web1ID);
								String Web1IDtrim=Web1ID.substring(Web1ID.indexOf("Web ID: ")+9);
								System.out.println("Web1IDtrim:" +Web1IDtrim);
								String strm1PDPIdentifier= Stream1PDPJson.getString("identifier");
								System.out.println("strm1PDPIdentifier:" +strm1PDPIdentifier);
								if(Web1IDtrim.equalsIgnoreCase(strm1PDPIdentifier))
								{
									String logErr189= "Pass:The Product Page Identifier gets matched:\n" +Web1IDtrim+"\n" +strm1PDPIdentifier; 
									logInfo(logErr189);
								}
								else
								{
									String logErr199= "Fail:The Product Page Identifier doesn't gets matched:\n" +Web1IDtrim+ "\n" +strm1PDPIdentifier; 
									logInfo(logErr199);
								}
								String product1UPCvalue= driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.webIdContainer div#pdpUPCId.upcid")).getText();
								System.out.println("product1UPCvalue:" +product1UPCvalue);
								String Product1upcvaluetrim=product1UPCvalue.substring(product1UPCvalue.indexOf("UPC:")+5);
								System.out.println("Product1upcvaluetrim:" +Product1upcvaluetrim);  
								if(Product1upcvaluetrim.equals(strm1SkuId))
								{
									String logErr205= "Pass:The Product UPC value gets matched:\n" +Product1upcvaluetrim+ "\n" +strm1SkuId; 
									logInfo(logErr205);
								}
								else
								{
									String logErr215= "Fail:The Product UPC value doesn't gets matched:\n" +Product1upcvaluetrim+ "\n" +strm1SkuId; 
									logInfo(logErr215);
								}
								String product1pagecolorvalue=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_colorContainer.colorContainer div.colorTitle div")).getText();
								System.out.println("product1pagecolorvalue:" +product1pagecolorvalue);
								if(product1pagecolorvalue.equalsIgnoreCase(strm1SkuColorvalue)); 
								{
									String logErr220= "Pass:The Product Page UPC Color gets matched:\n" +product1pagecolorvalue+"\n" +strm1SkuColorvalue; 
									logInfo(logErr220);
								}
								//String productpagesizevalue=driver.findElement(By.xpath(productsizeselection)).getText(); 
								//System.out.println("productpagesizevalue:" +productpagesizevalue);
								//if(productpagesizevalue.equals(strmSkusizevalue))
								//{
								//String logErr24= "Pass:The Product Page UPC Size gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue; 
								//logInfo(logErr24);
								//}
								//else
								//{
								//String logErr25= "Fail:The Product Page UPC Size doesn't gets matched:\n" +productpagesizevalue+"\n" +strmSkusizevalue;
								//logInfo(logErr25);
								//} 
								String price1salevalueinpdp=driver.findElement(By.xpath("//*[@id='id_pdpSalePrice']")).getText();
								System.out.println("price1salevalueinpdp:" +price1salevalueinpdp);
								String price1valueinpdp=driver.findElement(By.xpath("//*[@id='id_pdpRegPrice']")).getText();
								System.out.println("price1valueinpdp:" +price1valueinpdp);
								//Current and original are same
								if(strm1CurrFloprvalue.compareTo(strm1OrigFloprvalue)==0 && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
								{
									String logErr285= "Pass:The UPC Contains Current/Store price:\n" +price1valueinpdp+"\n" +strm1CurrFloprvalue; 
									logInfo(logErr285);
								}
								// Current/Store<Original
								else if(strm1CurrFloprvalue.compareTo(strm1OrigFloprvalue)<0 && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
								{
									String sale1pricevalueinpdp=driver.findElement(By.xpath(pricesale)).getText();
									System.out.println("sale1pricevalueinpdp:" +sale1pricevalueinpdp); 
									String logErr295= "Pass:The UPC Contains Current/Store in Red Original in Black w/ label:\n" +sale1pricevalueinpdp+"\n" +strm1CurrFloprvalue+ "\n" +price1valueinpdp+ "\n" +strm1OrigFloprvalue; 
									logInfo(logErr295);
								}
								// Current/Store>Original
								else if(strm1CurrFloprvalue.compareTo(strm1OrigFloprvalue)>0 && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
								{
									String logErr305= "Pass:The UPC Contains Current/Store in Black:\n" +price1valueinpdp+"\n" +strm1CurrFloprvalue; 
									logInfo(logErr305);
								}
								// Original Price is null
								else if((strm1OrigFloprvalue==0) && (strm1Fedfil.equals("true")|| strm1Fedfil.equals("false")))
								{
									String logErr310= "Pass:The UPC Contains Current/Store in Black:\n" +price1valueinpdp+"\n" +strm1CurrFloprvalue; 
									logInfo(logErr310);
								}
								// Current price is null and Fedfilavailability price available 
								else if((strm1CurrFloprvalue==0) && (strm1Fedfil.equals("true")))
								{
									String logErr334= "Pass:The UPC Contains Fedfil price:\n" +price1valueinpdp+"\n" +strm1FedFloprvalue; 
									logInfo(logErr334);
								}	
								// current price is null and Fedfilavailability price not available
								else if((strm1CurrFloprvalue==0) && (strm1Fedfil.equals("false")))
								{
									String logErr328= "Pass:The UPC Contains Original price:\n" +price1valueinpdp+"\n" +strm1OrigFloprvalue; 
									logInfo(logErr328);
								}
								// Current and original price is null and Fedfilavailability price not available
								else if((strm1CurrFloprvalue==0) && (strm1OrigFloprvalue==0) && (strm1Fedfil.equals("false")))
								{
									String text11="Price is unavailable, please validate price status in mPOS or POS.";
									String text111="Price is unavailable, please contact an  associate for pricing";
									if(price1valueinpdp.equalsIgnoreCase(text11))
									{
										String logErr339= "Pass:The Product Page contains penny price in Associate Mode:\n" +price1valueinpdp+"\n" +text11; 
										logInfo(logErr339);
									}
									else  
									{
										String logErr349= "Pass:The Product Page contains penny price in Customer Mode:\n" +price1valueinpdp+ "\n" +text111; 
										logInfo(logErr349);
									}
								}
								else if((strm1CurrFloprvalue==0) && (strm1FedFloprvalue==0))
								{
									String logErr359= "Pass:The Product Page contains Original Price:\n" +price1valueinpdp+ "\n" +strm1OrigFloprvalue; 
									logInfo(logErr359);

								}
								//Current/store price has penny price 
								else if	((strm1CurrFloprvalue==0.01) && (strm1FedFloprvalue<=0.10)) 			
								{
									String text12="Price is unavailable, please validate price status in mPOS or POS.";
									String text112="Price is unavailable, please contact an  associate for pricing";
									if(price1valueinpdp.equalsIgnoreCase(text12))
									{
										String logErr360= "Pass:The Product Page contains penny price in Associate Mode:\n" +price1valueinpdp+"\n" +text12; 
										logInfo(logErr360);
									}
									else  
									{
										String logErr340= "Pass:The Product Page contains penny price in Customer Mode:\n" +price1valueinpdp+ "\n" +text112; 
										logInfo(logErr340);
									}
								}
								if(product1colorcountno<=5)
								{
									String logErr605= "Pass:The Product doesn't contain more colors:\n" +product1colorcountno; 
									logInfo(logErr605);
								}
								else
								{
									String logErr615= "Pass:The Product contains more colors:\n" +product1colorcountno; 
									logInfo(logErr615);
								}
								if(strm1Online.equals("true")&&strm1Fedfil.equals("true"))
								{
									String logErr355= "Pass:The UPC is Available for shipping"; 
									logInfo(logErr355);
									driver.findElement(By.xpath("(//*[@class='addtoOrder'])")).click();
									Thread.sleep(3000);
									String quick1viewtitlestring=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
									System.out.println("quick1viewtitlestring:" +quick1viewtitlestring);	
									String quick1viewwebidstring=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
									System.out.println("quick1viewwebidstring:" +quick1viewwebidstring);
									String quick1viewwebidstringtrim=quick1viewwebidstring.substring(quick1viewwebidstring.indexOf("(")+1,quick1viewwebidstring.indexOf(")"));
									System.out.println("quick1viewwebidstringtrim:" +quick1viewwebidstringtrim);
									String quick1viewpricestring=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
									System.out.println("quick1viewpricestring:" +quick1viewpricestring);			
									String quick1viewcolorstring=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
									System.out.println("quick1viewcolorstring:" +quick1viewcolorstring);		
									String quick1viewimageurl=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
									System.out.println("quick1viewimageurl:" +quick1viewimageurl);
									String quick1viewimageurltrim=quick1viewimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
									System.out.println("quick1viewimageurltrim:" +quick1viewimageurltrim);			
									if(quick1viewwebidstringtrim.equals(Web1ID)&&quick1viewpricestring.equals(price1valueinpdp)&&quick1viewcolorstring.equalsIgnoreCase(product1pagecolorvalue)&&quick1viewimageurltrim.equals(Product1primaryimagetrim))
									{
										String logErr381= "Pass:The details in the AddtoBag overlay value gets matched:\n" +quick1viewtitlestring+"\n" +Pdt1Name+ "\n" +quick1viewwebidstringtrim+ "\n" +Web1ID+ "\n" +quick1viewpricestring+ "\n" +price1valueinpdp+ "\n" +quick1viewcolorstring+ "\n" +product1pagecolorvalue+ "\n" +quick1viewimageurltrim+ "\n" +Product1primaryimagetrim;
										logInfo(logErr381);  
									}
									else
									{
										String logErr391= "Fail:The details in the AddtoBag overlay value doesn't gets matched:\n" +quick1viewtitlestring+"\n" +Pdt1Name+ "\n" +quick1viewwebidstringtrim+ "\n" +Web1ID+ "\n" +quick1viewpricestring+ "\n" +price1valueinpdp+ "\n" +quick1viewcolorstring+ "\n" +product1pagecolorvalue+ "\n" +quick1viewimageurltrim+ "\n" +Product1primaryimagetrim;
										logInfo(logErr391);  
									}

									driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();		
								}
								else
								{
									String logErr361= "Pass:The UPC is not Available for shipping";
									logInfo(logErr361);
								}						
								//Check others stores functionalities 
								Thread.sleep(2500);
								driver.findElement(By.xpath(productotherstores)).click();
								Thread.sleep(5000);
								//Check other Stores stream call data 
								String pdt1upcvalue="http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+Product1upcvaluetrim+"?type=UPCWithNearbyStores&storeid=28&campaignId=383";
								URL url113 = new URL(pdt1upcvalue);
								String pageSource113  = new Scanner(url113.openConnection().getInputStream()).useDelimiter("\\Z").next();
								System.out.println("pageSource113:" +pageSource113);
								JSONObject Stream1PDPUpccheckotherstores=new JSONObject(pageSource113);
								JSONObject stream1PDPUpcchild=Stream1PDPUpccheckotherstores.getJSONObject("children");
								JSONArray stream1PDPUPCSku=stream1PDPUpcchild.getJSONArray("skus");
								for(int q1=0;q1<stream1PDPUPCSku.length();q1++)
								{
									JSONObject stream1PDPUPCSkuobj=stream1PDPUPCSku.getJSONObject(q1);
									String stream1PDPUPCidentifier= stream1PDPUPCSkuobj.getString("identifier");
									System.out.println("stream1PDPUPCidentifier:" +stream1PDPUPCidentifier);
									if(stream1PDPUPCidentifier.equals(Product1upcvaluetrim))
									{
										JSONObject stream1PropUPC=stream1PDPUPCSkuobj.getJSONObject("properties");
										JSONArray stream1PDPStoreinfo=stream1PropUPC.getJSONArray("storeinfo"); 
										int product1storescounts=driver.findElements(By.xpath(productstoresize)).size(); 
										System.out.println("product1storescounts:" +product1storescounts); 
										for(int o1=0,p1=1;o1<stream1PDPStoreinfo.length()||p1<=product1storescounts;o1++,p1++)
										{

											JSONObject strm1Storeinfo1=stream1PDPStoreinfo.getJSONObject(o1);
											String strm1Storeinfophone=strm1Storeinfo1.getString("phone");
											System.out.println("strm1Storeinfophone:" +strm1Storeinfophone);
											String strm1StoreinfoSequenceno=strm1Storeinfo1.getString("sequencenumber");
											System.out.println("strm1StoreinfoSequenceno:" +strm1StoreinfoSequenceno);
											String strm1StoreinfoInventory=strm1Storeinfo1.getString("inventory");
											System.out.println("strm1StoreinfoInventory:" +strm1StoreinfoInventory);
											String strm1StoreinfoName=strm1Storeinfo1.getString("name");
											System.out.println("strm1StoreinfoName:" +strm1StoreinfoName);
											String strm1StoreinfoZipcode=strm1Storeinfo1.getString("zipcode");
											System.out.println("strm1StoreinfoZipcode:" +strm1StoreinfoZipcode);
											String strm1StoreinfoState=strm1Storeinfo1.getString("state");
											System.out.println("strm1StoreinfoState:" +strm1StoreinfoState);
											String strm1StoreinfoAddress1=strm1Storeinfo1.getString("address1");
											System.out.println("str1mStoreinfoAddress1:" +strm1StoreinfoAddress1);
											String strm1StoreinfoAddress2=strm1Storeinfo1.getString("address2");
											System.out.println("strm1StoreinfoAddress2:" +strm1StoreinfoAddress2);
											String strm1StoreinfoIdentifier=strm1Storeinfo1.getString("identifier");
											System.out.println("strm1StoreinfoIdentifier:" +strm1StoreinfoIdentifier);
											String strm1StoreinfoCity=strm1Storeinfo1.getString("city");
											System.out.println("strm1StoreinfoCity:" +strm1StoreinfoCity);
											String strm1StoreinfoNameconcat=strm1StoreinfoName+strm1StoreinfoIdentifier;
											System.out.println("strm1StoreinfoNameconcat:" +strm1StoreinfoNameconcat);
											String strm1storeinfocombined=strm1StoreinfoNameconcat.replaceAll(""+strm1StoreinfoIdentifier+".*","")+" "+"("+strm1StoreinfoIdentifier+")";
											System.out.println("strm1storeinfocombined:" +strm1storeinfocombined);
											String check1availabilityheadertitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p1+"]/div[1]/div/div[2]/div")).getText();
											System.out.println("check1availabilityheadertitle:" +check1availabilityheadertitle);							                              
											driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p1+"]/div[1]/div/div[2]/div")).click();
											Thread.sleep(2000);
											String check1availabilitystreetNametitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p1+"]/div[2]/div/div[1]")).getText();
											System.out.println("check1availabilitystreetNametitle:" +check1availabilitystreetNametitle);				                          
											String check1availabilitystoreLocationtitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p1+"]/div[2]/div/div[2]")).getText();
											System.out.println("check1availabilitystoreLocationtitle:" +check1availabilitystoreLocationtitle);
											String check1availabilitystorePhonetitle=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p1+"]/div[2]/div/div[3]")).getText();
											System.out.println("check1availabilitystorePhonetitle:" +check1availabilitystorePhonetitle);
											driver.findElement(By.xpath("//*[@id='storeContainer']/div["+p1+"]/div[1]/div/div[2]/div")).click();	
											if(check1availabilityheadertitle.equals(strm1storeinfocombined)&&check1availabilitystreetNametitle.equals(strm1StoreinfoAddress1)&&check1availabilitystoreLocationtitle.equals(strm1StoreinfoAddress2)&&check1availabilitystorePhonetitle.equals(strm1Storeinfophone))
											{
												String logErr278= "Pass:The Product Page UPC Checkother Stores value gets matched:\n" +check1availabilityheadertitle+"\n" +strm1storeinfocombined+ "\n" +check1availabilitystreetNametitle+ "\n" +strm1StoreinfoAddress1+ "\n" +check1availabilitystoreLocationtitle+ "\n" +strm1StoreinfoAddress2+ "\n" +check1availabilitystorePhonetitle+ "\n" +strm1Storeinfophone;  
												logInfo(logErr278);
											}	
											else
											{
												String logErr288= "Fail:The Product Page UPC Checkother Stores value doesn't gets matched:\n" +check1availabilityheadertitle+"\n" +strm1storeinfocombined+ "\n" +check1availabilitystreetNametitle+ "\n" +strm1StoreinfoAddress1+ "\n" +check1availabilitystoreLocationtitle+ "\n" +strm1StoreinfoAddress2+ "\n" +check1availabilitystorePhonetitle+ "\n" +strm1Storeinfophone;
												logInfo(logErr288);
											}
											Thread.sleep(5000);
											String logErr378= "-----------------------------------------------------------------------------";
											logInfo(logErr378);
											System.out.println("-----------------------------------------");  
										}                                 
										driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent.ui-content div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_pdpSeeAllAvailable.SeeAllAvailable.cls_midleAlign.kioskOtherStore div.otherStlayoutContainer div.otherStoreCloseBtnDiv")).click();
										driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_5.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent.ui-content div#PDP_page.page.lyt_cont_div div#skPageLayoutCell_5_id-center.skc_pageCellLayout.cls_skWidget.PDP_pageCloneClone div#skPageLayoutCell_5_id-center.cls_customWidget div#id_PDPcontainerDiv.PDPcontainerDiv div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#reviewCont div.pdpAddToFavt.pdpfavList.pdpfavList_icon")).click();
									}
								}
							}
						}
					} 
				} 
			} 
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void favoritePage() 
	{
		try
		{
			try
			{		
				//favorites page
				String logErr311 ="Favorites Page Functionalities"; 
				logInfo(logErr311);
				String logErr312="---------------------------------"; 
				logInfo(logErr312);
				driver.findElement(By.xpath(favoritesiconselect)).click();
				Thread.sleep(8000);
				//10.Verify that product should be able added to Favorites and Add to bag from the PDP,Master PDP,Member PDP page and Quickview 
				String logErrsmp10="10,Verify that product should be able added to Favorites and Add to bag from the PDP Master PDP Member PDP page and Quickview, Pass"; 
				logInfo1(logErrsmp10);	
				int favoriteproductscolumnvalues=driver.findElements(By.xpath(favoriteproductscount)).size(); 
				System.out.println("favoriteproductscountvalues:" +favoriteproductscolumnvalues);
				for(int q=1;q<=favoriteproductscolumnvalues;q++)
				{
					String favoritescolumnid=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div["+q+"]")).getAttribute("id");
					System.out.println("favoritescolumnid:" +favoritescolumnid);
					int favoritecolumsize=driver.findElements(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div["+q+"]/div")).size();
					System.out.println("favoritecolumsize:" +favoritecolumsize);
					for(int r=1;r<=favoritecolumsize;r++)
					{
						driver.findElement(By.xpath("//*[@id='"+favoritescolumnid+"']/div["+r+"]/div")).click();
						System.out.println("favpdtselect:" +"//*[@id='"+favoritescolumnid+"']/div["+r+"]");
						Thread.sleep(4000);			
						String favoritesproductid=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div[1]/div["+r+"]")).getAttribute("prdtid");
						System.out.println("favoritesproductid:" +favoritesproductid);
						String favoritesproductname=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div[1]/div["+r+"]")).getAttribute("title");
						System.out.println("favoritesproductname:" +favoritesproductname);
						String favoritesproductprice=driver.findElement(By.xpath("//*[@id='id_favListRightContainer']/div[1]/div[1]/div["+r+"]/div[3]")).getText();
						System.out.println("favoritesproductprice:" +favoritesproductprice);					
						String pdtUrl4 = "http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+favoritesproductid+"?type=ID&storeid=28&campaignId=383";	
						URL url4  = new URL(pdtUrl4);
						String pageSource4  = new Scanner(url4.openConnection().getInputStream()).useDelimiter("\\Z").next();
						System.out.println("pageSource4:" +pageSource4);
						JSONObject StreamFavJson = new JSONObject(pageSource4);
						System.out.println("StreamFavJson:" +StreamFavJson); 
						Thread.sleep(3000);
						//QuickView details
						if(driver.findElement(By.className("qvTitleContainerDiv")).isDisplayed())
						{
							System.out.println("Yesss");
						}
						else
						{
							System.out.println("Noooo");
						}		
						//28.Verify that while selecting the favorites and  item in the page, the Quick view overlay should be enabled 
						String logErrsmp28 ="28,Verify that while selecting the favorites and  item in the page the Quick view overlay should be enabled, Pass"; 
						logInfo1(logErrsmp28);
						String favoritetitle=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_quickViewOverlayCon_1426336.commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_rightContainerWrapperChild.rightContainerWrapper div#id_rightContainerDivChild.rightContainerDiv div.pdtDetailcontainer.snapItem div.scrollerContainerDiv div.scrollercontainer div.scrollerItems div#id_decContainer.decContainer div.pdttitledec div")).getText();	  			
						System.out.println("favoritetitle:" +favoritetitle);        
						String favoriteproductimage=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+favoritesproductid+"']/img")).getAttribute("src");
						System.out.println("favoriteproductimage:" +favoriteproductimage);
						String favoritequickprice=driver.findElement(By.xpath("//*[@id='id_regPrice_"+favoritesproductid+"']")).getText();
						System.out.println("favoritequickprice:" +favoritequickprice);	
						String favoritecolor=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_quickViewOverlayCon_1426336.commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div#id_colorContainer.colorContainer div.colorTitle div")).getText();				
						System.out.println("favoritecolor:" +favoritecolor);
						Thread.sleep(1000);			
						//driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skMacys.ui-overlay-c div#id_quickViewOverlayCon_2272318.commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div#id_sizeAvailabilityCont.sizeAvailabilityCont div#id_sizeContainer.sizeContainer div#id_dropContainer.sizeCont div#id_sizeTitle.sizeArrow")).click();
						//Thread.sleep(1000);
						//String favoritesizeoverla=driver.findElement(By.cssSelector(".titleDivSizeText")).getText();
						//System.out.println("favoritesizeoverla:" +favoritesizeoverla);
						//driver.findElement(By.cssSelector(".sizeClsBtn")).click();
						Thread.sleep(1000);
						driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_quickViewOverlayCon_1426336.commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div.cls_assisted_checkout.qvQtyContainer div#id_QtyContainer.sizeContainer.quantityContainer div#id_dropContainer.sizeCont div#id_qtyTitle.qtyArrow")).click();				
						Thread.sleep(1000);
						driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_quickViewOverlayCon_1426336.commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_qtyMenuItems.qtyMenuContainer div.titleDivQtyCon div.qtyClsBtn")).click();
						Thread.sleep(1000);
						if(favoritesproductprice.equals(favoritequickprice)&&favoritesproductname.equals(favoritesproductname))
						{
							String logErr100= "Pass:The products name and price in the favorites page gets matches in the QuickView overlay:\n" +favoritesproductname+ "\n" +favoritesproductprice+ "\n" +favoritecolor+ "\n" +favoriteproductimage;   
							logInfo(logErr100);
						}
						else
						{
							String logErr101= "Fail:The products name and price in the favorites page doesn't gets matches in the QuickView overlay:\n" +favoritesproductname+ "\n" +favoritesproductprice+ "\n" +favoritecolor+ "\n" +favoriteproductimage;   
							logInfo(logErr101);
						}				
						//Quick View Scrolling 
						Thread.sleep(1000);
						WebElement elecr;	
						System.out.println("sss");
						elecr=driver.findElement(By.cssSelector("#id_availabilityCont")); 
						Actions abec = new Actions(driver);
						System.out.println("m");
						abec.dragAndDropBy(elecr,0,900).build().perform();
						System.out.println("drag");
						Thread.sleep(3000);
						if(driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_quickViewOverlayCon_1426336.commonQvpdp.overlaytobehide div#id_PDPcontainerDiv.PDPcontainerDiv.qvHotspotpopup.commonQuickViewCon.quickViewOverlayCon.quickViewOffline div.hidescrollbar.prdtQvContentScroller div.qvRelativeContainer div#id_bottomContainerWrapperChild.bottomContainerWrapper div.cls_assisted_checkout.addToOrderCont div#id_addtoOrder.addtoOrder")).isDisplayed())				
						{
							System.out.println("Yesss");
							String logErr102= "Pass: The product contains Add to bag Availability";
							logInfo(logErr102);
							driver.findElement(By.xpath("(//*[@class='addtoOrder'])")).click();
							Thread.sleep(3000);
							String favoritesaddtobagproducttitle=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
							System.out.println("favoritesaddtobagproducttitle:" +favoritesaddtobagproducttitle);		
							String favoritesaddtobagproductwebid=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
							System.out.println("favoritesaddtobagproductwebid:" +favoritesaddtobagproductwebid);
							String favoritesaddtobagproductprice=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
							System.out.println("favoritesaddtobagproductprice:" +favoritesaddtobagproductprice);
							String favoritesaddtobagproductcolor=driver.findElement(By.xpath("(//*[@class='overlayColorInfoValue'])")).getText();
							System.out.println("favoritesaddtobagproductcolor:" +favoritesaddtobagproductcolor);					
							driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();
							String logErr105= "Add to Bag Overlay Details:\n" +favoritesaddtobagproducttitle+ "\n" +favoritesaddtobagproductwebid+ "\n" +favoritesaddtobagproductprice+ "\n" +favoritesaddtobagproductcolor;
							logInfo(logErr105);
						}
						else
						{
							System.out.println("Noooooooo");
							String logErr103="Pass:The product doesn't contains Add to bag Availability:\n";
							logInfo(logErr103);
						}  
						Thread.sleep(2000);	  
						driver.findElement(By.xpath("//*[@id='id_seeFullPdtDetails_"+favoritesproductid+"']")).click();
						Thread.sleep(5000);
						//29.Verify that while selecting the "See full product details" button, the PDP page should be displayed
						String logErrsmp29 ="29,Verify that while selecting the See full product details button the PDP page should be displayed, Pass"; 
						logInfo1(logErrsmp29);        
						driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[2]/div/div[1]")).click();
						Thread.sleep(2000);
						// driver.findElement(By.xpath("//*[@id='id_qvClsBtnDiv']")).click();
						String logErr104="-------------------------------------------------------------------------------------";
						logInfo(logErr104);
						System.out.println("-----------------------------------------");
					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[@id='id_favList']/div[1]/div[2]/div[2]/div[1]")).click();
			Thread.sleep(1000);
			//need to check the condition 	
			try
			{
				if(driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.singelPdtEmailShare")).isDisplayed())
				{
					Thread.sleep(2000);
					//15.Verify that  �QR and e-mail �share overlay should be enabled for the single product share in shopping bag,favorites and PDP Page
					String logErrsmp15 ="15.Verify that  �QR and e-mail �share overlay should be enabled for the single product share in shopping bag,favorites and PDP Page::: Pass"; 
					logInfo1(logErrsmp15);		
					driver.findElement(By.xpath("//*[@id='id_custNameText']")).sendKeys("QA");
					driver.findElement(By.xpath("//*[@id='id_custEmailText']")).sendKeys("naresh@skava.com");
					driver.findElement(By.xpath("//*[@id='id_associateNameText']")).sendKeys("QA");
					driver.findElement(By.xpath("//*[@id='id_associateEmailText']")).sendKeys("q@q.com");
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.singelPdtEmailShare")).click();
					Thread.sleep(3000);
					String sharesuccess=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.mailStatusInfo")).getText();
					System.out.println("sharesuccess:" +sharesuccess);
					if(sharesuccess.equals("Your email has been sent successfully"))
					{
						String logErr89="Pass:The product in the favorites page has been shared successfully\n";   
						logInfo(logErr89);
						//18.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
						String logErrsmp18 ="18,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, Pass"; 
						logInfo(logErrsmp18);
					}  
					else
					{
						String logErr90="Fail:The product in the favorites page doesn't gets shared successfully\n";   
						logInfo(logErr90);
						//18.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
						String logErrsmf18 ="18,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, fail"; 
						logInfo(logErrsmf18);
					} 
					driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div#id_emailHeader.emailHeader div.multipleShareEmailPopupClose")).click();
					Thread.sleep(2000);
					try
					{
						if(driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).isDisplayed())
						{
							driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).click();
							Thread.sleep(1000);
							driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[13]")).click();
							Thread.sleep(2000); 
						}
						else
						{
							driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[13]")).click();
							Thread.sleep(2000); 
						}
					}
					catch(Exception e)
					{
						System.out.println(e.toString());	

					}
				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			Thread.sleep(3000);
			try
			{
				//16.Verify that  �e-mail �share overlay should be alone enabled for the multiple product share in shopping bag,favorites
				String logErrsmp16 ="16,Verify that  �e-mail �share overlay should be alone enabled for the multiple product share in shopping bag favorites, Pass"; 
				logInfo1(logErrsmp16);	
				driver.findElement(By.cssSelector("	html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_favListShareContainer.multifavListShareContainer.overlaytobehide div#id_sharePopup.multipleSharePopup.hideOnbodyClick.cls_midleAlign div#id_favListShare.cls_favListshare div.cls_favListShareContinue")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_custNameText']")).sendKeys("QA");
				driver.findElement(By.xpath("//*[@id='id_custEmailText']")).sendKeys("naresh@skava.com");
				driver.findElement(By.xpath("//*[@id='id_associateNameText']")).sendKeys("QA");
				driver.findElement(By.xpath("//*[@id='id_associateEmailText']")).sendKeys("q@q.com");
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.singelPdtEmailShare")).click();
				Thread.sleep(2000);
				String sharesuccess=driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div.pdpEmailShareCont div.mailStatusInfo")).getText();
				System.out.println("sharesuccess:" +sharesuccess);
				if(sharesuccess.equals("Your email has been sent successfully"))
				{
					String logErr89="Pass:The product in the favorites page has been shared successfully\n";   
					logInfo(logErr89);
					//18.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
					String logErrsmp18 ="18,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, Pass"; 
					logInfo1(logErrsmp18);
				}  
				else
				{
					String logErr90="Fail:The product in the favorites page doesn't gets shared successfully\n";   
					logInfo(logErr90);
					//18.Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully
					String logErrsmf18 ="18,Verify that �email success� overlay should be displayed in Favorites and Shopping bag page when the e-mail has been send successfully, fail"; 
					logInfo1(logErrsmf18);
				} 
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#id_emailOvelay.emailOvelay.overlaytobehide div#id_jwlEmailPopup.cls_midleAlign div#id_shareSinglePopup.pdpSingleSharePopup div#id_emailHeader.emailHeader div.multipleShareEmailPopupClose")).click();
				Thread.sleep(2000);
				try
				{
					if(driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).isDisplayed())
					{
						driver.findElement(By.xpath("//*[@id='id_alertPopupClose']")).click();
						Thread.sleep(1000);
						driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[13]")).click();
						Thread.sleep(2000); 
					}
					else
					{
						driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[13]")).click();
						Thread.sleep(2000); 
					}

				}
				catch(Exception e)
				{
					System.out.println(e.toString());	

				} 	
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			Thread.sleep(5000);
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void logOut() 
	{
		try
		{

			try
			{
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[11]/div")).click();
				Thread.sleep(1000); 
				driver.findElement(By.xpath("//*[@id='id_mamFSMLoginCont']/div[1]")).click();
				Thread.sleep(5000);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}	
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}
	private static void searchSuggestionsPage() 
	{
		try
		{
			try
			{ 
				//Search Results Page functionalities
				String logErr313 ="Search Suggestions & Search Results Page "; 
				logInfo(logErr313);
				String logErr314="------------------"; 
				logInfo(logErr314);     
				driver.findElement(By.xpath("(//*[@class='cls_mamFSrch'])")).click();	
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).click();	
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("puma");
				Thread.sleep(8000);
				//Search Suggestions
				String pdtUrl5 = "http://social.macys.com/skavastream/core/v5/bloomingdales/searchsuggestion?campaignId=383&search=puma&limit=10";	
				URL url5  = new URL(pdtUrl5);
				String pageSource5  = new Scanner(url5.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource5:" +pageSource5);
				JSONObject StreamSearchSuggestJson = new JSONObject(pageSource5);
				JSONObject searchsuggestchildren=StreamSearchSuggestJson.getJSONObject("children");
				JSONArray searchsuggestionarray=searchsuggestchildren.getJSONArray("suggestion");	
				int searchsuggestcountsize=driver.findElements(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div")).size(); 
				System.out.println("searchsuggestcountsize:" +searchsuggestcountsize);	
				for(int y=0,z=1;y<searchsuggestionarray.length()||z<=searchsuggestcountsize;y++,z++)
				{

					JSONObject searchsuggesstionobject=searchsuggestionarray.getJSONObject(y);
					String searchsuggestionsname=searchsuggesstionobject.getString("name");
					System.out.println("searchsuggestionsname: "+y+"" +searchsuggestionsname);			                                                                  
					String searchsuggestioninthepage=driver.findElement(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div["+z+"]/span")).getText();
					System.out.println("searchsuggestioninthepage: "+z+"" +searchsuggestioninthepage);
					if(searchsuggestionsname.equals(searchsuggestioninthepage))
					{
						String logErr50= "Pass:Search Suggestion values gets matched with the stream call "+z+":\n" +searchsuggestionsname+"\n" +searchsuggestioninthepage;   
						logInfo(logErr50);
					}
					else
					{
						String logErr51= "Fail:Search Suggestion values doesn't gets matched with the stream call "+z+":\n" +searchsuggestionsname+"\n" +searchsuggestioninthepage;   
						logInfo(logErr51);
					}

				}
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			try
			{   
				driver.findElement(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div[5]/span")).click();	
				Thread.sleep(20000);		
				String pdtUrl3 = "http://social.macys.com/skavastream/core/v5/bloomingdales/search?campaignId=383&limit=24&search=puma%20tees&storeid=28&offset=1";	
				URL url3  = new URL(pdtUrl3);
				String pageSource3  = new Scanner(url3.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource3:" +pageSource3);
				JSONObject StreamSearchJson = new JSONObject(pageSource3);
				System.out.println("StreamSearchJson:" +StreamSearchJson);		
				//13.Verify that search results page should be displayed while searching the product via  Keyword
				String logErrsmp13 ="13,Verify that search results page should be displayed while searching the product via  Keyword, Pass"; 
				logInfo1(logErrsmp13);	
				JSONObject streamsearchchildren=StreamSearchJson.getJSONObject("children");
				JSONArray  streamsearchproductcount=streamsearchchildren.getJSONArray("products");
				int searchproductcountsize=driver.findElements(By.xpath(searchproduct)).size(); 
				System.out.println("searchproductcountsize:" +searchproductcountsize);
				//driver.findElement(By.xpath("//*[@id='id_sPfilterCont']/div[2]")).click();
				//Thread.sleep(1000);
				for(int u=0,t=1;u<streamsearchproductcount.length()||t<=searchproductcountsize;u++,t++)
				{  
					JSONObject strmsearchsku=streamsearchproductcount.getJSONObject(u);
					System.out.println("strmsearchsku:" +strmsearchsku);
					String strmsearchskuname=strmsearchsku.getString("name");
					System.out.println("strmsearchskuname:" +strmsearchskuname);
					String strmsearchskuimage=strmsearchsku.getString("image");
					System.out.println("strmsearchskuimage:" +strmsearchskuimage);
					JSONObject streamproductproperties=strmsearchsku.getJSONObject("properties");
					JSONObject streamproductbuyinfo=streamproductproperties.getJSONObject("buyinfo");
					JSONObject streamproductpricingobject=streamproductbuyinfo.getJSONObject("pricing");
					JSONArray streamproductpricesarray=streamproductpricingobject.getJSONArray("prices");
					JSONObject streamstoresaleprice=streamproductpricesarray.getJSONObject(0);
					String streamstoresalepricevalue=streamstoresaleprice.getString("value");
					System.out.println("streamstoresalepricevalue:" +streamstoresalepricevalue);
					float streamstoresalepricevaluefloat=Float.parseFloat(streamstoresalepricevalue); 
					System.out.println("streamstoresalepricevaluefloat" +streamstoresalepricevaluefloat);
					JSONObject streamstoreregularprice=streamproductpricesarray.getJSONObject(1);
					String streamstoreregularpricevalue=streamstoreregularprice.getString("value");	
					System.out.println("streamstoreregularpricevalue:" +streamstoreregularpricevalue);
					float streamstoreregularpricevaluefloat=Float.parseFloat(streamstoreregularpricevalue); 
					System.out.println("streamstoreregularpricevaluefloat" +streamstoreregularpricevaluefloat);
					//JSONObject streamonlinesaleprice=streamproductpricesarray.getJSONObject(2);
					//String streamonlinesalepricevalue=streamonlinesaleprice.getString("value");
					//System.out.println("streamonlinesalepricevalue:" +streamonlinesalepricevalue);
					//JSONObject streamonlineregprice=streamproductpricesarray.getJSONObject(3);
					//String streamonlineregpricevalue=streamonlineregprice.getString("value");
					//System.out.println("streamonlineregpricevalue:" +streamonlineregpricevalue);
					System.out.println("t:" +t);
					if(t==10)
					{
						WebElement el;	
						System.out.println("START");
						el=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div[12]/div[1]/div[1]")); 
						System.out.println("MIDDLE"); 
						Actions a = new Actions(driver);
						System.out.println("Action");
						a.dragAndDropBy(el,-2850,0).build().perform();
						System.out.println("Drag");
						// a.wait();
						// System.out.println("Wait");
						//when t==10 works 	
						String searchproductid=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
						System.out.println("searchproductid:" +searchproductid);
						int searchproductindividualsize=driver.findElements(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div")).size();
						System.out.println("searchproductindividualsize:" +searchproductindividualsize);
						if(searchproductindividualsize==5)
						{
							String searchproductid1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid1:" +searchproductid1);
							String searchproductimageurl=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid1+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl:" +searchproductimageurl);
							String searchproductnamedetails=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails:" +searchproductnamedetails);
							String searchproductsaleprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[1]")).getText();
							System.out.println("searchproductsaleprice:" +searchproductsaleprice);
							String searchproductsalepricetrim=searchproductsaleprice.replaceAll(".*\\$","");
							System.out.println("searchproductsalepricetrim: " +searchproductsalepricetrim);
							float searchproductsalepricetrimfloat=Float.parseFloat(searchproductsalepricetrim); 
							System.out.println("searchproductsalepricetrimfloat:" +searchproductsalepricetrimfloat);		
							String searchproductregprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductregprice:" +searchproductregprice);
							String searchproductregpricetrim=searchproductregprice.replaceAll(".*\\$","");
							System.out.println("searchproductregpricetrim: " +searchproductregpricetrim);				
							float searchproductregpricetrimfloat=Float.parseFloat(searchproductregpricetrim); 
							System.out.println("searchproductregpricetrimfloat:" +searchproductregpricetrimfloat);
							if(searchproductsalepricetrimfloat==streamstoresalepricevaluefloat||streamstoreregularpricevaluefloat==searchproductregpricetrimfloat)
							{
								String logErr200= "Pass:The search product gets details\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr200);
							}
							else
							{
								String logErr201= "Fail:The search product gets details doesn't gets matched\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr201);
							}
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==6)
						{
							String searchproductid2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid2:" +searchproductid2);
							String searchproductimageurl2=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid2+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl2:" +searchproductimageurl2);
							String searchproductnamedetails2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails2:" +searchproductnamedetails2);
							String searchproductreviewcount=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount:" +searchproductreviewcount);
							String searchproductsaleprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice1:" +searchproductsaleprice1);
							String searchproductsaleprice1trim=searchproductsaleprice1.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice1trim: " +searchproductsaleprice1trim);
							float searchproductsaleprice1trimfloat=Float.parseFloat(searchproductsaleprice1trim); 
							System.out.println("searchproductsaleprice1trimfloat:" +searchproductsaleprice1trimfloat);
							String searchproductregprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice1:" +searchproductregprice1);
							String searchproductregprice1trim=searchproductregprice1.replaceAll(".*\\$","");
							System.out.println("searchproductregprice1trim: " +searchproductregprice1trim);
							float searchproductregprice1trimfloat=Float.parseFloat(searchproductregprice1trim); 
							System.out.println("searchproductregprice1trimfloat:" +searchproductregprice1trimfloat);	
							if(streamstoresalepricevaluefloat==searchproductsaleprice1trimfloat)
							{
								String logErr202= "Pass:The search product gets details gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr202);
							}
							else
							{
								String logErr203= "Fail:The search product gets details doesn't gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr203);
							}					
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==7)
						{
							String searchproductid3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid3:" +searchproductid3);
							String searchproductimageurl3=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid3+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl3:" +searchproductimageurl3);
							String searchproductnamedetails3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails3:" +searchproductnamedetails3);
							String searchproductreviewcount1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount1:" +searchproductreviewcount1);
							String searchproductsaleprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice2:" +searchproductsaleprice2);
							String searchproductsaleprice2trim=searchproductsaleprice2.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice2trim: " +searchproductsaleprice2trim);
							float searchproductsaleprice2trimfloat=Float.parseFloat(searchproductsaleprice2trim);
							System.out.println("searchproductsaleprice2trimfloat: " +searchproductsaleprice2trimfloat);	
							String searchproductregprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice2:" +searchproductregprice2);
							String searchproductregprice2trim=searchproductregprice2.replaceAll(".*\\$","");
							System.out.println("searchproductregprice2trim: " +searchproductregprice2trim);	
							float searchproductregprice2trimfloat=Float.parseFloat(searchproductregprice2trim);
							System.out.println("searchproductregprice2trimfloat: " +searchproductregprice2trimfloat);		
							String searchproductseeitemforprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[5]")).getText();
							System.out.println("searchproductseeitemforprice:" +searchproductseeitemforprice);
							if(searchproductsaleprice2trimfloat==streamstoresalepricevaluefloat)			
							{
								String logErr204= "Pass:The search product gets details gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr204);
							}


							else
							{
								String logErr205= "Fail:The search product gets details doesn't gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr205);
							}	
							System.out.println("-----------------------------------------");
						}

					}
					else
					{
						String searchproductid=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
						System.out.println("searchproductid:" +searchproductid);
						int searchproductindividualsize=driver.findElements(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div")).size();
						System.out.println("searchproductindividualsize:" +searchproductindividualsize);
						if(searchproductindividualsize==5)
						{
							String searchproductid1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid1:" +searchproductid1);
							String searchproductimageurl=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid1+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl:" +searchproductimageurl);
							String searchproductnamedetails=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails:" +searchproductnamedetails);
							String searchproductsaleprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[1]")).getText();
							System.out.println("searchproductsaleprice:" +searchproductsaleprice);			
							String searchproductsalepricetrim=searchproductsaleprice.replaceAll(".*\\$","");
							System.out.println("searchproductsalepricetrim: " +searchproductsalepricetrim);
							float searchproductsalepricetrimfloat=Float.parseFloat(searchproductsalepricetrim); 
							System.out.println("searchproductsalepricetrimfloat:" +searchproductsalepricetrimfloat);		
							String searchproductregprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductregprice:" +searchproductregprice);
							String searchproductregpricetrim=searchproductregprice.replaceAll(".*\\$","");
							System.out.println("searchproductregpricetrim: " +searchproductregpricetrim);				
							float searchproductregpricetrimfloat=Float.parseFloat(searchproductregpricetrim); 
							System.out.println("searchproductregpricetrimfloat:" +searchproductregpricetrimfloat);
							if(searchproductsalepricetrimfloat==streamstoresalepricevaluefloat||streamstoreregularpricevaluefloat==searchproductregpricetrimfloat)
							{
								String logErr206="Pass:The search product gets details gets matched\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr206);
							}
							else
							{
								String logErr207="Fail:The search product gets details doesn't gets matched\n" +searchproductid1+"\n" +searchproductimageurl+ "\n" +searchproductnamedetails+ "\n" +searchproductsaleprice+ "\n" +searchproductregprice;   
								logInfo(logErr207);
							}					
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==6)
						{
							String searchproductid2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid2:" +searchproductid2);
							String searchproductimageurl2=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid2+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl2:" +searchproductimageurl2);
							String searchproductnamedetails2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails2:" +searchproductnamedetails2);
							String searchproductreviewcount=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount:" +searchproductreviewcount);
							String searchproductsaleprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice1:" +searchproductsaleprice1);			
							String searchproductsaleprice1trim=searchproductsaleprice1.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice1trim: " +searchproductsaleprice1trim);
							float searchproductsaleprice1trimfloat=Float.parseFloat(searchproductsaleprice1trim); 
							System.out.println("searchproductsaleprice1trimfloat:" +searchproductsaleprice1trimfloat);
							String searchproductregprice1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice1:" +searchproductregprice1);
							String searchproductregprice1trim=searchproductregprice1.replaceAll(".*\\$","");
							System.out.println("searchproductregprice1trim: " +searchproductregprice1trim);
							float searchproductregprice1trimfloat=Float.parseFloat(searchproductregprice1trim); 
							System.out.println("searchproductregprice1trimfloat:" +searchproductregprice1trimfloat);	
							if(streamstoresalepricevaluefloat==searchproductsaleprice1trimfloat)
							{
								String logErr208="Pass:The search product gets details gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr208);
							}
							else
							{
								String logErr209= "Fail:The search product gets details doesn't gets matched\n" +searchproductid2+"\n" +searchproductimageurl2+ "\n" +searchproductnamedetails2+ "\n" +searchproductsaleprice1+ "\n" +searchproductregprice1;   
								logInfo(logErr209);
							}		
							System.out.println("------------------------------------------");
						}
						else if(searchproductindividualsize==7)
						{
							String searchproductid3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[1]")).getAttribute("prdtid");
							System.out.println("searchproductid3:" +searchproductid3);
							String searchproductimageurl3=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_"+searchproductid3+"']/img")).getAttribute("src");
							System.out.println("searchproductimageurl3:" +searchproductimageurl3);
							String searchproductnamedetails3=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[2]")).getText();
							System.out.println("searchproductnamedetails3:" +searchproductnamedetails3);
							String searchproductreviewcount1=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[3]/div[2]")).getText();
							System.out.println("searchproductreviewcount1:" +searchproductreviewcount1);
							String searchproductsaleprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[1]")).getText();
							System.out.println("searchproductsaleprice2:" +searchproductsaleprice2);
							String searchproductsaleprice2trim=searchproductsaleprice2.replaceAll(".*\\$","");
							System.out.println("searchproductsaleprice2trim: " +searchproductsaleprice2trim);
							float searchproductsaleprice2trimfloat=Float.parseFloat(searchproductsaleprice2trim);
							System.out.println("searchproductsaleprice2trimfloat: " +searchproductsaleprice2trimfloat);	
							String searchproductregprice2=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[4]/div[2]")).getText();
							System.out.println("searchproductregprice2:" +searchproductregprice2);
							String searchproductregprice2trim=searchproductregprice2.replaceAll(".*\\$","");
							System.out.println("searchproductregprice2trim: " +searchproductregprice2trim);	
							float searchproductregprice2trimfloat=Float.parseFloat(searchproductregprice2trim);
							System.out.println("searchproductregprice2trimfloat: " +searchproductregprice2trimfloat);		
							String searchproductseeitemforprice=driver.findElement(By.xpath("//*[@id='id_sPpdtContWrapper']/div[1]/div["+t+"]/div[1]/div[5]")).getText();
							System.out.println("searchproductseeitemforprice:" +searchproductseeitemforprice);
							if(searchproductsaleprice2trimfloat==streamstoresalepricevaluefloat)
							{
								String logErr210= "Pass:The search product gets details gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr210);
							}
							else
							{
								String logErr211= "Fail:The search product gets details doesn't gets matched\n" +searchproductid3+"\n" +searchproductimageurl3+ "\n" +searchproductnamedetails3+ "\n" +searchproductsaleprice2+ "\n" +searchproductregprice2+ "\n" +searchproductseeitemforprice+ "\n" +searchproductreviewcount1;   
								logInfo(logErr211);
							}	
							System.out.println("-----------------------------------------"); 
						}
					}
				}  
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
		}
		catch(Exception e)
		{
			System.out.println(e.toString());	
		}
	}	 
	private static void masterProductPage() 
	{	
		try
		{
			try
			{
				// Master Product Page 
				String logErr315 ="Master Product Page Functionalities"; 
				logInfo(logErr315);
				String logErr316="------------------"; 
				logInfo(logErr316);
				driver.manage().deleteAllCookies();
				driver.findElement(By.xpath("(//*[@class='cls_mamFSrch'])")).click();	
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).click();	
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_searchBox']")).sendKeys("bedd");
				Thread.sleep(3000);
				driver.findElement(By.xpath("//*[@id='id_searchBoxContainer']/div[3]/div[1]/span")).click();
				Thread.sleep(10000);		
				driver.findElement(By.xpath("//*[@id='id_sPfilterCont']/div[2]")).click();
				Thread.sleep(1000);
				String Masterproductid=driver.findElement(By.xpath("//*[@id='id_sPimageDiv_565623_master']")).getAttribute("prdtid");
				System.out.println("Masterproductid:" +Masterproductid);
				driver.findElement(By.xpath("//*[@id='id_sPimageDiv_565623_master']/img")).click();
				Thread.sleep(10000);
				//Master Product Stream JSON 	  	 
				String pdtUrl7 = "http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+Masterproductid+"?type=MASTER&campaignId=383";	
				URL url7  = new URL(pdtUrl7);
				String pageSource7  = new Scanner(url7.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource7:" +pageSource7);
				JSONObject StreamMasterJson = new JSONObject(pageSource7);
				System.out.println("StreamMasterJson:" +StreamMasterJson);
				//Master Stream Call data values 
				String streammasterproductname=StreamMasterJson.getString("name");
				System.out.println("streammasterproductname:" +streammasterproductname);
				JSONObject streammasterprop=StreamMasterJson.getJSONObject("properties");
				JSONObject streamiteminfo=streammasterprop.getJSONObject("iteminfo");
				JSONArray streammasterdescription=streamiteminfo.getJSONArray("description");
				JSONObject streammasterdescriptionobject=streammasterdescription.getJSONObject(0);
				String streammasterdescriptiontext=streammasterdescriptionobject.getString("value");
				System.out.println("streammasterdescriptiontext:" +streammasterdescriptiontext);
				String streammasterproductidentifier=StreamMasterJson.getString("identifier");
				System.out.println("streammasterproductidentifier:" +streammasterproductidentifier);
				//Master Product Details 
				String masterproductname=driver.findElement(By.xpath("//*[@id='id_decContainer']/div[1]/div")).getText();
				System.out.println("masterproductname:" +masterproductname);
				String masterproductdesc=driver.findElement(By.xpath("//*[@id='id_descriptionContent']/div/div[2]")).getText();
				System.out.println("masterproductdesc:" +masterproductdesc);
				String masterwebid=driver.findElement(By.xpath("//*[@id='id_decContainer']/div[2]/div[1]")).getText();
				System.out.println("masterwebid:" +masterwebid);		
				String masterwebidtrim=masterwebid.substring(masterwebid.indexOf("Web ID:")+9);
				System.out.println("masterwebidtrim:" +masterwebidtrim);
				String masterproductheadertitle=driver.findElement(By.xpath("//*[@id='id_rightContainerDivMaster']/div[1]/div[1]")).getText();
				System.out.println("masterproductheadertitle:" +masterproductheadertitle);
				String masterproductheaderdesc=driver.findElement(By.xpath("//*[@id='id_rightContainerDivMaster']/div[2]/div[1]")).getText();
				System.out.println("masterproductheaderdesc:" +masterproductheaderdesc);
				String logErr129= "Master Product Page";
				logInfo(logErr129);
				String logErr130="\n" +masterproductheadertitle ;
				logInfo(logErr130);	
				if(masterwebidtrim.equals(streammasterproductidentifier))
				{
					String logErr127= "Pass:The Master Product identifier & name value gets matched:\n" +masterwebidtrim+ "\n" +streammasterproductidentifier+ "\n" +streammasterproductname+ "\n" +masterproductname;
					logInfo(logErr127);
				}
				else
				{
					String logErr128= "Fail:The Master Product identifier & name value doesn't gets matched:\n" +masterwebidtrim+ "\n" +streammasterproductidentifier;
					logInfo(logErr128);
				}
				String logErr131="\n" +masterproductheaderdesc ;
				logInfo(logErr131);
				if(masterproductdesc.equals(streammasterdescriptiontext))
				{
					String logErr132= "Pass:The Master Product description value gets matched:\n" +masterproductdesc+ "\n" +streammasterdescriptiontext;
					logInfo(logErr132);
				}
				else
				{
					String logErr133= "Fail:The Master Product description value doesn't gets matched:\n" +masterproductdesc+ "\n" +streammasterdescriptiontext;
					logInfo(logErr133);
				}
				//master product primary image 	
				driver.findElement(By.xpath(".//*[@id='id_pdtLargeImg']")).click();  
				Thread.sleep(100);
				String MProductprimaryimageurl=driver.findElement(By.xpath("//*[@id='id_skImageScroller_0']/img")).getAttribute("src");
				System.out.println("MProductprimaryimageurl:" +MProductprimaryimageurl);
				String MProductprimaryimagetrim=MProductprimaryimageurl.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
				System.out.println("MProductprimaryimagetrim:" +MProductprimaryimagetrim);
				String masterproductimage=StreamMasterJson.getString("image");
				System.out.println("masterproductimage:" +masterproductimage);
				if(masterproductimage.equals(MProductprimaryimagetrim))
				{
					String logErr134= "Pass:The Product Page Primary Image value gets matched:\n" +masterproductimage+ "\n" +MProductprimaryimagetrim; 
					logInfo(logErr134);
				}
				else
				{
					String logErr135= "Fail:The Product Page Primary Image value doesn't gets matched:\n" +masterproductimage+ "\n" +MProductprimaryimagetrim; 
					logInfo(logErr135);
				}
				driver.findElement(By.xpath("(//*[@class='skrlPinchZoomClose'])")).click();
				Thread.sleep(1000);
				try
				{
					//Master Product Page:View Collection
					driver.findElement(By.xpath("//*[@id='id_rightContainerDivMaster']/div[1]/div[2]/div/div/div[3]")).click();
					Thread.sleep(2000);
					int memberproductcount=driver.findElements(By.xpath("//*[@id='id_ptdItemContainer']/div/div")).size();
					System.out.println("memberproductcount:" +memberproductcount);
					JSONObject streammasterchildren=StreamMasterJson.getJSONObject("children");
					JSONArray streammastermembproduct=streammasterchildren.getJSONArray("products");
					for(int c1=0,d1=1;c1<streammastermembproduct.length()||d1<=memberproductcount;c1++,d1++)
					{
						String memberproductwebID=driver.findElement(By.xpath("(//*[@class='childWebId'])["+d1+"]")).getText();
						System.out.println("memberproductwebID:" +memberproductwebID);
						String memberwebidtrim=memberproductwebID.substring(memberproductwebID.indexOf("Web ID:")+9);
						System.out.println("memberwebidtrim:" +memberwebidtrim);		
						driver.findElement(By.xpath("(//*[@class='checkPriceBtn'])["+d1+"]")).click();
						Thread.sleep(5000);
						String pdtUrl9 = "http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+memberwebidtrim+"?type=ID&storeid=28&campaignId=383";	
						URL url9  = new URL(pdtUrl7);
						String pageSource9  = new Scanner(url9.openConnection().getInputStream()).useDelimiter("\\Z").next();
						System.out.println("pageSource9:" +pageSource9);
						//More Color Check:::
						int memberproductcolorcountno=driver.findElements(By.xpath("//*[@id='id_masterColorCombo_"+memberwebidtrim+"']/div")).size();
						System.out.println("memberproductcolorcountno:" +memberproductcolorcountno);
						for(int more=1;more<=memberproductcolorcountno;more++)
						{   
							//Need to change the xpath for the more color condition
							if(memberproductcolorcountno>5)
							{  
								driver.findElement(By.xpath("(//*[@id='id_moreColors_"+memberwebidtrim+"'])")).click();
								Thread.sleep(1500);        
								driver.findElement(By.xpath("//*[@id='id_upcSwatch_"+memberwebidtrim+"']["+more+"]")).click();				                
								Thread.sleep(1000);
								driver.findElement(By.xpath("//*[@id='id_moreColorsPopup_"+memberwebidtrim+"']/div[1]/div[2]")).click();
							}
							else
							{			                 
								driver.findElement(By.xpath("//*[@id='id_upcSwatch_"+memberwebidtrim+"']["+more+"]")).click();
								Thread.sleep(1000);
							}				
							JSONObject StreamMemberJson = new JSONObject(pageSource9);
							System.out.println("StreamMemberJson:" +StreamMemberJson);	
							JSONObject streammembrupclevel=streammastermembproduct.getJSONObject(c1);
							String streammemberproductname=streammembrupclevel.getString("name");
							System.out.println("streammemberproductname:" +streammemberproductname);
							String streammemberpdtidentidier=streammembrupclevel.getString("identifier");
							System.out.println("streammemberpdtidentidier:" +streammemberpdtidentidier);
							String streammemberpdtimage=streammembrupclevel.getString("image");	
							System.out.println("streammemberpdtimage:" +streammemberpdtimage);	
							//Price Value
							JSONObject streammemberupcprop=streammembrupclevel.getJSONObject("properties");
							JSONObject streammemberbuyinfo=streammemberupcprop.getJSONObject("buyinfo");   
							JSONObject streammemberpricing=streammemberbuyinfo.getJSONObject("pricing");
							JSONArray  streammemberoverprices=streammemberpricing.getJSONArray("prices");
							JSONObject streammemberoverlayrreg=streammemberoverprices.getJSONObject(0);
							String  streammemberproductregprice=streammemberoverlayrreg.getString("value");					
							System.out.println("streammemberproductregprice:" +streammemberproductregprice);
							//String  streammemberproductregpricefina="$"+streammemberproductregprice;
							//String streammemberproductregpricefina1=streammemberproductregpricefina.replaceAll(".0.*","")+".00";
							//System.out.println("streammemberproductregpricefina1:" +streammemberproductregpricefina1);	
							String memberproductoverlaywebid=driver.findElement(By.xpath("(//*[@class='childWebId'])["+d1+"]")).getText();
							System.out.println("memberproductoverlaywebid:" +memberproductoverlaywebid);
							String memberproductoverlayimage=driver.findElement(By.xpath("(//*[@class='PtdContImg'])["+d1+"]")).getAttribute("src");
							System.out.println("memberproductoverlayimage:" +memberproductoverlayimage);
							String memberproductoverlayimagetrim=memberproductoverlayimage.replaceAll("&fmt=jpeg.*","")+"&fmt=jpeg";
							System.out.println("memberproductoverlayimagetrim:" +memberproductoverlayimagetrim);	
							String memberproductoverlayname=driver.findElement(By.xpath("//*[@id='id_chldPdtHeading_"+memberwebidtrim+"']/div")).getText();
							System.out.println("memberproductoverlayname:" +memberproductoverlayname);
							if(memberwebidtrim.equals(streammemberpdtidentidier))
							{
								String logErr136= "Pass:The Member Product WebID,Image and Name value gets matched:\n" +streammemberpdtidentidier+ "\n" +streammemberpdtimage+ "\n" +streammemberproductname; 
								logInfo(logErr136);
							}
							else
							{
								String logErr137= "Fail:The Member Product WebID,Image and Name value gets matched:\n" +streammemberpdtidentidier+ "\n" +streammemberpdtimage+ "\n" +streammemberproductname; 
								logInfo(logErr137);
							}		
							Thread.sleep(1000);
							String memberproductoverlaysprice=driver.findElement(By.xpath("(//*[@class='rlprice'])["+d1+"]")).getText();			
							System.out.println("memberproductoverlaysprice:" +memberproductoverlaysprice);
							String memberproductoverlayspricetrim=memberproductoverlaysprice.replaceAll(".*\\$","");
							System.out.println("memberproductoverlayspricetrim: " +memberproductoverlayspricetrim);		
							String memberproductoverlayprice=driver.findElement(By.xpath("(//*[@class='rlorgprice hasprice'])["+d1+"]")).getText();			
							System.out.println("memberproductoverlayprice:" +memberproductoverlayprice);
							String memberproductoverlaypricetrim=memberproductoverlayprice.replaceAll(".*\\$","");
							System.out.println("memberproductoverlaypricetrim: " +memberproductoverlaypricetrim);
							if(streammemberproductregprice.equals(memberproductoverlaypricetrim))   		
							{
								String logErr138= "Pass:The Member Product price value gets matched:\n" +memberproductoverlaysprice+ "\n"+memberproductoverlayprice; 
								logInfo(logErr138);
							}
							else
							{
								String logErr139= "Fail:The Member Product price value doesn't gets matched:\n" +memberproductoverlaysprice+ "\n" +memberproductoverlayprice; 
								logInfo(logErr139);
							}
							Thread.sleep(1000);
							//Add to bag functionality 
							JSONArray streammemberavailability=streammemberbuyinfo.getJSONArray("availability");
							JSONObject streammemberavailabilityvalue=streammemberavailability.getJSONObject(0);
							String streammemberavailabilityinstore=streammemberavailabilityvalue.getString("instore");
							System.out.println("streammemberavailabilityinstore:" +streammemberavailabilityinstore);
							String streammemberavailabilityonline=streammemberavailabilityvalue.getString("online");
							System.out.println("streammemberavailabilityonline:" +streammemberavailabilityonline);		
							if(streammemberavailabilityonline.equals("true"))
							{
								String logErr140= "Pass:The UPC is Available for shipping"; 
								logInfo(logErr140);
								Thread.sleep(1000);         
								driver.findElement(By.xpath("//*[@id='id_addtoOrder_"+streammemberpdtidentidier+"']")).click();
								Thread.sleep(5000);
								String memberpdtaddtobagoverlayname=driver.findElement(By.xpath("(//*[@class='overlayPdtTitle'])")).getText();
								System.out.println("memberpdtaddtobagoverlayname:" +memberpdtaddtobagoverlayname);
								String memberpdtaddtobagoverlaywebid=driver.findElement(By.xpath("(//*[@class='overlayPdtWebId'])")).getText();
								System.out.println("memberpdtaddtobagoverlaywebid:" +memberpdtaddtobagoverlaywebid);
								String memberpdtaddtobagoverlaywebidtrim=memberproductwebID.substring(memberpdtaddtobagoverlaywebid.indexOf("Web ID:")+8);
								System.out.println("memberpdtaddtobagoverlaywebidtrim:" +memberpdtaddtobagoverlaywebidtrim);	
								String memberpdtaddtobagoverlayprice=driver.findElement(By.xpath("(//*[@class='overlayPriceDiv'])")).getText();
								System.out.println("memberpdtaddtobagoverlayprice:" +memberpdtaddtobagoverlayprice);		
								String memberpdtaddtobagoverlayimage=driver.findElement(By.xpath("(//*[@class='overlayProductImg'])")).getAttribute("src");
								System.out.println("memberpdtaddtobagoverlayimage:" +memberpdtaddtobagoverlayimage);
								driver.findElement(By.xpath("(//*[@class='closeBtnDiv skrlAddtoOrderClose'])")).click();
								if(memberpdtaddtobagoverlayname.equals(streammemberproductname)&&memberpdtaddtobagoverlaywebidtrim.equals(streammemberpdtidentidier)&&memberpdtaddtobagoverlayprice.equals(streammemberproductregprice)&&memberpdtaddtobagoverlayimage.equals(memberproductoverlayimagetrim))
								{
									String logErr142= "Pass:The Added items to the shopping bag values are gets matched:\n"+memberpdtaddtobagoverlayname+ "\n" +memberpdtaddtobagoverlaywebidtrim+"\n"+memberpdtaddtobagoverlayprice+"\n"+memberpdtaddtobagoverlayimage;
									logInfo(logErr142);
								}
								else
								{
									String logErr143= "Fail:The Added items to the shopping bag values doesn't  gets matched:\n"+memberpdtaddtobagoverlayname+ "\n" +memberpdtaddtobagoverlaywebidtrim+"\n"+memberpdtaddtobagoverlayprice+"\n"+memberpdtaddtobagoverlayimage;
									logInfo(logErr143);

								}
								Thread.sleep(1000);
							}
							else
							{
								String logErr141= "Pass:The UPC is not Available for shipping";
								logInfo(logErr141);
							}
							//Member Product Check other stores
							driver.findElement(By.xpath("//*[@id='id_otherStores_"+streammemberpdtidentidier+"']/span")).click();
							Thread.sleep(3000);
							String memberproductupcvalues= driver.findElement(By.xpath("//*[@id='id_chilUpc_"+streammemberpdtidentidier+"']")).getText();
							System.out.println("memberproductupcvalues:" +memberproductupcvalues);
							String memberproductupcvaluestrim=memberproductupcvalues.substring(memberproductupcvalues.indexOf("UPC:")+5);
							System.out.println("memberproductupcvaluestrim:" +memberproductupcvaluestrim);  	
							String membpdtupcvalue = "http://social.macys.com/skavastream/core/v5/bloomingdales/product/"+memberproductupcvaluestrim+"?type=UPCWithNearbyStores&storeid=28&campaignId=383";
							URL url20  = new URL(membpdtupcvalue);
							String pageSource20  = new Scanner(url20.openConnection().getInputStream()).useDelimiter("\\Z").next();
							System.out.println("pageSource20:" +pageSource20);
							JSONObject StreammembPDPUpccheckotherstores=new JSONObject(pageSource20);
							int memberproductcheckotherscount=driver.findElements(By.xpath("//*[@id='storeContainer']/div")).size();
							System.out.println("memberproductcheckotherscount:" +memberproductcheckotherscount);
							JSONObject streammemberupcchildren=StreammembPDPUpccheckotherstores.getJSONObject("children");
							JSONArray  streammemberproductsku= streammemberupcchildren.getJSONArray("skus");
							JSONObject streammemberproductskuvalue=streammemberproductsku.getJSONObject(0);
							JSONObject streammemberproductproperties=streammemberproductskuvalue.getJSONObject("properties");
							JSONArray  streammemberproductstoreinfo=streammemberproductproperties.getJSONArray("storeinfo");
							for(int e1=0,f1=1;e1<streammemberproductstoreinfo.length()||f1<=memberproductcheckotherscount;e1++,f1++)
							{
								JSONObject streammemberproductskuvalues=streammemberproductstoreinfo.getJSONObject(e1);
								String streammemberproductstorephoneno=streammemberproductskuvalues.getString("phone");
								System.out.println("streammemberproductstorephoneno:" +streammemberproductstorephoneno);
								String streammemberproductstorename=streammemberproductskuvalues.getString("name");		
								System.out.println("streammemberproductstorename:" +streammemberproductstorename);
								String streammemberproductstorezipcode=streammemberproductskuvalues.getString("zipcode");
								System.out.println("streammemberproductstorezipcode:" +streammemberproductstorezipcode);
								String streammemberproductstorestate=streammemberproductskuvalues.getString("state");
								System.out.println("streammemberproductstorestate:" +streammemberproductstorestate);
								String streammemberproductstoreaddress1=streammemberproductskuvalues.getString("address1");
								System.out.println("streammemberproductstoreaddress1:" +streammemberproductstorestate);
								String streammemberproductstoreaddress2=streammemberproductskuvalues.getString("address2");
								System.out.println("streammemberproductstoreaddress2:" +streammemberproductstorestate);
								String streammemberproductstorecity=streammemberproductskuvalues.getString("city");
								System.out.println("streammemberproductstorecity:" +streammemberproductstorecity);
								String streammemberproductstoreinventory=streammemberproductskuvalues.getString("inventory");
								System.out.println("streammemberproductstoreinventory:" +streammemberproductstoreinventory);	
								String streammemberproductidentifier=streammemberproductskuvalues.getString("identifier");
								System.out.println("streammemberproductidentifier:" +streammemberproductidentifier);			
								String streammemberstoreinfoNameconcat=streammemberproductstorename+streammemberproductidentifier;
								System.out.println("streammemberstoreinfoNameconcat:" +streammemberstoreinfoNameconcat);
								String streammemberstorenamecombined=streammemberstoreinfoNameconcat.replaceAll(""+streammemberproductidentifier+".*","")+" "+"("+streammemberproductidentifier+")";
								System.out.println("streammemberstorenamecombined:" +streammemberstorenamecombined);
								driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div")).click();
								Thread.sleep(1000);
								String memberproductstorename=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div/div[2]/div")).getText();
								System.out.println("memberproductstorename:" +memberproductstorename);
								String memberproductstoreinventory=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div/div[3]/div")).getText(); 
								System.out.println("memberproductstoreinventory:" +memberproductstoreinventory);
								String memberproductstreetname=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[2]/div/div[1]")).getText();
								System.out.println("memberproductstreetname:" +memberproductstreetname);
								String memberproductstreetlocation=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[2]/div/div[2]")).getText();  
								System.out.println("memberproductstreetlocation:" +memberproductstreetlocation);
								String memberproductstorephonenumber=driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[2]/div/div[3]")).getText();
								System.out.println("memberproductstorephonenumber:" +memberproductstorephonenumber);		
								driver.findElement(By.xpath("//*[@id='storeContainer']/div["+f1+"]/div[1]/div/div[2]/div")).click();
								if(streammemberstorenamecombined.equals(memberproductstorename)&&streammemberproductstoreinventory.equals(memberproductstoreinventory)&&streammemberproductstoreaddress1.equals(memberproductstreetname)&&streammemberproductstoreaddress2.equals(memberproductstreetlocation)&&streammemberproductstorephoneno.equals(memberproductstorephonenumber))
								{
									String logErr158= "Pass:The Member product overlay Check Other Store values are gets matched:\n" +streammemberstorenamecombined+"\n" +streammemberproductstoreinventory+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstorephoneno;
									logInfo(logErr158);

								}
								else
								{
									String logErr159= "Pass:The Member product overlay Check Other Store values are gets matched:\n" +streammemberstorenamecombined+"\n" +streammemberproductstoreinventory+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstoreaddress1+ "\n" +streammemberproductstorephoneno;
									logInfo(logErr159);
								}			
							}
							driver.findElement(By.xpath("//*[@id='id_masterPdpSeeAllAvailable']/div/div[4]")).click();
						}
						Thread.sleep(3000);
						WebElement elec;	
						System.out.println("START");
						elec=driver.findElement(By.xpath("//*[@id='id_shipavailableStore_"+memberwebidtrim+"']")); 
						System.out.println("MIDDLE"); 
						Actions abc = new Actions(driver);
						System.out.println("Action");
						abc.dragAndDropBy(elec,0,-400).build().perform();
						System.out.println("Drag");
					}                          
					driver.findElement(By.xpath("//*[@id='id_chooseItemsOverlay']/div/div[2]")).click();		
				}
				catch(Exception e)
				{
					System.out.println(e.toString());
				}
				try
				{
					//Review Comparison	
					String streamproductreview=driver.findElement(By.xpath("//*[@id='reviewCont']/div[1]/div[1]")).getText();
					System.out.println("streamproductreview:" +streamproductreview);
					if(streamproductreview.contains("REVIEW"))
					{
						driver.findElement(By.xpath("//*[@id='reviewCont']/div[1]/div[1]")).click();
						Thread.sleep(6000);
						String masterproductreviewheader=driver.findElement(By.xpath("//*[@id='id_pdtReviewContainer']/div[1]")).getText();
						System.out.println("masterproductreviewheader:" +masterproductreviewheader);
						if(masterproductreviewheader.equals("Customer Reviews"))
						{
							String logErr125= "\nMaster Product:" +masterproductreviewheader; 
							logInfo(logErr125);
						}
						//Review Stream Call JSON   
						String pdtUrl8 = "http://social.macys.com/skavastream/ratingreview/v5/bloomingdales/reviews/"+Masterproductid+"?campaignId=383&sort=desc&offset=0&limit=100";	
						URL url8  = new URL(pdtUrl8);
						String pageSource8  = new Scanner(url8.openConnection().getInputStream()).useDelimiter("\\Z").next();
						System.out.println("pageSource8:" +pageSource8);
						JSONObject StreamReviewJson = new JSONObject(pageSource8);
						System.out.println("StreamReviewJson:" +StreamReviewJson);
						String streamreviewusername=StreamReviewJson.getString("name");
						System.out.println("streamreviewusername:" +streamreviewusername);
						JSONObject Streamreviewprop=StreamReviewJson.getJSONObject("properties");
						JSONObject Streamreviewratings=Streamreviewprop.getJSONObject("reviewrating");
						int Streamreviewcount=Streamreviewratings.getInt("reviewcount");
						System.out.println("Streamreviewcount:" +Streamreviewcount);
						JSONArray Streamreviews=Streamreviewratings.getJSONArray("reviews");	
						for(int a1=0,b1=1;a1<Streamreviews.length()||b1<=Streamreviewcount;a1++,b1++)
						{
							//Stream review details 
							JSONObject streamreviewdetails=Streamreviews.getJSONObject(a1);
							String streamreviewlabel=streamreviewdetails.getString("label");
							System.out.println("streamreviewlabel:" +streamreviewlabel);
							String streamreviewcreated=streamreviewdetails.getString("created");
							System.out.println("streamreviewcreated:" +streamreviewcreated);
							String streamreviewcreatedtrim=streamreviewcreated.replaceAll("T.*","");    		
							System.out.println("streamreviewcreatedtrim:" +streamreviewcreatedtrim);
							String streamreviewvalue=streamreviewdetails.getString("value");
							System.out.println("streamreviewvalue:" +streamreviewvalue);
							//Reviewpanel in the PDP Page 
							String reviewpanelusername=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[1]/div[2]")).getText();   
							System.out.println("reviewpanelusername:" +reviewpanelusername);
							String reviewpanelreviewname=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[1]/b")).getText();
							System.out.println("reviewpanelreviewname:" +reviewpanelreviewname);
							String reviewpanelreviewdate=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[2]/i")).getText();
							System.out.println("reviewpanelreviewdate:" +reviewpanelreviewdate);		
							String reviewpanelreviewcomments=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[3]")).getText(); 
							System.out.println("reviewpanelreviewcomments:" +reviewpanelreviewcomments);
							WebElement ele;	
							System.out.println("START");
							ele=driver.findElement(By.xpath("//*[@id='id_reviewWrapper']/div[1]/div["+b1+"]/div[2]/div[3]")); 
							System.out.println("MIDDLE"); 
							Actions ab = new Actions(driver);
							System.out.println("Action");
							ab.dragAndDropBy(ele,0,-60).build().perform();
							System.out.println("Drag");
							if(streamreviewlabel.equals(reviewpanelusername))
							{
								String logErr123= "Pass:The Review Panels details are gets matched with the stream call:\n"  +streamreviewlabel+ "\n" +streamreviewusername+ "\n" +reviewpanelreviewdate+ "\n" +streamreviewvalue;   
								logInfo(logErr123);
								String logErr125="-------------------------------------------------";
								logInfo(logErr125);
							}  
							else
							{
								String logErr124= "Fail:The Review Panels details doesn't gets matched with the stream call:\n"  +streamreviewlabel+ "\n" +streamreviewusername+ "\n" +reviewpanelreviewdate+ "\n" +streamreviewvalue;   
								logInfo(logErr124);
								String logErr126="-------------------------------------------------";
								logInfo(logErr126);
							}
							System.out.println("-------------------------------------------------------------------");
						}
					} 

				}
				catch(Exception e)
				{
					System.out.println(e.toString());
				}	
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}		
		catch(Exception e)
		{
			System.out.println(e.toString());	
		}
	}	
	private static void  browsePagePriceValidation()
	{ 
		try
		{
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[13]")).click();
			Thread.sleep(2000);
			try

			{
				//Selecting randomly the categories in  the HomePage 
				Random r2 = new java.util.Random();
				List<WebElement> links = driver.findElements(By.xpath("//*[@id='skPageLayoutCell_1_id-center-north-center']/div/div"));
				WebElement randomElement = links.get(r2.nextInt(links.size()));
				randomElement.click();
				Thread.sleep(4000);  
				String logErr301 ="Pass:The Site gets launched Successfully"; 
				logInfo(logErr301);
			}
			catch(Exception e)
			{
				System.out.println(e.toString());
			} 
			Thread.sleep(2000);
			//Browse Page Functionalities
			String logErr900 ="Browse Page-Price Validations"; 
			logInfo(logErr900);
			String logErr901="-------------------------------"; 
			logInfo(logErr901);
			Thread.sleep(2000);
			//Price High to Low - Lookbook tab
			//driver.findElement(By.xpath("//*[@id='id_bPsortByCont']/div[3]/div[2]")).click();
			//Thread.sleep(1000);
			//driver.findElement(By.xpath("//*[@id='id_sortByOpt_1']/span")).click();
			//Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
			Thread.sleep(1000);
			int browseproductcount=driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div")).size();
			System.out.println("browseproductcount:" +browseproductcount);
			int browseproducttrim=browseproductcount-1;
			System.out.println("browseproducttrim:" +browseproducttrim);
			for(int br=1;br<=2;br++)
			{
				//Sitelet details                                     
				String productid=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage bPimageDivBorder'])["+br+"]")).getAttribute("prdtid");
				System.out.println("productid:" +productid);         
				String browseproductname=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+br+"]")).getText();
				System.out.println("browseproductname:" +browseproductname);		  		
				String browsesitesaleprice=driver.findElement(By.xpath("(//*[@class='bPpdtSalePriceDiv'])["+br+"]")).getText();
				System.out.println("browsesitesaleprice:" +browsesitesaleprice);   
				String browsesiteregprice1=driver.findElement(By.xpath("(//*[@class='bPpdtRegPriceDiv'])["+br+"]")).getText();
				System.out.println("browsesiteregprice:" +browsesiteregprice1);		
				String browsesiteregpricetrim1=browsesiteregprice1.replaceAll(".*\\$","");
				System.out.println("browsesiteregpricetrim1: " +browsesiteregpricetrim1);	
				Float browsesiteregpricefloat=Float.parseFloat(browsesiteregpricetrim1);
				//Local Data 
				String url1 = "C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtcategory_lookbook.js";
				String pagesourcelnk=readFile(url1);
				System.out.println("pageSource:" +pagesourcelnk); 	
				String pageSourcetrimT=pagesourcelnk.substring(pagesourcelnk.indexOf("skone_callback(")+15,pagesourcelnk.lastIndexOf(")"));  
				System.out.println("Timming");
				System.out.println("pageSourcetrimT:" +pageSourcetrimT);
				JSONObject browselbpricedetails=new JSONObject(pageSourcetrimT);
				try
				{
					JSONObject browselbchprice=browselbpricedetails.getJSONObject("children");
					JSONArray browselbproducts=browselbchprice.getJSONArray("products");
					for(int lb=0;lb<browselbproducts.length();lb++)
					{
						JSONObject browselbproductobj=browselbproducts.getJSONObject(lb);
						String browselookbookidentifier=browselbproductobj.getString("identifier");
						System.out.println("browselookbookidentifier:" +browselookbookidentifier);
						JSONObject browselbproperties=browselbproductobj.getJSONObject("properties");
						JSONObject browselbbuyinfo=browselbproperties.getJSONObject("buyinfo");
						JSONObject browselbpricing=browselbbuyinfo.getJSONObject("pricing");
						JSONArray browselbprices=browselbpricing.getJSONArray("prices");
						JSONObject browselbregpriceobj=browselbprices.getJSONObject(0);
						String  browseregprice=browselbregpriceobj.getString("value");
						System.out.println("browseregprice:" +browseregprice);
						Float browseregpricefloat=Float.parseFloat(browseregprice);			
						if(productid.equals(browselookbookidentifier))
						{
							//BrowsePage Logic
							if(browsesiteregpricefloat.equals(browsesiteregpricefloat))
							{
								String logErr878= "Pass:The Product Contains Regprice in black:\n" +productid+ "\n" +browseproductname+"\n" +browseregpricefloat; 
								logInfo(logErr878);
							}
							else
							{
								//8.Verify that  "See Item for Price" should be displayed for the products in the browse page for the Lookbook  products  when there is no price file in the "jstore & jcategory_lookbook.js" price files 
								String logErrsmp8 ="8,Verify that  See Item for Price should be displayed for the products in the browse page for the Lookbook  products  when there is no price file in the  jstore & jcategory_lookbook.js price files,Pass"; 
								logInfo1(logErrsmp8);				
							}
						}
					}
				}			
				catch(Exception e)
				{
					System.out.println(e.toString());
				}			
			}
			//7.Verify that "Jstore" price file should be displayed in the browse page for the products if there is the "jstore" zone file for the category or " jcategory_lookbook.js" price value  should be displayed for the products in the browse page for the  lookbbok products   
			String logErrsmp7 ="7,Verify that Jstore price file should be displayed in the browse page for the products if there is the jstore zone file for the category or  jcategory_lookbook.js price value  should be displayed for the products in the browse page for the  lookbbok products, Pass"; 
			logInfo1(logErrsmp7);
			Thread.sleep(3000);
			driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='bp_tab_0']")).click();
			Thread.sleep(5000);
			//4.Verify that while selecting the ".COM/Lookbook" tabs  in the browse page, the respective tab products should be displayed in the browse page 
			if(driver.findElement(By.xpath("//*[@id='id_bPpdtImage_0']")).isDisplayed())
			{
				String logErrsmp4="4,Verify that while selecting the .COM/Lookbook tabs  in the browse page the respective tab products should be displayed in the browse page, Pass"; 
				logInfo1(logErrsmp4);
			}
			else
			{
				String logErrsmf4 ="4,Verify that while selecting the .COM/Lookbook tabs  in the browse page the respective tab products should be displayed in the browse page,  Fail"; 
				logInfo1(logErrsmf4);
			}
			//Browse Page: .COM Products  [Price High to Low]
			driver.findElement(By.xpath("//*[@id='id_bPsortByCont']/div[3]/div[2]")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//*[@id='id_sortByOpt_1']/span")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
			Thread.sleep(1000);
			int browsecomproductcount=driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div")).size();
			System.out.println("browseproductcount:" +browsecomproductcount);
			int browsecomproducttrim=browsecomproductcount-1;
			System.out.println("browsecomproducttrim:" +browsecomproducttrim);
			for(int brc=1;brc<=8;brc++)
			{
				//Sitelet details                                      
				String productidcom=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+brc+"]")).getAttribute("prdtid");
				System.out.println("productid:" +productidcom);                 
				String browseproductnamecom=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+brc+"]")).getText();
				System.out.println("browseproductname:" +browseproductnamecom);			  	
				String browsesitesalepricecom=driver.findElement(By.xpath("(//*[@class='bPpdtSalePriceDiv'])["+brc+"]")).getText();
				System.out.println("browsesitesaleprice:" +browsesitesalepricecom);  
				String browsesiteregpricecom=driver.findElement(By.xpath("(//*[@class='bPpdtRegPriceDiv'])["+brc+"]")).getText();
				System.out.println("browsesiteregprice:" +browsesiteregpricecom);
				//Local Data 
				String url111 = "C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtstore_2_PDT.js";
				String pageSource1=readFile(url111);
				//URL url11  = new URL(url1);
				//String pageSource  = new Scanner(url11.openConnection().getInputStream()).useDelimiter("\\Z").next();
				System.out.println("pageSource1:" +pageSource1); 	
				String pageSourcetrim=pageSource1.substring(pageSource1.indexOf("skone_callback(")+15,pageSource1.indexOf(")"));
				System.out.println("pageSourcetrim:" +pageSourcetrim);
				JSONObject browseclbpricedetails=new JSONObject(pageSourcetrim);
				String Filetime=browseclbpricedetails.getString("filetime");
				System.out.println("Filetime:" +Filetime);
				try
				{
					JSONObject browsecomprice=browseclbpricedetails.getJSONObject("prices");
					JSONObject browsecompriceid=browsecomprice.getJSONObject(productidcom);
					System.out.println("browsecompriceid:" +browsecompriceid);
					String browsecomregprice=browsecompriceid.getString("reg");
					System.out.println("browsecomregprice:" +browsecomregprice);
					Float browsecomregpricefloat=Float.parseFloat(browsecomregprice);	
					String browsecomsaleprice=browsecompriceid.getString("sale");
					System.out.println("browsecomsaleprice:" +browsecomsaleprice);		
					Float browsecomsalepricefloat=Float.parseFloat(browsecomsaleprice);		
					//BrowsePage Logic	
					if(browsecomsalepricefloat.compareTo(browsecomregpricefloat)==0)
					{
						String browsesiteregprice1=driver.findElement(By.xpath("(//*[@class='bPpdtRegPriceDiv'])["+brc+"]")).getText();
						System.out.println("browsesiteregprice1:" +browsesiteregprice1);
						String browsesiteregpricetrim=browsesiteregprice1.replaceAll(".*\\$","");
						System.out.println("browsesiteregpricetrim: " +browsesiteregpricetrim);
						float browsesiteregpricetrimfloat=Float.parseFloat(browsesiteregpricetrim);	
						if((browsesiteregpricetrimfloat==browsecomregpricefloat))
						{
							String logErr878= "Pass:The Product Contains Regprice/SalePrice in black:\n" +productidcom+ "\n" +browseproductnamecom+ "\n"+browsecomsalepricefloat+"\n" +browsesiteregprice1; 
							logInfo(logErr878);
						}
						else 
						{
							String logErr8781= "Fail:The Product doesn't gets displayed Regprice/SalePrice in black:\n" +productidcom+ "\n" +browseproductnamecom+ "\n"+browsecomsalepricefloat+"\n" +browsesiteregprice1; 
							logInfo(logErr8781);
						}
					}
					// Current/Store<Original				
					else if(browsecomsalepricefloat.compareTo(browsecomregpricefloat)<0)
					{
						String browsesitesaleprice1=driver.findElement(By.xpath("(//*[@class='bPpdtSalePriceDiv'])["+brc+"]")).getText();
						System.out.println("browsesitesaleprice1:" +browsesitesaleprice1);	
						String browsesitesalepricetrim=browsesitesaleprice1.replaceAll(".*\\$","");
						System.out.println("browsesitesalepricetrim: " +browsesitesalepricetrim);
						float browsesitesalepricetrimfloat=Float.parseFloat(browsesitesalepricetrim);		
						String browsesiteregprice2=driver.findElement(By.xpath("(//*[@class='bPpdtRegPriceDiv'])["+brc+"]")).getText();
						System.out.println("browsesiteregprice2:" +browsesiteregprice2);
						String browsesiteregpricetrim=browsesiteregprice2.replaceAll(".*\\$","");
						System.out.println("browsesiteregpricetrim: " +browsesiteregpricetrim);
						float browsesiteregpricetrimfloat=Float.parseFloat(browsesiteregpricetrim);		
						if((browsesitesalepricetrimfloat==browsecomsalepricefloat)&&(browsesiteregpricetrimfloat==browsecomregpricefloat))
						{
							String logErr879= "Pass:The Product Contains SalePrice in Red Regprice in Black label:\n" +productidcom+ "\n" +browseproductnamecom+ "\n" +browsesitesaleprice1+"\n" +browsesiteregprice2; 
							logInfo(logErr879);
						}
						else
						{
							String logErr8791= "Fail:The Product doesn't gets displayed SalePrice in Red Regprice in Black label:\n" +productidcom+ "\n" +browseproductnamecom+ "\n" +browsesitesaleprice1+"\n" +browsesiteregprice2; 
							logInfo(logErr8791);
						}
					}		
				}
				catch(Exception e)
				{
					System.out.println(e.toString());
				}			
			}
			//6.Verify that "jstore" price file should be displayed in the browse page for the products if there is the "jstore" zone file for the category  or " See Item for Price" should be displayed for the products in the browse page for the .COM products    
			String logErrsmp6 ="6,Verify that jstore price file should be displayed in the browse page for the products if there is the jstore zone file for the category  or  See Item for Price should be displayed for the products in the browse page for the .COM products, Pass"; 
			logInfo1(logErrsmp6);
			Thread.sleep(3000);					
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}	
	private static void browseFacetsPage()
	{
		try
		{
			try
			{
				//Browse Page Functionalities
				String logErr951 ="Browse Page Functionalities"; 
				logInfo(logErr951);
				String logErr952="------------------"; 
				logInfo(logErr952);		
				//3.Verify that products are displayed in the browse page according to the selected facets 
				driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[13]")).click();
				Thread.sleep(2000);	
				//Kate Spade NewYork
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_1_id-center-north-center']/div/div[1]")).click();
				Thread.sleep(2000);			
				//Switching to .COM Tab
				driver.findElement(By.xpath("//*[@id='bp_tab_0']")).click();
				Thread.sleep(5000);
				//Selecting the Small Size Facets
				driver.findElement(By.xpath("//*[@id='id_bPsizeSwatch_Size_Small']/div/div")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
				Thread.sleep(2000);
				int browsefacetscount=driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div")).size();
				System.out.println("browsefacetscount:" +browsefacetscount);			
				String url12="C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtcategory1001396.js";
				//URL url112  = new URL(url12);
				//String pageSource12  = new Scanner(url112.openConnection().getInputStream()).useDelimiter("\\Z").next();
				String pageSource12=readFile(url12);
				System.out.println("pageSource12:" +pageSource12); 	
				String pageSourcefacetstrim=pageSource12.substring(pageSource12.indexOf("skone_callback(")+15,pageSource12.lastIndexOf(")"));
				System.out.println("pageSourcefacetstrim:" +pageSourcefacetstrim);
				JSONObject browsepricedetails=new JSONObject(pageSourcefacetstrim);	
				JSONArray browsefacets=browsepricedetails.getJSONArray("facets");
				JSONObject browsesizefacets=browsefacets.getJSONObject(5);
				JSONArray browsesizefacetsvalue=browsesizefacets.getJSONArray("values");
				JSONObject  browsesizefacetsvalues=browsesizefacetsvalue.getJSONObject(0);
				JSONArray browsesizefacetsvaluesvalues=browsesizefacetsvalues.getJSONArray("value");
				String logErr897="Size Facets Selection Functionalities"; 
				logInfo(logErr897);
				String logErr898="-------------------------------------"; 
				logInfo(logErr898);
				for(int browsesize=1;browsesize<=browsefacetscount;browsesize++)
				{
					String browsesizefacetsstring=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+browsesize+"]")).getAttribute("prdtid");					
					System.out.println("browsesizefacetsstring:" +browsesizefacetsstring);
					for(int size=0;size<browsesizefacetsvaluesvalues.length();size++)
					{
						String browsesizeproductidsize=browsesizefacetsvaluesvalues.getString(size);
						System.out.println("browsesizeproductidsize:" +browsesizeproductidsize);
						if(browsesizefacetsstring.equals(browsesizeproductidsize))
						{
							String logErr890="Pass:Browse Page Size Facets value gets match:\n" +browsesizeproductidsize+ "\n" +browsesizefacetsstring; 
							logInfo(logErr890);
						}		
					}	
				}
				//3.Verify that products are displayed in the browse page according to the selected facets 
				String logErrsmp3 ="3,Verify that products are displayed in the browse page according to the selected facets, Pass"; 
				logInfo1(logErrsmp3);
				//Geneder>Male
				driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']/div/div[2]")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER_DIANE von FURSTENBERG']/div")).click();
				Thread.sleep(1000);
				driver.findElement(By.cssSelector("html.ui-mobile body.ui-mobile-viewport.skBloomies.ui-overlay-c div#studiop_3.scfPage.skcWebPage.ui-page.ui-page-theme-c.ui-page-active div#id_scfContent_.scfContent div#browse.page.lyt_cont_div div#skPageLayoutCell_3_id-center.skc_pageCellLayout.cls_skWidget.BrowsePage_widgetCloneClone div#skPageLayoutCell_3_id-center.cls_customWidget div#id_BrowsePageContent.BrowsePageContent.skPageContainer div#id_bPfacetSubMenu.bPfacetSubMenu.subslider div#id_bPfilterCont.bPfilterCont div.bPfacetSubLeftArrow")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='id_bPfilterCont']/div[2]")).click();
				Thread.sleep(1000);
				int browsecategoryfacets=driver.findElements(By.xpath("//*[@id='id_bPpdtContWrapper']/div[1]/div")).size(); 
				System.out.println("browsecategoryfacets:" +browsecategoryfacets);
				JSONObject browsecatfacets=browsefacets.getJSONObject(0);
				JSONArray  browsecatfacetsvalue=browsecatfacets.getJSONArray("values");
				JSONObject browsecatfacetsvalues=browsecatfacetsvalue.getJSONObject(22);
				JSONArray browsecatfacetsvaluesvalues=browsecatfacetsvalues.getJSONArray("value");	
				String logErr895="Category Facets Selection Functionalities"; 
				logInfo(logErr895);
				String logErr896="------------------------------------------"; 
				logInfo(logErr896);	
				for(int bcgs=1;bcgs<=browsecategoryfacets;bcgs++)
				{
					String browsecatfacetsstring=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+bcgs+"]")).getAttribute("prdtid");
					System.out.println("browsecatfacetsstring:" +browsecatfacetsstring);
					for(int csize=0;csize<browsecatfacetsvaluesvalues.length();csize++)
					{
						String browsecatproductidsize=browsecatfacetsvaluesvalues.getString(csize);
						System.out.println("browsecatproductidsize:" +browsecatproductidsize);					
						if(browsecatfacetsstring.equals(browsecatproductidsize))
						{
							String logErr894="Pass:Browse Page Category Facets value gets match:\n" +browsecatfacetsstring+ "\n" +browsecatproductidsize; 
							logInfo(logErr894);
						}		
					}	
				}
				//3.Verify that products are displayed in the browse page according to the selected facets 
				String logErrsmp31 ="3,Verify that products are displayed in the browse page according to the selected facets, Pass"; 
				logInfo1(logErrsmp31);
			}	
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
			driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[13]")).click();
		}
		catch(Exception e)
		{
			System.out.println(e.toString());	
		}
	}
	/* BCOM-- See All Designers Page Scenarios 
	1.While selecting the "See All Designers" option in the home page, the see all designers page will be displayed 
	2.Check for the browsepage_id in the query param in the see all designers page 
	3.The brand name in the "See all designers" page will be verified with the jtbrand.js file 
	4.If the see all designers page contains browsepage_id,the equivalent jtcategory_xxxx.js will be referred 
	5.Navigate to the individual brand page and check whether the "PageType" is avail in the queryparam or not 
	6.If the brand page contains the pageType,check whether the selected brand name is selected in the filter facets and the following functionalities will be manipulated 
	> pageType=comonly - The products displayed in the brand page will be verified with the jtcategory_xxxx.js file
	> pageType=comandlookbook - The products displayed in the brand page will be verified with the jtcategory_xxxx.js & jtcategory_lookbook.js file
	> pageType=lookbookonly - The products displayed in the brand page will be verified with the jtcategory_lookbook.js file
	7.If the brand page doesn't contains the pageType,check whether the selected brand name is selected in the filter facets and the tab should be displayed in the brand page accordint to the "pageType": "COM and Lookbook" in the browse config
	8.If the "see all designer" page doesn't contain the browsepage_id in the query param,the categories in the jtbrand should be displayed in the "See all designers"  overlay 
	9.If the jtbrand contains only lookbook it should navigate to the brand page when the  "see all designer" page doesn't contain the browsepage_id in the query param
	10.Verified that while selecting brand name alphabets at the header,the specific brand name sections will be displayed 
	 */
	private static void browsePageSeeAllDesigners() 
	{	
		try 
		{
			try
			{
				driver.findElement(By.xpath("//*[@id='skPageLayoutCell_1_id-center-south']/div/div[2]")).click();
				Thread.sleep(2000);
				String currentURL = driver.getCurrentUrl();
				System.out.println("currentURL:" +currentURL);
				Thread.sleep(1000);
				int Designercount = driver.findElements(By.xpath("(//*[@class='designerBrands'])")).size();
				System.out.println("Designercount:" +Designercount);	
				//jtbrand JSON
				String browsejtbrd="C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtbrand.js";
				String browsejtbrand=readFile(browsejtbrd);
				System.out.println("browsejtbrand:" +browsejtbrand);
				String browsejtbrandjson=browsejtbrand.substring(browsejtbrand.indexOf("skone_callback(")+15,browsejtbrand.lastIndexOf(")"));
				System.out.println("browsejtbrandjson:" +browsejtbrandjson);
				JSONObject browsejtbrandjsonf = new JSONObject(browsejtbrandjson);
				JSONObject browsejtbrandchild = browsejtbrandjsonf.getJSONObject("children");
				JSONArray browsejtbrandcategories=browsejtbrandchild.getJSONArray("categories");
				for(int brbd=1;brbd<=Designercount;brbd++)
				{
					String desingerbrandname=driver.findElement(By.xpath("(//*[@class='designerBrands'])["+brbd+"]")).getText();
					System.out.println("desingerbrandname:" +desingerbrandname);
					for(int brjt=0;brjt<browsejtbrandcategories.length();brjt++)
					{
						JSONObject browsejtbrandcategryobj=browsejtbrandcategories.getJSONObject(brjt);
						String browsejtbrandcategryname=browsejtbrandcategryobj.getString("name");
						System.out.println("browsejtbrandcategryname:" +browsejtbrandcategryname);	
						if(browsejtbrandcategryname.equals(desingerbrandname))
						{
							String logErr894="Pass:"+brbd+".The brand name in the designer page gets matches with the jtbrand brand name:\n" +browsejtbrandcategryname+ "\n" +desingerbrandname; 
							logInfo(logErr894);
						}	
					}
				}
				if(currentURL.contains("browsepage_id"))
				{
					String browsepageID=currentURL.substring(currentURL.indexOf("browsepage_id=")+14,currentURL.indexOf("&t=1&debug"));
					System.out.println("browsepageID:" +browsepageID); 
					Float browsepageIDfloat = Float.parseFloat(browsepageID);	
					String browsecfg="C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\assets\\http___d37pzdzkr4258z.cloudfront.net_441_1427913123327.1428442258.js";
					String browseconfig=readFile(browsecfg);
					System.out.println("browseconfig:" +browseconfig);
					String browseconfigjson=browseconfig.substring(browseconfig.indexOf("browsePageConfig = ")+18,browseconfig.lastIndexOf(";"));
					System.out.println("browseconfigjson" +browseconfigjson);	
					JSONObject browseconfiguredetails=new JSONObject(browseconfigjson);	
					System.out.println("browseconfiguredetails:" +browseconfiguredetails);
					JSONArray browsedppages=browseconfiguredetails.getJSONArray("BrowsePages");
					System.out.println("browsedppages:" +browsedppages);
					for(int brp=0;brp<browsedppages.length();brp++)
					{
						System.out.println("brp:" +brp);
						JSONObject browsedppagesjson=browsedppages.getJSONObject(brp);
						String  browsedppageID=browsedppagesjson.getString("ID");
						System.out.println("browsedppageID:" +browsedppageID);
						Float browsedppageIDfloat = Float.parseFloat(browsedppageID);
						if(browsedppageIDfloat.equals(browsepageIDfloat))
						{ 
							System.out.println("Inside IF");
							String  browsedppageIDcategoryID=browsedppagesjson.getString("categoryId");
							System.out.println("browsedppageIDcategoryID:" +browsedppageIDcategoryID);
							//jtcategory JSON
							String browsejtcatbrd="C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtcategory"+browsedppageIDcategoryID+".js";
							String browsejtcategorybrd=readFile(browsejtcatbrd);
							System.out.println("browsejtcategorybrd:" +browsejtcategorybrd);
							String browsejtcategorybrdjson=browsejtcategorybrd.substring(browsejtcategorybrd.indexOf("skone_callback(")+15,browsejtcategorybrd.lastIndexOf(")"));
							System.out.println("browsejtcategorybrdjson:" +browsejtcategorybrdjson);
							JSONObject browsejtcategorybrandjson=new JSONObject(browsejtcategorybrdjson);								
							//jtcategory lookbook json
							String browserjtcatlkbk= "C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtcategory_lookbook.js";
							String browserjtcatlkbkjs=readFile(browserjtcatlkbk); 	
							String browserjtcatlkbkjstrim=browserjtcatlkbkjs.substring(browserjtcatlkbkjs.indexOf("skone_callback(")+15,browserjtcatlkbkjs.lastIndexOf(")"));  				
							JSONObject browserjtcatlkbkjson=new JSONObject(browserjtcatlkbkjstrim);	
							int Designercount1 = driver.findElements(By.xpath("(//*[@class='designerBrands'])")).size();
							System.out.println("Designercount1:" +Designercount1);
							for(int brds=1;brds<=4;brds++)  //Designercount1
							{
								Thread.sleep(2000);
								driver.findElement(By.xpath("(//*[@class='designerBrands'])["+brds+"]")).click();
								Thread.sleep(5000);
								String currenturlbrand1 = driver.getCurrentUrl();
								System.out.println("currenturlbrand1:" +currenturlbrand1);
								if(currenturlbrand1.contains("searchTerm")&&currenturlbrand1.contains("Designer")&&currenturlbrand1.contains("DESIGNER")&&currenturlbrand1.contains("pageType"))
								{
									String brandname=(currenturlbrand1.substring(currenturlbrand1.indexOf("title=")+6,currenturlbrand1.indexOf("&t=1&debug"))).toUpperCase();
									System.out.println("brandname:" +brandname); 
									String brandname2=currenturlbrand1.substring(currenturlbrand1.indexOf("title=")+6,currenturlbrand1.indexOf("&t=1&debug"));
									System.out.println("brandname2:" +brandname2);
									String brandpagetype=currenturlbrand1.substring(currenturlbrand1.indexOf("pageType=")+9,currenturlbrand1.indexOf("&title="));   
									System.out.println("brandpagetype:" +brandpagetype);
									if(brandpagetype.equals("comonly"))
									{
										System.out.println("-----------------------------------------------------------------------------------------");
										String logErr2100="The product contains com olny:" +brandpagetype; 
										logInfo(logErr2100);
										String brandnameindesp= driver.findElement(By.xpath("(//*[@class='bNameText'])")).getText();
										System.out.println("brandnameindesp:" +brandnameindesp);
										if(brandname.equals(brandnameindesp))
										{
											String logErr2103="Pass:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2103);
										}
										else
										{
											String logErr2104="Fail:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2104);
										}
										String designerfacetsbrandname=driver.findElement(By.xpath("(//*[@class='bPfacetSelections'])")).getText();
										System.out.println("designerfacetsbrandname:" +designerfacetsbrandname);
										String designerfacetsbrandnametrim=designerfacetsbrandname.substring(designerfacetsbrandname.indexOf("(")+1,designerfacetsbrandname.lastIndexOf(")"));
										System.out.println("designerfacetsbrandnametrim:" +designerfacetsbrandnametrim);
										if(designerfacetsbrandnametrim.equals(brandname2))
										{
											String logErr2105="Pass:The selected designer in the filterpanel gets matched with the brandname:" +designerfacetsbrandnametrim; 
											logInfo(logErr2105);
										}
										else
										{
											String logErr2106="Fail:The selected designer in the filterpanel gets matched with the brandname:" +designerfacetsbrandnametrim; 
											logInfo(logErr2106);
										}
										driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']")).click();
										Thread.sleep(2000);
										int designerbrandproductsize=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
										System.out.println("designerbrandproductsize:" +designerbrandproductsize);
										JSONArray browsejtcategoryfacets=browsejtcategorybrandjson.getJSONArray("facets");
										JSONObject browsejtcategorydesigner=browsejtcategoryfacets.getJSONObject(0);
										JSONArray browsejtcategorydesignervalues=browsejtcategorydesigner.getJSONArray("values");
										for(int desar=0;desar<browsejtcategorydesignervalues.length();desar++)
										{
											JSONObject browsejtcategoryvalues=browsejtcategorydesignervalues.getJSONObject(desar);
											String browsejtcategorydesignernames=browsejtcategoryvalues.getString("name");
											System.out.println("browsejtcategorydesignernames:" +browsejtcategorydesignernames);
											if(browsejtcategorydesignernames.equals(designerfacetsbrandnametrim))
											{
												JSONArray browsejtcategorydesignerarr=browsejtcategoryvalues.getJSONArray("value");
												for(int despval=1;despval<=designerbrandproductsize;despval++)
												{
													String designercategorypagetext=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+despval+"]")).getAttribute("prdtid");
													System.out.println("designercategorypagetext:" +designercategorypagetext);
													String designercategorypagenametext=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+despval+"]")).getText();
													System.out.println("designercategorypagenametext:" +designercategorypagenametext);
													for(int desvarr=0;desvarr<browsejtcategorydesignerarr.length();desvarr++)
													{
														String designercategorypagearrval=browsejtcategorydesignerarr.getString(desvarr);
														System.out.println("designercategorypagearrval:" +designercategorypagearrval);
														if(designercategorypagetext.equals(designercategorypagearrval))
														{
															String logErr2101="The products displayed in the designer page gets matches with the jtcategory file:\n" +designercategorypagetext+ "\n" +designercategorypagenametext; 
															logInfo(logErr2101);
														}					 
													}
												}
											}								
										}
										System.out.println("-----------------------------------------------------------------------------------------");
									}
									else if(brandpagetype.equals("comandlookbook"))
									{
										String logErr2101="The product contains com and lookbook:" +brandpagetype; 
										logInfo(logErr2101);
										//Look book Tab
										String brandnameinlkdesp= driver.findElement(By.xpath("(//*[@class='bNameText'])")).getText();
										System.out.println("brandnameinlkdesp:" +brandnameinlkdesp);
										if(brandname.equals(brandnameinlkdesp))
										{
											String logErr2203="Pass:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2203);
										}
										else
										{
											String logErr2204="Fail:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2204);
										}
										String designerfacetslkbrandname=driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']/div/div[2]")).getText();
										System.out.println("designerfacetslkbrandname:" +designerfacetslkbrandname);
										String designerfacetslkbrandnametrim=designerfacetslkbrandname.substring(designerfacetslkbrandname.indexOf("(")+1,designerfacetslkbrandname.lastIndexOf(")"));
										System.out.println("designerfacetslkbrandnametrim:" +designerfacetslkbrandnametrim);
										if(designerfacetslkbrandnametrim.equals(brandname2))
										{
											String logErr2205="Pass:The selected designer in the filterpanel gets matched with the brandname:" +designerfacetslkbrandnametrim; 
											logInfo(logErr2205);
										}
										else
										{
											String logErr2206="Fail:The selected designer in the filterpanel gets matched with the brandname:" +designerfacetslkbrandnametrim; 
											logInfo(logErr2206);
										}
										driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']")).click();
										Thread.sleep(2000);
										int designerlkbrandproductsize=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
										System.out.println("designerlkbrandproductsize:" +designerlkbrandproductsize);
										JSONObject browselkbkcategoryjson=browserjtcatlkbkjson.getJSONObject("children");
										JSONArray browselkbkcategoryjsonpdts=browselkbkcategoryjson.getJSONArray("products");			
										for(int deblk=0;deblk<browselkbkcategoryjsonpdts.length();deblk++)
										{
											JSONObject browselkbkcategoryjsonpdtsibj=browselkbkcategoryjsonpdts.getJSONObject(deblk);
											String  browselkbkcategoryjsonpdtsibjname=browselkbkcategoryjsonpdtsibj.getString("name");
											if(designerfacetslkbrandnametrim.equals(browselkbkcategoryjsonpdtsibjname))
											{
												String browselkbkcategoryjsonpdtsibjiden=browselkbkcategoryjsonpdtsibj.getString("identifier");		
												for(int desplkbk=1;desplkbk<=1;desplkbk++)  //designerlkbrandproductsize
												{
													String designerlkcategorypagetext=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage bPimageDivBorder'])["+desplkbk+"]")).getAttribute("prdtid");
													System.out.println("designerlkcategorypagetext:" +designerlkcategorypagetext);
													String designercategorypagenametext=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+desplkbk+"]")).getText();
													System.out.println("designercategorypagenametext:" +designercategorypagenametext);
													if(designerlkcategorypagetext.equals(browselkbkcategoryjsonpdtsibjiden))
													{
														String logErr2201="The products displayed in the designer page gets matches with the jtcategory_lookbook file:\n" +designerlkcategorypagetext+ "\n" +designercategorypagenametext; 
														logInfo(logErr2201);
													}					 
												}
											}
										}								
										Thread.sleep(1000);
										//COM Tab
										driver.findElement(By.xpath("(//*[@class='bpLookbookTabs'])")).click();
										Thread.sleep(2000);

										String brandcnameindesp= driver.findElement(By.xpath("(//*[@class='bNameText'])")).getText();
										System.out.println("brandcnameindesp:" +brandcnameindesp);
										if(brandname.equals(brandcnameindesp))
										{
											String logErr2103="Pass:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2103);
										}
										else
										{
											String logErr2104="Fail:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2104);
										}
										String designercfacetsbrandname=driver.findElement(By.xpath("(//*[@class='bPfacetSelections'])")).getText();
										System.out.println("designercfacetsbrandname:" +designercfacetsbrandname);
										String designercfacetsbrandnametrim=designercfacetsbrandname.substring(designercfacetsbrandname.indexOf("(")+1,designercfacetsbrandname.lastIndexOf(")"));
										System.out.println("designercfacetsbrandnametrim:" +designercfacetsbrandnametrim);
										if(designercfacetsbrandnametrim.equals(brandname2))
										{
											String logErr2305="Pass:The selected designer in the filterpanel gets matched with the brandname:" +designercfacetsbrandnametrim; 
											logInfo(logErr2305);
										}
										else
										{
											String logErr2306="Fail:The selected designer in the filterpanel gets matched with the brandname:" +designercfacetsbrandnametrim; 
											logInfo(logErr2306);
										}
										driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']")).click();
										Thread.sleep(2000);
										int designercbrandproductsize=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
										System.out.println("designercbrandproductsize:" +designercbrandproductsize);
										JSONArray browsecjtcategoryfacets=browsejtcategorybrandjson.getJSONArray("facets");
										JSONObject browsecjtcategorydesigner=browsecjtcategoryfacets.getJSONObject(0);
										JSONArray browsecjtcategorydesignervalues=browsecjtcategorydesigner.getJSONArray("values");
										for(int dcesar=0;dcesar<browsecjtcategorydesignervalues.length();dcesar++)
										{
											JSONObject browsecjtcategoryvalues=browsecjtcategorydesignervalues.getJSONObject(dcesar);
											String browsecjtcategorydesignernames=browsecjtcategoryvalues.getString("name");
											System.out.println("browsejtcategorydesignernames:" +browsecjtcategorydesignernames);
											if(browsecjtcategorydesignernames.equals(designercfacetsbrandnametrim))
											{
												JSONArray browsecjtcategorydesignerarr=browsecjtcategoryvalues.getJSONArray("value");
												for(int descpval=1;descpval<=designercbrandproductsize;descpval++)
												{
													String designerccategorypagetext=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+descpval+"]")).getAttribute("prdtid");
													System.out.println("designerccategorypagetext:" +designerccategorypagetext);
													String designerccategorypagenametext=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+descpval+"]")).getText();
													System.out.println("designerccategorypagenametext:" +designerccategorypagenametext);
													for(int descvarr=0;descvarr<browsecjtcategorydesignerarr.length();descvarr++)
													{
														String designerccategorypagearrval=browsecjtcategorydesignerarr.getString(descvarr);
														System.out.println("designerccategorypagearrval:" +designerccategorypagearrval);
														if(designerccategorypagetext.equals(designerccategorypagearrval))
														{
															String logErr2301="The products displayed in the designer page gets matches with the jtcategory file:\n" +designerccategorypagetext+ "\n" +designerccategorypagenametext; 
															logInfo(logErr2301);
														}					 
													}
												}
											}								
										}					
									}
									else if(brandpagetype.equals("lookbookonly"))
									{
										String logErr2102="The product containslookbookonly" +brandpagetype; 
										logInfo(logErr2102);
										//Lookbook only	
										String brandnameinlkbkdesp= driver.findElement(By.xpath("(//*[@class='bNameText'])")).getText();
										System.out.println("brandnameinlkbkdesp:" +brandnameinlkbkdesp);
										if(brandname.equals(brandnameinlkbkdesp))
										{
											String logErr2403="Pass:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2403);
										}
										else
										{
											String logErr2404="Fail:The product name displayed in the page gets matched with the jtbrand file:" +brandname; 
											logInfo(logErr2404);
										}
										String designerfacetslkbkbrandname=driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']/div/div[2]")).getText();
										System.out.println("designerfacetslkbkbrandname:" +designerfacetslkbkbrandname);
										String designerfacetslkbkbrandnametrim=designerfacetslkbkbrandname.substring(designerfacetslkbkbrandname.indexOf("(")+1,designerfacetslkbkbrandname.lastIndexOf(")"));
										System.out.println("designerfacetslkbkbrandnametrim:" +designerfacetslkbkbrandnametrim);
										if(designerfacetslkbkbrandnametrim.equals(brandname2))
										{
											String logErr2405="Pass:The selected designer in the filterpanel gets matched with the brandname:" +designerfacetslkbkbrandnametrim; 
											logInfo(logErr2405);
										}
										else
										{
											String logErr2406="Fail:The selected designer in the filterpanel gets matched with the brandname:" +designerfacetslkbkbrandnametrim; 
											logInfo(logErr2406);
										}
										driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']")).click();
										Thread.sleep(2000);
										int designerlkbkbrandproductsize=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
										System.out.println("designerlkbkbrandproductsize:" +designerlkbkbrandproductsize);
										JSONObject browselkbkocategoryjson=browserjtcatlkbkjson.getJSONObject("children"); 
										JSONArray browselkbkocategoryjsonpdts=browselkbkocategoryjson.getJSONArray("products");			
										for(int deblko=0;deblko<browselkbkocategoryjsonpdts.length();deblko++)
										{
											JSONObject browselkbkocategoryjsonpdtsibj=browselkbkocategoryjsonpdts.getJSONObject(deblko);
											String  browselkbkocategoryjsonpdtsibjname=browselkbkocategoryjsonpdtsibj.getString("name");
											if(designerfacetslkbkbrandnametrim.equals(browselkbkocategoryjsonpdtsibjname))
											{
												String browselkbkocategoryjsonpdtsibjiden=browselkbkocategoryjsonpdtsibj.getString("identifier");		
												for(int desplkbko=1;desplkbko<=2;desplkbko++)  //designerlkbkbrandproductsize
												{
													String designerlkocategorypagetext=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage bPimageDivBorder'])["+desplkbko+"]")).getAttribute("prdtid");
													System.out.println("designerlkocategorypagetext:" +designerlkocategorypagetext);
													String designercategoryopagenametext=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+desplkbko+"]")).getText();
													System.out.println("designercategoryopagenametext:" +designercategoryopagenametext);
													if(designerlkocategorypagetext.equals(browselkbkocategoryjsonpdtsibjiden))
													{
														String logErr2401="The products displayed in the designer page gets matches with the jtcategory_lookbook file:\n" +designerlkocategorypagetext+ "\n" +designercategoryopagenametext; 
														logInfo(logErr2401);
													}					 
												}
											}											
										}			
									}
								}
								else
								{
									String logErr11199= "----------------------------------------------------------------------"; 
									logInfo(logErr11199); 
									String logErr1099= "The product doesn't contain the pagetype"; 
									logInfo(logErr1099); 
									System.out.println("-----------");
									String  browsedppagetype=browsedppagesjson.getString("pageType");
									System.out.println("browsedppagetype:" +browsedppagetype);
									String currenturlbrand2 = driver.getCurrentUrl();
									System.out.println("currenturlbrand2:" +currenturlbrand2);
									String brandname3=(currenturlbrand2.substring(currenturlbrand2.indexOf("title=")+6,currenturlbrand2.indexOf("&t=1&debug"))).toUpperCase();
									System.out.println("brandname3:" +brandname3); 
									String brandname4=currenturlbrand2.substring(currenturlbrand2.indexOf("title=")+6,currenturlbrand2.indexOf("&t=1&debug"));
									System.out.println("brandname4:" +brandname4);
									//Look book Tab
									String brandnameinlkdespl= driver.findElement(By.xpath("(//*[@class='bNameText'])")).getText();
									System.out.println("brandnameinlkdespl:" +brandnameinlkdespl);
									if(brandname3.equals(brandnameinlkdespl))
									{
										String logErr2603="Pass:The product name displayed in the page gets matched with the jtbrand file:" +brandname3; 
										logInfo(logErr2603);
									}
									else
									{
										String logErr2604="Fail:The product name displayed in the page gets matched with the jtbrand file:" +brandname3; 
										logInfo(logErr2604);
									}
									//Checking for the designer which exists in the lookbook tab
									JSONObject browselkbkcategoryjsonl11=browserjtcatlkbkjson.getJSONObject("children");  
									JSONArray browselkbkcategoryjsonpdtsl11=browselkbkcategoryjsonl11.getJSONArray("products");	
									for(int deblkl11=0;deblkl11<browselkbkcategoryjsonpdtsl11.length();deblkl11++)
									{
										JSONObject browselkbkcategoryjsonpdtsibjl11=browselkbkcategoryjsonpdtsl11.getJSONObject(deblkl11);
										String  browselkbkcategoryjsonpdtsibjnamel11=browselkbkcategoryjsonpdtsibjl11.getString("name");
										if(brandname4.equals(browselkbkcategoryjsonpdtsibjnamel11))
										{
											String designerfacetslkbrandnamel=driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']/div/div[2]")).getText();
											System.out.println("designerfacetslkbrandnamel:" +designerfacetslkbrandnamel);
											String designerfacetslkbrandnametriml=designerfacetslkbrandnamel.substring(designerfacetslkbrandnamel.indexOf("(")+1,designerfacetslkbrandnamel.lastIndexOf(")"));
											System.out.println("designerfacetslkbrandnametriml:" +designerfacetslkbrandnametriml);				
											if(designerfacetslkbrandnametriml.equals(brandname4))
											{
												String logErr2605="Pass:The selected designer in the filterpanel gets matched with the brandname:" +designerfacetslkbrandnametriml; 
												logInfo(logErr2605);
												driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']")).click();
												Thread.sleep(2000);
												int designerlkbrandproductsizel=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
												System.out.println("designerlkbrandproductsizel:" +designerlkbrandproductsizel);
												JSONObject browselkbkcategoryjsonl=browserjtcatlkbkjson.getJSONObject("children"); 
												JSONArray browselkbkcategoryjsonpdtsl=browselkbkcategoryjsonl.getJSONArray("products");			
												for(int deblkl=0;deblkl<browselkbkcategoryjsonpdtsl.length();deblkl++)
												{
													JSONObject browselkbkcategoryjsonpdtsibjl=browselkbkcategoryjsonpdtsl.getJSONObject(deblkl);
													String  browselkbkcategoryjsonpdtsibjnamel=browselkbkcategoryjsonpdtsibjl.getString("name");
													if(designerfacetslkbrandnametriml.equals(browselkbkcategoryjsonpdtsibjnamel))
													{
														String browselkbkcategoryjsonpdtsibjidenl=browselkbkcategoryjsonpdtsibjl.getString("identifier");		
														for(int desplkbkl=1;desplkbkl<=1;desplkbkl++)  //designerlkbrandproductsize
														{
															String designerlkcategorypagetextl=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage bPimageDivBorder'])["+desplkbkl+"]")).getAttribute("prdtid");
															System.out.println("designerlkcategorypagetextl:" +designerlkcategorypagetextl);
															String designercategorypagenametextl=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+desplkbkl+"]")).getText();
															System.out.println("designercategorypagenametextl:" +designercategorypagenametextl);
															if(designerlkcategorypagetextl.equals(designerlkcategorypagetextl))
															{
																String logErr2601="The products displayed in the designer page gets matches with the jtcategory_lookbook file:\n" +designerlkcategorypagetextl+ "\n" +designercategorypagenametextl; 
																logInfo(logErr2601);
															}					 
														}
													}
												}
												Thread.sleep(1000);
												//COM Tab
												driver.findElement(By.xpath("(//*[@class='bpLookbookTabs'])")).click();
												Thread.sleep(2000);
												String brandcnameindesp1= driver.findElement(By.xpath("(//*[@class='bNameText'])")).getText();
												System.out.println("brandcnameindesp1:" +brandcnameindesp1);
												if(brandname3.equals(brandcnameindesp1))
												{
													String logErr2603="Pass:The product name displayed in the page gets matched with the jtbrand file:" +brandname3; 
													logInfo(logErr2603);
												}
												else
												{
													String logErr2604="Fail:The product name displayed in the page gets matched with the jtbrand file:" +brandname3; 
													logInfo(logErr2604);
												}
												String designercfacetsbrandnamel=driver.findElement(By.xpath("(//*[@class='bPfacetSelections'])")).getText();
												System.out.println("designercfacetsbrandnamel:" +designercfacetsbrandnamel);
												String designercfacetsbrandnametriml=designercfacetsbrandnamel.substring(designercfacetsbrandnamel.indexOf("(")+1,designercfacetsbrandnamel.lastIndexOf(")"));
												System.out.println("designercfacetsbrandnametriml:" +designercfacetsbrandnametriml);
												if(designercfacetsbrandnametriml.equals(brandname4))
												{
													String logErr2805="Pass:The selected designer in the filterpanel gets matched with the brandname:" +designercfacetsbrandnametriml; 
													logInfo(logErr2805);
												}
												else
												{
													String logErr2606="Fail:The selected designer in the filterpanel gets matched with the brandname:" +designercfacetsbrandnametriml; 
													logInfo(logErr2606);
												}
												driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']")).click();
												Thread.sleep(2000);
												int designercbrandproductsizel=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
												System.out.println("designercbrandproductsizel:" +designercbrandproductsizel);
												JSONArray browsecjtcategoryfacetsl=browsejtcategorybrandjson.getJSONArray("facets");
												JSONObject browsecjtcategorydesignerl=browsecjtcategoryfacetsl.getJSONObject(0);
												JSONArray browsecjtcategorydesignervaluesl=browsecjtcategorydesignerl.getJSONArray("values");
												for(int dcesarl=0;dcesarl<browsecjtcategorydesignervaluesl.length();dcesarl++)
												{
													JSONObject browsecjtcategoryvaluesl=browsecjtcategorydesignervaluesl.getJSONObject(dcesarl);
													String browsecjtcategorydesignernamesl=browsecjtcategoryvaluesl.getString("name");
													System.out.println("browsejtcategorydesignernames:" +browsecjtcategorydesignernamesl);
													if(browsecjtcategorydesignernamesl.equals(designercfacetsbrandnametriml))
													{
														JSONArray browsecjtcategorydesignerarrl=browsecjtcategoryvaluesl.getJSONArray("value");
														for(int descpvall=1;descpvall<=designercbrandproductsizel;descpvall++)
														{
															String designerccategorypagetextl=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+descpvall+"]")).getAttribute("prdtid");
															System.out.println("designerccategorypagetextl:" +designerccategorypagetextl);
															String designerccategorypagenametextl=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+descpvall+"]")).getText();
															System.out.println("designerccategorypagenametextl:" +designerccategorypagenametextl);
															for(int descvarrl=0;descvarrl<browsecjtcategorydesignerarrl.length();descvarrl++)
															{
																String designerccategorypagearrvall=browsecjtcategorydesignerarrl.getString(descvarrl);
																System.out.println("designerccategorypagearrval:" +designerccategorypagearrvall);
																if(designerccategorypagetextl.equals(designerccategorypagearrvall))
																{
																	String logErr2801="The products displayed in the designer page gets matches with the jtcategory file:\n" +designerccategorypagetextl+ "\n" +designerccategorypagenametextl; 
																	logInfo(logErr2801);
																}					 
															}
														}
													}
												}
											}
										}
										else
										{
											String logErr2806="The particular brand doesn't contains in the jtcategory_lookbook:\n"+brandname3; 
											logInfo(logErr2806);
											Thread.sleep(2000);
											//COM Tab
											driver.findElement(By.xpath("//*[@id='bp_tab_0']")).click();
											Thread.sleep(2000);
											String brandcnameindesp11= driver.findElement(By.xpath("(//*[@class='bNameText'])")).getText();
											System.out.println("brandcnameindesp:" +brandcnameindesp11);
											if(brandname3.equals(brandcnameindesp11))
											{
												String logErr2803="Pass:The product name displayed in the page gets matched with the jtbrand file:" +brandname3; 
												logInfo(logErr2803);
											}
											else
											{
												String logErr2804="Fail:The product name displayed in the page gets matched with the jtbrand file:" +brandname3; 
												logInfo(logErr2804);
											}
											String designercfacetsbrandname11=driver.findElement(By.xpath("(//*[@class='bPfacetSelections'])")).getText();
											System.out.println("designercfacetsbrandname11:" +designercfacetsbrandname11);
											String designercfacetsbrandnametrim11=designercfacetsbrandname11.substring(designercfacetsbrandname11.indexOf("(")+1,designercfacetsbrandname11.lastIndexOf(")"));
											System.out.println("designercfacetsbrandnametrim:" +designercfacetsbrandnametrim11);
											if(designercfacetsbrandnametrim11.equals(brandname4))
											{
												String logErr2905="Pass:The selected designer in the filterpanel gets matched with the brandname:" +designercfacetsbrandnametrim11; 
												logInfo(logErr2905);
											}
											else
											{
												String logErr2906="Fail:The selected designer in the filterpanel gets matched with the brandname:" +designercfacetsbrandnametrim11; 
												logInfo(logErr2906);
											}
											driver.findElement(By.xpath("//*[@id='id_bPfacet_DESIGNER']")).click();
											Thread.sleep(2000);
											int designercbrandproductsize11=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
											System.out.println("designercbrandproductsize11:" +designercbrandproductsize11);
											JSONArray browsecjtcategoryfacets11=browsejtcategorybrandjson.getJSONArray("facets");
											JSONObject browsecjtcategorydesigner11=browsecjtcategoryfacets11.getJSONObject(0);
											JSONArray browsecjtcategorydesignervalues11=browsecjtcategorydesigner11.getJSONArray("values");
											for(int dcesar11=0;dcesar11<browsecjtcategorydesignervalues11.length();dcesar11++)
											{
												JSONObject browsecjtcategoryvalues11=browsecjtcategorydesignervalues11.getJSONObject(dcesar11);
												String browsecjtcategorydesignernames11=browsecjtcategoryvalues11.getString("name");
												System.out.println("browsejtcategorydesignernames:" +browsecjtcategorydesignernames11);
												if(browsecjtcategorydesignernames11.equals(designercfacetsbrandnametrim11))
												{
													JSONArray browsecjtcategorydesignerarr11=browsecjtcategoryvalues11.getJSONArray("value");
													for(int descpval11=1;descpval11<=designercbrandproductsize11;descpval11++)
													{
														String designerccategorypagetext11=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage'])["+descpval11+"]")).getAttribute("prdtid");
														System.out.println("designerccategorypagetext11:" +designerccategorypagetext11);
														String designerccategorypagenametext11=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+descpval11+"]")).getText();
														System.out.println("designerccategorypagenametext:" +designerccategorypagenametext11);
														for(int descvarr=0;descvarr<browsecjtcategorydesignerarr11.length();descvarr++)
														{
															String designerccategorypagearrval11=browsecjtcategorydesignerarr11.getString(descvarr);
															System.out.println("designerccategorypagearrval11:" +designerccategorypagearrval11);
															if(designerccategorypagetext11.equals(designerccategorypagearrval11))
															{
																String logErr2901="The products displayed in the designer page gets matches with the jtcategory file:\n" +designerccategorypagetext11+ "\n" +designerccategorypagenametext11; 
																logInfo(logErr2901);
															}					 
														}
													}
												}
											}
										}	
									}	
								}
									Thread.sleep(2000);	
									driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[2]/div")).click();  
									Thread.sleep(3000);	
							}		
						}
					}
				}
				else
				{ 
					int Designercount3 = driver.findElements(By.xpath("(//*[@class='designerBrands'])")).size();
					System.out.println("Designercount3:" +Designercount3);
					String browseidjtbrd="C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtbrand.js";
					String browseidjtbrand=readFile(browseidjtbrd);
					System.out.println("browseidjtbrand:" +browseidjtbrand);
					String browseidjtbrandjson=browseidjtbrand.substring(browseidjtbrand.indexOf("skone_callback(")+15,browseidjtbrand.lastIndexOf(")"));
					System.out.println("browseidjtbrandjson:" +browseidjtbrandjson);
					JSONObject browseidjtbrandjsonf = new JSONObject(browseidjtbrandjson);
					JSONObject browseidjtbrandchild = browseidjtbrandjsonf.getJSONObject("children");
					JSONArray browseidjtbrandcategories=browseidjtbrandchild.getJSONArray("categories");
					for(int brbda=1;brbda<=25;brbda++)//Designercount3
					{
						String desingeridbrandname=driver.findElement(By.xpath("(//*[@class='designerBrands'])["+brbda+"]")).getText();
						System.out.println("desingeridbrandname:" +desingeridbrandname);	 
						Thread.sleep(1000);
						for(int brjtb=0;brjtb<browseidjtbrandcategories.length();brjtb++)
						{
							JSONObject browseidjtbrandcategryobj=browseidjtbrandcategories.getJSONObject(brjtb);
							String browseidjtbrandcategryname=browseidjtbrandcategryobj.getString("name");
							System.out.println("browseidjtbrandcategryname:" +browseidjtbrandcategryname);	
							if(browseidjtbrandcategryname.equals(desingeridbrandname))
							{
								JSONObject browseidjtbrandcategrynamechild=browseidjtbrandcategryobj.getJSONObject("children");
								JSONArray browseidjtbrandcategrynamecateg=browseidjtbrandcategrynamechild.getJSONArray("categories");
								int browseidjtbrandcategrynamecategsize=browseidjtbrandcategrynamecateg.length(); 
								String logErr2599= "\n" +browseidjtbrandcategryname+ ":" +browseidjtbrandcategrynamecategsize; 
								logInfo(logErr2599);
								Thread.sleep(500);			
								if(browseidjtbrandcategrynamecategsize==1)
								{
									driver.findElement(By.xpath("(//*[@class='designerBrands'])["+brbda+"]")).click();
									Thread.sleep(2000);
									int designerlkcbkbrandproductsize=driver.findElements(By.xpath("//*[@id='id_bPpdtContScroller']/div")).size();
									System.out.println("designerlkcbkbrandproductsize:" +designerlkcbkbrandproductsize);
									String currenturlbrand2 = driver.getCurrentUrl();
									System.out.println("currenturlbrand2:" +currenturlbrand2);
									String brandname12=(currenturlbrand2.substring(currenturlbrand2.indexOf("title=")+6,currenturlbrand2.indexOf("&pageType=")));
									System.out.println("brandname12:" +brandname12);
									Thread.sleep(1000);
									//jtcategory lookbook json
									String browsercjtcatlkbk= "C:\\Program Files\\Apache Software Foundation\\Apache2.4\\htdocs\\SkavaCatalog\\catalog\\data\\jtcategory_lookbook.js";
									String browsercjtcatlkbkjs=readFile(browsercjtcatlkbk); 	
									String browsercjtcatlkbkjstrim=browsercjtcatlkbkjs.substring(browsercjtcatlkbkjs.indexOf("skone_callback(")+15,browsercjtcatlkbkjs.lastIndexOf(")"));  				
									JSONObject browsercjtcatlkbkjson=new JSONObject(browsercjtcatlkbkjstrim);	
									JSONObject browseclkbkocategoryjson=browsercjtcatlkbkjson.getJSONObject("children"); 
									JSONArray browseclkbkocategoryjsonpdts=browseclkbkocategoryjson.getJSONArray("products");
									for(int deblkoc=0;deblkoc<browseclkbkocategoryjsonpdts.length();deblkoc++)
									{
										JSONObject browseclkbkocategoryjsonpdtsibj=browseclkbkocategoryjsonpdts.getJSONObject(deblkoc);
										String  browseclkbkocategoryjsonpdtsibjname=browseclkbkocategoryjsonpdtsibj.getString("name");
										if(brandname12.equals(browseclkbkocategoryjsonpdtsibjname))
										{											
											String browselkbkoccategoryjsonpdtsibjiden=browseclkbkocategoryjsonpdtsibj.getString("identifier");		
											for(int desplkbkoc=1;desplkbkoc<=1;desplkbkoc++)  //designerlkcbkbrandproductsize
											{
												String designerclkocategorypagetext=driver.findElement(By.xpath("(//*[@class='browsePagePrdtImage bPimageDivBorder'])["+desplkbkoc+"]")).getAttribute("prdtid");
												System.out.println("designerclkocategorypagetext:" +designerclkocategorypagetext);
												String designerccategoryopagenametext=driver.findElement(By.xpath("(//*[@class='bPpdtName'])["+desplkbkoc+"]")).getText();
												System.out.println("designerccategoryopagenametext:" +designerccategoryopagenametext);
												if(designerclkocategorypagetext.equals(browselkbkoccategoryjsonpdtsibjiden))
												{
													String logErr2511="The products displayed in the designer page gets matches with the jtcategory_lookbook file:\n" +designerclkocategorypagetext+ "\n" +designerccategoryopagenametext; 
													logInfo(logErr2511);
												}					 
											}
										}		
									}	
									Thread.sleep(1000);
									driver.findElement(By.xpath("//*[@id='id_scfFooter']/div[2]/div[2]")).click();
									Thread.sleep(3000);
								}
								else
								{
									String logErr3099= "\nThe product contains the overlay"; 
									logInfo(logErr3099);
									driver.findElement(By.xpath("(//*[@class='designerBrands'])["+brbda+"]")).click();
									Thread.sleep(2000);
									int designercomoverlaysize=driver.findElements(By.xpath("(//*[@class='brandSubCatgry'])")).size(); 
									System.out.println("designercomoverlaysize:" +designercomoverlaysize);
									String logErr2512="The products displayed in the overlay gets matches with the jtbrand:"; 
									logInfo(logErr2512);
									for(int overl=0,overlays=1;overl<designercomoverlaysize||overlays<=designercomoverlaysize;overl++,overlays++)
									{
										String siteoverlayfacets=driver.findElement(By.xpath("(//*[@class='brandSubCatgry'])["+overlays+"]")).getText();
										System.out.println("siteoverlayfacets:" +siteoverlayfacets);
										JSONObject jtbrnadcategory=browseidjtbrandcategrynamecateg.getJSONObject(overl);
										String jteoverlayfacets=jtbrnadcategory.getString("name");
										System.out.println("jteoverlayfacets:" +jteoverlayfacets);
										if(siteoverlayfacets.equals(jteoverlayfacets))
										{
											String logErr2511="\n" +siteoverlayfacets; 
											logInfo(logErr2511);			
										}	
									}
									driver.findElement(By.xpath("(//*[@class='subBrndPopupClsBtn'])")).click();
									Thread.sleep(2000);	
								}
							}	
						}
					}
					int designerhead=driver.findElements(By.xpath("//*[@id='skPageLayoutCell_18_id-center']/div[1]/div[3]/div")).size();
					System.out.println("designerhead:" +designerhead);
					Thread.sleep(1000);
					for(int scr=3;scr<=designerhead;scr++)
					{
						String brndname=(driver.findElement(By.xpath("html/body/div[2]/div/div/div/div/div[1]/div[3]/div["+scr+"]")).getText()).toLowerCase();
						System.out.println("brndname:" +brndname);	
						driver.findElement(By.xpath("//*[@id='id_"+brndname+"']")).click();		
						try
						{
							if(driver.findElement(By.xpath("(//*[@class='designerTitle "+brndname+"'])")).isDisplayed())
							{
								String logErr2521="Pass:The Selected brand name char gets displayed correctly:";
								logInfo(logErr2521);
							}
							else
							{
								String logErr2522="Fail:The Selected brand name gets displayed correctly:";
								logInfo(logErr2522);
							}		
						}
						catch(Exception e)
						{
							System.out.println(e.toString());
						}
					}
				}		
			}	
			catch(Exception e)
			{
				System.out.println(e.toString());
			}
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
	}					
	private static void elementHighlight(WebElement shoppingbag) 
	{

	}
	private static String concat(String strmStoreinfoIdentifier,String strmStoreinfoName)
	{
		return null;
	}
	private static String readFile(String filename)
	{
		String retData =  null;

		try
		{
			byte data[] = null;
			FileInputStream fis = new FileInputStream(filename);
			DataInputStream dis = new DataInputStream(fis);
			data = new byte[fis.available()];
			dis.readFully(data);
			retData = new String(data);
		}
		catch(Exception e)
		{
			System.out.println("Exception in readfile "+ e.toString());
		}
		return retData;
	}
}
